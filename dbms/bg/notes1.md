## Functional dependency

R(A, B, C, D)

BA -> CD

1NF: Multiple entries in one cell are removed
2NF: No partial dependency

AB -> C
A -> C
B -> C
C is partially dependent on AB

R(A, B, C, D, E)

ABCD -> ABCDE
ABC -> ABCDE
AB -> ABCDE
Candidate key is AB

## Transitive Dependency

A -> B -> C
A -> C

## The algorithm for finding the closure

X+ := X
Repeat
old X+ := X+
for each functional dependency Y -> Z in F do
    if X+ improper super set of Y then 

Consider a relation R(A, C, D, E, F)
E -> A, E -> D, A -> C, A -> D, AE -> F, AG ->

Consider a relation R(A, C, D, E)
FD: A -> B, B -> C, C -> D, A -> E

Closure:
A+ = ABCDE
B+ = BCD
C+ = CD
D+ = D

If FD: A -> B, B -> D, C -> D, B -> E

A+ = ABDE
B+ = BDE
C+ = CD
D and E does not derive anything

(AB)+ = ABDE
(AC)+ = ABCDE
(BC)+ = BCDE

Hence candidate key is AC

Now we have to decompose the relation into two tables, in a way it would maintain referential integrity.

```
A | B | C | E // C is non primary key
C | D // C is primary key
```
Transitive dependency still exists (A -> B -> E), hence 2NF.





