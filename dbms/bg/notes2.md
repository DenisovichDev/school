## Fully Functional Dependency

AB -> C holds, but B -> C or A -> C does not.
Otherwise it would have been partial dependency.

## Normalisation Steps

**Unnormalised data**: Database does not have separate date values. Meaning multiple values in one column

Hence, in 1NF, the multiple values are removed and assigned to different cells, hence adding more redundancy, which seems counter intuitive to the very reason of normalisation which is to reduce redundancy, not _increase_ it.

To convert a dataset to 2NF:

 - The dataset has to be in 1NF
 - Partial key dependency has to be identified, and removed through decomposition

The decomposition needs to be done in such a way that referential integrity is maintained.

To identify partial key dependency, we need to find candidate key, hence we need closure.

Algorithm to find closure:
result := A;
while (changes to result) do
    for each functional dependency B -> C in F do
        if B is (improper) subset of result then
            result := result U C;
    end for
end while

In 2NF there can exist transitive functional dependency,
A -> B -> C


For a table to be in 3NF:

- The table should be in 2NF
- There should not be any Transitive Functional Dependency. Which simply means all the non-key attributes must depend on the primary key only, no non-key (non prime) attribute can depend on another non-key (non prime) attribute.

**Q**: _Given relation R(PQRST), FD = {PQ -> R, S -> T}. Determine whether the given R is in 2NF. If not, convert to 2NF._
From the above diagram of R we can see that an attribute PQS is not determined by any of the given FD, hence PQS will be the integral part of the candidate key, ie, no matter what will be the candidate key, and how many will be the candidate keys, but all will have PWS compulsary attribute. 
Since the closure of PQS contains all the attributes of R, hence PQS is the candidate key
From the definition of candidate key (candidate key is a super key whose no proper subset is a super key)
Since all key will have PQS as an integral part and we have proved that PQS is the candidate key. Therefore any superset of PQS will be a super key but not candidate key. Hence there will be any one candidate key PQS. From the definition of 2NF no non-prime attribute should be partially dependent on the candidate key. Since R has five attributes, PQRST, and the candidate key is PQS, therefore, prime attributes (part of candidate key) are P, Q and S while a non-prime attribute is R and T.

- FD: PQ -> R does not satisfy the condition of 2NF, that non-prime attribute is partially dependent on a part of PQS
- S -> T does not satisfy the definition of 2NF, with the same reason as above

Hence FD: PQ -> R and S -> T, the above table R(PQRST) in not in 2NF.

Reference table: PQS
