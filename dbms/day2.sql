-- SELECT Clause
SELECT member_name, member_address, penalty_amount
FROM member;

-- Cartesian product
SELECT * FROM member, books;

SELECT DISTINCT member_name, member_address, penalty_amount -- For getting non duplicate values
FROM member;

-- WHERE Clause
SELECT member_name, member_address
FROM member
WHERE membership_type='lifetime'

-- ORDER BY keyword
SELECT * FROM member
ORDER BY member_name

SELECT * FROM member
ORDER BY member_name DESC -- For Descending order. ASC for ascending

-- Operators
-- Logical
SELECT * FROM member
WHERE address='Kolkata' AND fees_paid > 500

-- LIKE
SELECT * FROM member
WHERE address='Kolkata' AND fees_paid > 500 LIKE 'a%'

SELECT * FROM issue
WHERE return_date IS NULL;

-- Retrive the penalty amount of the members who have taken 101 (The C Programing Language)
SELECT member_name, book_name, penalty_amount FROM member, books, issue
WHERE member.member_id = issue.member_id AND issue.book_no = books.book_no AND book_name='The C Programing Language'

-- Schema Manipulation Statements
-- INSERT INTO
INSERT INTO member
VALUES (9, 'Sayantan Sinha', 'Pune', '2010-12-10', 'Lifetime', 2000, 6, 50);
-- DELETE
DELETE FROM member
WHERE member_id = 9;
-- UPDATE
UPDATE member
SET member_name='Suradip Basu', member_address='Kolkata'
WHERE member_id = 9


