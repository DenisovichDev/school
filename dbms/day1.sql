DROP DATABASE library;	-- Deletes previous datebases named library

CREATE DATABASE library;

CREATE TABLE member (member_id int(5),
					member_name varchar(30) NOT NULL,
					member_address varchar(50),
					acc_open_date date,
					membership_type varchar(20),
					fees_paid int(4),
					max_books_allowed int(2),
					penalty_amount decimal(7,2)
                    );

-- Setting constraints directly
/*
CREATE TABLE member (member_id int(5) NOT NULL PRIMARY KEY,
					member_name varchar(30) NOT NULL,
					member_address varchar(50) DEFAULT NULL,
					acc_open_date date DEFAULT NULL,
					membership_type varchar(20) DEFAULT NULL CHECK (membership_type = 'lifetime' or membership_type='annual' or membership_type='half yearly' or membership_type = 'quaterly'),
					fees_paid int(4) DEFAULT NULL,
					max_books_allowed int(2) DEFAULT NULL CHECK (max_books_allowed < 7),
					penalty_amount decimal(7,2) DEFAULT NULL CHECK (penalty_amount < 1000)
                    );
*/

DESC member; -- Shows the table that's been created

DROP TABLE member;	-- Deletes the table

ALTER TABLE member 
MODIFY member_name varchar(30) NOT NULL; -- if you forget to add it before

ALTER TABLE member ADD CONSTRAINT m1 PRIMARY KEY(member_id); -- Adds contraint m1 to member_id
ALTER TABLE member ADD CONSTRAINT m2 CHECK (membership_type IN ('Lifetime', 'Annual', 'Half Yearly', 'Quarterly'));
ALTER TABLE member ADD CONSTRAINT m3 CHECK (max_books_allowed<7);
ALTER TABLE member ADD CONSTRAINT m4 CHECK (penalty_amount<=1000);

-- Test data
INSERT INTO member VALUES (1, 'Sayantan Sinha', 'Pune', '2010-12-10', 'Lifetime', 2000, 6, 50);

TRUNCATE member; -- Deletes all the data instances

CREATE TABLE books (book_no int(6),
					book_name varchar(30) NOT NULL,
					author_name varchar(30),
					cost decimal(7,2),
					category char(10) -- char for names, varchar is for alphanumerics
					);

ALTER TABLE books ADD CONSTRAINT b1 PRIMARY KEY (book_no);
ALTER TABLE books ADD CONSTRAINT b2 CHECK (category IN ('Science', 'Database', 'System', 'Others'));

CREATE TABLE issue (lib_issue_id int(10),
					book_no int(6),
					member_id int(5),
					issue_date date,
					return_date date
					);

ALTER TABLE issue ADD CONSTRAINT i1 PRIMARY KEY (lib_issue_id);
ALTER TABLE issue ADD CONSTRAINT i2 FOREIGN KEY (book_no) REFERENCES books (book_no);
ALTER TABLE issue ADD CONSTRAINT i3 FOREIGN KEY (member_id) REFERENCES member (member_id);

-- Data Entry
INSERT INTO member VALUES
(1, 'Sayantan Sinha', 'Pune', '2010-12-10', 'Lifetime', 2000, 6, 50),
(2, 'Abhirup Sarkar', 'Kolkata', '2011-01-19', 'Annual', 1400, 3, 0),
(3, 'Ritesh Bhuniya', 'Gujarat', '2011-02-20', 'Quarterly', 350, 2, 100),
(4, 'Paresh Sen', 'Tripura', '2011-03-21', 'Half Yearly', 700, 1, 200),
(5, 'Sohini Halder', 'Birbhum', '2011-04-11', 'Lifetime', 2000, 6, 10),
(6, 'Suparna Biswas', 'Kolkata', '2011-04-12', 'Half Yearly', 700, 1, 0),
(7, 'Suranjana Basu', 'Purulia', '2011-06-30', 'Annual', 1400, 3, 50),
(8, 'Arpita Roy', 'Kolkata', '2011-07-31', 'Half Yearly', 700, 1, 0);

INSERT INTO books VALUES 
(101, 'The C Programing Language', 'Brian Kernighan', 450, 'Others'),
(102, 'Oracle - Complete Reference', 'Loni', 550, 'Database'),
(103, 'Visual Basic 10', 'BPB', 700, 'Others'),
(104, 'Mastering SQL', 'Loni', 450, 'Database'),
(105, 'PL SQL Reference', 'Scott Urman', 760, 'Database'),
(106, 'UNIX', 'Sumitava Das', 300, 'System'),
(107, 'Optics', 'Ghatak', 600, 'Science'),
(108, 'Data Structure', 'G.S. Baluja', 350, 'Others');

INSERT INTO issue VALUES 
(7001, 101, 1, '2011-01-10', ''),
(7002, 102, 2, '2011-01-23', ''),
(7003, 104, 1, '2011-02-01', ''),
(7004, 104, 2, '2011-03-15', ''),
(7005, 101, 4, '2011-04-04', ''),
(7006, 108, 5, '2011-04-12', ''),
(7007, 101, 8, '2011-08-01', '');

-- Query
SELECT member_id, member_name
FROM member
WHERE membership_type='annual' AND member_address='kolkata';

