---
numbersections: false
geometry:
- margin=1in
header-includes: |
    \usepackage{tikzit}
    \usepackage{algorithm}
    \usepackage{algpseudocode}
    \usepackage{multirow}
    \usepackage{rotating}
    \input{styles.tikzstyles}
    \usepackage[document]{ragged2e}
---

# Normalization

The process of organizing the fields and tables of a relational database to minimize redundancy and dependability.
There are four types of normal forms: 1NF, 2NF, 3NF and Boyce-Codd Normal Form (BCNF/4NF)

## Problems of Unnormalised Data

Unnormalized form (UNF), also known as an unnormalized relation or non-first normal form (N1NF), is a database data model (organization of data in a database) which does not meet any of the conditions of database normalization defined by the relational model. The problems of unnormalized data is:

- **Data Redundancy**
- **Update Anomalies**: Unnormalized data may suffer from anomalies during data update, leading to inconsistency. E.g., If the address of a customer changes multiple records need to be updated, increasing the likelihood of errors.
- **Insertion Anomalies**: Inserting new data may be problematic, especially when the mandatory fields are not applicable. Also the absence of referential integrity creates problems leading to anomalies.
- **Deletion Anomalies**: Deletion may result in unintentional loss of related information.

## Functional Dependency:

Relationship between two sets of attributes in a database, where one set of attributes can determine the value of the other. If A and B are attributes, and B is functionally dependent on A, then the notation is $A \rightarrow B$ (A *derives* B). Here $A$ is determinant and $B$ is dependent.

### Derivation Rules:

The derivation rules are defined by three axioms named Armstrong's axioms, the following:

- **Reflexivity**: If $Y \subseteq X$, then $X \rightarrow Y$.
- **Augmentation**: If $X \rightarrow Y$, then  $XZ \rightarrow YZ$, for any  $Z$.
- **Transitivity**: If $X \rightarrow Y$ and $Y \rightarrow Z$, then $X \rightarrow Z$.

## Closure of FD Set:

The closure of a set a of functional dependencies is the set of all functional dependencies logically implied by the given set.  
The algorithm to find the closure of an FD set utilizes Armstrong's axioms to find all possible FDs. The algorithm is the following

### Algorithm to find closure:

```
result := A;
while (changes to result) do
    for each functional dependency B -> C in F do
        if B is (improper) subset of result then
            result := result U C;
    end for
end while
```

### Canonical Cover

## Normalization

In the normalization process of a table, we need to check whether a table satisfies the conditions of every
normal form. If yes, we could say the given table is in that normal form. That is, if a table satisfies the conditions of 1NF, then we say that the table is in 1NF, and so on.

### First Normal Form (1NF)

In RDBMS, 1NF is one of the properties a relation or table must satisfy. It requires the following conditions to be met:

1. The table has a primary key, in which each of the other non-prime attributes are functionally dependent upon.
1. \underline{Any attribute (column) of a table must have been designed to accept only atomic values.}
1. Any value stored in any column must have single value (no repeating groups).

Hence, in 1NF, the multiple values are removed and assigned to different cells, hence adding more redundancy, which seems counter intuitive to the very reason of normalization which is to reduce redundancy, not _increase_ it.

![Unnormalized table Student](./figures/UNF.png)

The above _Student_ table is unnormalized, since it does not satisfy any of the conditions, especially that a tuple cannot be uniquely identified. Hence we need to add a primary key. Let's call it _student ID_. Hence our new table would be:  
_Student (\underline{student ID}, student name, fees paid, date of birth, address, subject 1, subject 2, subject 3, subject 4, teacher name, teacher address, course name)_  
The also cannot be multiple values in a single column in this table, so those would be split into multiple different rows. However, in this particular case it is not needed.

### Second Normal Form (2NF)

For a database to be in 2NF, the two following properties must satisfy:

1. The table must be in 1NF (satisfy all the properties of 1NF)
1. There should not be any partial key dependencies.

The second property is not applicable for relations (tables) which have single simple attribute as Primary Key. Because, single attribute primary key means that all the other attributes can be determined by the Prime attribute. Hence, property 2 is applicable for relations those have more than one attribute combination as Primary Key (i.e., Composite Key). Because, here is the possibility of some of the attributes of the table depend on any one or all of the attributes of the composite primary key.

Partial key dependency occurs when one primary key determines some other attribute/attributes. For example, if $AB \rightarrow CDEF$ and $B \rightarrow D$, then D is partially dependent on B.

If we look at our current table, we see that 

#### Advantages


