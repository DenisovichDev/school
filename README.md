# University Class Notes

This repository has the class notes, programs, assignments etc. of my current university semester. 

Papers for the current semesters:

- Database Management Systems
- Object Oriented Programming using Java
- Python Programing Language
- Data Image Processing using OpenCV

## Reference Books

**Database Management Systems**:
- [Database System Concepts](https://drive.google.com/file/d/1JQIDZFNzc02YQczxI9_7N62ZQMLdJueC/view?usp=drive_link) - Abraham Silberschatz, Henry F. Korth, S. Sudarshan

**Digital Image Processing**:
- [Digital Image Processing](reference/Digital%20Image%20Processing%20by%20Gonzalez%20and%20Woods%20(4th%20Edition).pdf) - Gonzalez and Woods

**Java**:
- [Java: The Complete Reference](reference/Java%20The%20Complete%20Reference%20-%20Herbert%20Schildt.pdf) - Herbert Schildt
