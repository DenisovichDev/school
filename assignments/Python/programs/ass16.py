def filter_list(tuple_list):
    # This removes adds if the tuple t
    # returns true, meaning the tuple is 
    # is not empty
    result_list = [t for t in tuple_list if t]
    return result_list

if __name__ == "__main__":
    n = int(input("Enter number of tuples: "))
    tuple_list = [tuple(map(int, input(f"Enter tuple {i+1} elements separated by space: ").split())) for i in range(n)]

    filtered_list = filter_list(tuple_list)

    print("\nList of Tuples after Removing Empty Tuples:")
    print(filtered_list)

