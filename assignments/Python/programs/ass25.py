from collections import Counter

if __name__ == "__main__":
    input_dict1_str = input("Enter the first dictionary (in the format key:value, separated by commas): ")
    input_dict1_items = [item.split(':') for item in input_dict1_str.split(',')]
    dict1 = {key.strip(): int(value.strip()) for key, value in input_dict1_items}

    input_dict2_str = input("Enter the second dictionary (in the format key:value, separated by commas): ")
    input_dict2_items = [item.split(':') for item in input_dict2_str.split(',')]
    dict2 = {key.strip(): int(value.strip()) for key, value in input_dict2_items}

    combined_dict = dict(Counter(dict1) + Counter(dict2))

    print("\nOriginal Dictionary 1:")
    print(dict1)
    print("\nOriginal Dictionary 2:")
    print(dict2)
    print("\nCombined Dictionary:")
    print(combined_dict)
