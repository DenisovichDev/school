def find_longest_word(words):
    if not words:
        return 0
    longest_length = len(words[0])

    for word in words[1:]:
        current_length = len(word)
        if current_length > longest_length:
            longest_length = current_length

    return longest_length

if __name__ == "__main__":
    word_list = input("Enter a list of words separated by space: ").split()
    result = find_longest_word(word_list)
    print("The length of the longest word is:", result)
