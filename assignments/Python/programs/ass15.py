def transpose_matrix(matrix):
    rows = len(matrix)
    columns = len(matrix[0])

    result = [[0 for _ in range(rows)] for _ in range(columns)]

    for i in range(rows):
        for j in range(columns):
            result[j][i] = matrix[i][j]

    return result

def print_matrix(matrix):
    for row in matrix:
        print(row)

if __name__ == "__main__":
    rows = int(input("Enter the number of rows: "))
    columns = int(input("Enter the number of columns: "))

    matrix = [[int(input(f"Enter element at position {i+1},{j+1}: ")) for j in range(columns)] for i in range(rows)]

    transposed_matrix = transpose_matrix(matrix)

    print("\nOriginal Matrix:")
    print_matrix(matrix)

    print("\nTransposed Matrix:")
    print_matrix(transposed_matrix)

