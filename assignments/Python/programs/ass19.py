input_str = input("Enter the string: ")
find_str = input("Enter the substring to find: ")
replace_str = input("Enter the replacement string: ")

result_string = input_str.replace(find_str, replace_str)

print("\nOriginal String:")
print(input_str)

print("\nString after Replacement:")
print(result_string)
