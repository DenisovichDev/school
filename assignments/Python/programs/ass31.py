def count_words(filename):
    try:
        with open(filename, 'r') as file:
            content = file.read()
            words = content.split()
            num_words = len(words)
            print(f"Number of words in '{filename}': {num_words}")
    except FileNotFoundError as e:
        print(f"Error: {e.filename} not found.")
    except PermissionError as e:
        print(f"Error: Permission denied. Check if you have read access to '{e.filename}'.")

def main():
    filename = input("Enter the filename: ")
    count_words(filename)

if __name__ == "__main__":
    main()
