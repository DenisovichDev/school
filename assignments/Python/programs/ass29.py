def combine_lines(file1, file2):
    try:
        with open(file1, 'r') as f1, open(file2, 'r') as f2:
            lines1 = f1.readlines()
            lines2 = f2.readlines()
            min_length = min(len(lines1), len(lines2))
            for line1, line2 in zip(lines1[:min_length], lines2[:min_length]):
                print(line1.strip(), line2.strip())

    except FileNotFoundError as e:
        print(f"Error: {e.filename} not found.")
    except PermissionError as e:
        print(f"Error: Permission denied. Check if you have read access to '{e.filename}'.")

def main():
    file1 = input("Enter the first filename: ")
    file2 = input("Enter the second filename: ")

    combine_lines(file1, file2)

if __name__ == "__main__":
    main()
