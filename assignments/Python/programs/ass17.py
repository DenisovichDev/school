def count_elements_until_tuple(lst):
    count = 0
    for element in lst:
        if isinstance(element, tuple):
            break
        count += 1
    return count

# Get input from the user
n = int(input("Enter the number of elements in the list: "))
input_list = [eval(input(f"Enter element {i+1} (Python expression): ")) for i in range(n)]

# Count elements until a tuple is encountered
result_count = count_elements_until_tuple(input_list)

# Display the result
print(f"\nThe number of elements until a tuple is encountered: {result_count}")

