import random

if (__name__ == "__main__"):
    print("Flipping a coin 100 times...");
    h = 0;
    t = 0;
    for i in range(100):
        if (random.randint(0, 1)):
            h += 1;
        else:
            t += 1;

    print("\nNumber of heads:", h);
    print(  "Number of tails:", t);

