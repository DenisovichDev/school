if __name__ == "__main__":
    input_dict_str = input("Enter a dictionary (in the format key:value, separated by commas):")
    input_dict_items = [item.split(':') for item in input_dict_str.split(',')]
    input_dict = {key.strip(): value.strip() for key, value in input_dict_items}

    # Invert the dictionary
    inverted_dict = {value: key for key, value in input_dict.items()}

    # Display the original and inverted dictionaries
    print("\nOriginal Dictionary:")
    print(input_dict)
    print("\nInverted Dictionary:")
    print(inverted_dict)
