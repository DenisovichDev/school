if __name__ == "__main__":
    n = int(input("Enter number of elements: "))
    input_list = [int(input(f"Enter element {i+1}: ")) for i in range(n)]

    filtered_list = list(set(input_list))

    print("\nOriginal List:")
    print(input_list)

    print("\nDuplicate Removed List")
    print(filtered_list)
