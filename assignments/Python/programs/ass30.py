import sys

def count_word_frequency(file_path):
    try:
        with open(file_path, 'r') as file:
            word_count = {}
            for line in file:
                words = line.split()
                for word in words:
                    word = word.strip().lower()
                    if word:
                        word_count[word] = word_count.get(word, 0) + 1

            return word_count

    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return None
    except PermissionError:
        print(f"Error: Permission denied for '{file_path}'.")
        return None
    except Exception as e:
        print(f"An unexpected error occurred: {e}")
        return None

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python word_frequency_counter.py <file_path>")
    else:
        file_path = sys.argv[1]
        word_frequency = count_word_frequency(file_path)

        if word_frequency is not None:
            print("Word Frequency:")
            for word, count in word_frequency.items():
                print(f"{word}: {count}")

