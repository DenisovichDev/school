def matrixmult(m1, m2):
    result = [[0 for _ in range(len(m2[0]))] for _ in range(len(m1))]

    for i in range(len(m1)):
        for j in range(len(m2[0])):
            for k in range(len(m2)):
                result[i][j] += m1[i][k] * m2[k][j]

    return result

def print_matrix(matrix):
    for row in matrix:
        print(row)

if __name__ == "__main__":
    rows1 = int(input("Enter the number of rows for matrix 1: "))
    cols1 = int(input("Enter the number of columns for matrix 1: "))

    m1 = [[int(input(f"Enter element at position {i+1},{j+1} for matrix 1: ")) for j in range(cols1)] for i in range(rows1)]

    rows2 = int(input("Enter the number of rows for matrix 2: "))
    cols2 = int(input("Enter the number of columns for matrix 2: "))

    m2 = [[int(input(f"Enter element at position {i+1},{j+1} for matrix 2: ")) for j in range(cols2)] for i in range(rows2)]

    if cols1 != rows2:
        print("Matrix multiplication is not possible. Number of columns in matrix 1 should be equal to the number of rows in matrix 2.")
    else:
        result_matrix = matrixmult(m1, m2)
        print("\nProduct Matrix:")
        print_matrix(result_matrix)
