import shutil
import re
import sys

def copy_file(source, destination):
    try:
        shutil.copy(source, destination)
        print(f"File '{source}' copied to '{destination}'.")
    except FileNotFoundError:
        print(f"Error: File '{source}' not found.")
    except PermissionError:
        print(f"Error: Permission denied for '{destination}'.")

def grep(pattern, file_path):
    try:
        with open(file_path, 'r') as file:
            for line_number, line in enumerate(file, start=1):
                if re.search(pattern, line):
                    print(f"{file_path}:{line_number}:{line.strip()}")
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
    except PermissionError:
        print(f"Error: Permission denied for '{file_path}'.")

def cat(file_path):
    try:
        with open(file_path, 'r') as file:
            content = file.read()
            print(content)
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
    except PermissionError:
        print(f"Error: Permission denied for '{file_path}'.")

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python program.py <command> <arguments>")
    else:
        command = sys.argv[1]

        if command == 'cp':
            if len(sys.argv) != 4:
                print("Usage: python program.py cp <source_file> <destination_file>")
            else:
                copy_file(sys.argv[2], sys.argv[3])

        elif command == 'grep':
            if len(sys.argv) != 4:
                print("Usage: python program.py grep <pattern> <file_path>")
            else:
                grep(sys.argv[2], sys.argv[3])

        elif command == 'cat':
            if len(sys.argv) != 3:
                print("Usage: python program.py cat <file_path>")
            else:
                cat(sys.argv[2])

        else:
            print(f"Error: Unknown command '{command}'. Valid commands are 'cp', 'grep', and 'cat'.")
