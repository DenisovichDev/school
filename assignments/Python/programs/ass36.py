class Square:
    def __init__(self, side):
        self.side = side;

    def getVolume(self):
        vol = self.side ** 3;
        print("Volume:", vol);


class Cylinder(Square):
    def __init__(self, height, radius):
        self.height = height;
        self.side = radius;

    def getVolume(self):
        vol = self.side * (math.pi ** 2) * self.height;
        print("Volume:", vol);

if __name__ == "__main__":
    s1 = Square(5)
    s2 = Square(6)
    s3 = Square(2)
    s4 = Square(8)

    c1 = Square(5)
    c2 = Square(6)
    c3 = Square(2)
    c4 = Square(8)

    s1.getVolume()
    s2.getVolume()
    s3.getVolume()
    s4.getVolume()
    c1.getVolume()
    c2.getVolume()
    c3.getVolume()
    c4.getVolume()
