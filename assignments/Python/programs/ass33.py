def pascals(rows, symbol):
    triangle = []
    for i in range(rows):
        row = [symbol] * (i + 1)
        triangle.append(row)

    for i in range(2, rows):
        for j in range(1, i):
            triangle[i][j] = triangle[i-1][j-1] + triangle[i-1][j]
    return triangle

def print_pascals_triangle(triangle):
    for row in triangle:
        print(" ".join(map(str, row)).center(len(triangle[-1]) * 3))

if __name__ == "__main__":
    try:
        rows = int(input("Enter the number of rows for Pascal's Triangle: "))
        symbol = input("Enter the character or string to be printed: ")
        if rows <= 0:
            print("Please enter a positive integer for the number of rows.")
        else:
            pascals_triangle = pascals(rows, symbol)
            print_pascals_triangle(pascals_triangle)
    except ValueError:
        print("Invalid input. Please enter a positive integer for the number of rows.")
