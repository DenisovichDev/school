def reverse(x):
  return x[::-1]

def isPalin(input_str):
    rev_string = reverse(input_str)
    if (input_str == rev_string):
        return True
    return False

if __name__ == "__main__":
    input_str = input("Enter a string: ")
    if (isPalin(input_str)):
        print("The string is a palindrome.")
    else:
        print("The string is not a palindrome.")
