def is_palindrome(string):
    return string == string[::-1]

if __name__ == "__main__":
    input_strings_str = input("Enter a list of strings (comma-separated values): ")
    strings = input_strings_str.split(',')

    palindromes = list(filter(lambda x: is_palindrome(x), strings))

    print("\nOriginal List of Strings:", strings)
    print("Palindromes:", palindromes)
