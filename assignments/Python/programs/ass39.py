class Node:
    def __init__(self, data):
        self.data = data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None

    def insert(self, data):     # Insert at beginning
        newNode = Node(data)
        newNode.next = self.head
        self.head = newNode
        return newNode

    def delete(self):           # Delete from end
        if self.head is None:
            return None
        if self.head.next is None:
            self.head = None
            return None

        currentNode = self.head
        prevNode = self.head
        while currentNode.next != None:
            prevNode = currentNode
            currentNode = currentNode.next
        prevNode.next = None
        return currentNode

    def show(self):
        temp = self.head
        print("Head ->", end=" ")
        while temp != None:
            print(temp.data, "->", end=" ")
            temp = temp.next
        print("None")

if __name__ == "__main__":

    ll = LinkedList()
    ll.show()
    ll.insert(4)
    ll.show()
    ll.insert(5)
    ll.insert(6)
    ll.insert(3)
    ll.insert(7)
    ll.show()
    ll.delete()
    ll.show()
    ll.delete()
    ll.delete()
    ll.delete()
    ll.delete()
    ll.delete()
    ll.show()
