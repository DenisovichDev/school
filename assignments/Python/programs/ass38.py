import math

class Shape:
    def calcArea(self):
        pass

    def calcPerimeter(self):
        pass

class Circle(Shape):
    def __init__(self, r):
        self.r = r

    def calcArea(self):
        return math.pi * (self.r ** 2)

    def calcPerimeter(self):
        return 2 * math.pi * self.r

class Triangle(Shape):
    def __init__(self, b, h, side=(0, 0, 0)):
        self.b = b
        self.h = h
        self.side = side

    def calcArea(self):
        return 0.5 * self.b * self.h

    def calcPerimeter(self):
        return sum(self.side)

class Square(Shape):
    def __init__(self, s):
        self.s = s

    def calcArea(self):
        return self.s ** 2

    def calcPerimeter(self):
        return 4 * self.s

if __name__ == "__main__":
    # Example usage of the classes
    circle = Circle(5)
    print(f"Circle Area: {circle.calcArea()}")
    print(f"Circle Perimeter: {circle.calcPerimeter()}")

    triangle = Triangle(3, 4, (5, 6, 7))
    print(f"Triangle Area: {triangle.calcArea()}")
    print(f"Triangle Perimeter: {triangle.calcPerimeter()}")

    square = Square(4)
    print(f"Square Area: {square.calcArea()}")
    print(f"Square Perimeter: {square.calcPerimeter()}")

