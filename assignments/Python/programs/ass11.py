def gaussian_addition(nums):
    if len(nums) % 2 != 0:
        print("The list should have an even number of numbers.")
        return

    start = 0
    end = len(nums) - 1

    while start < end:
        partial_sum = nums[start] + nums[end]
        print(f"{nums[start]} + {nums[end]}")
        start += 1
        end -= 1

    total_sum = sum(nums)
    print(f"\nThe result of the Gaussian addition is: {total_sum}")

if __name__ == "__main__":
    user_input = input("Enter a list of numbers separated by space: ")
    num_list = list(map(int, user_input.split()))

    gaussian_addition(num_list)
