fibonacci = lambda n: [0, 1] if n <= 2 else fibonacci(n - 1) + [fibonacci(n - 1)[-1] + fibonacci(n - 1)[-2]]

if __name__ == "__main__":
    try:
        n = int(input("Enter the value of n: "))
        if n <= 0:
                print("Please enter a positive integer.")
        else:
            result = fibonacci(n)
            print(f"Fibonacci series up to {n}: {result}")
    except ValueError:
        print("Invalid input. Please enter a positive integer.")
