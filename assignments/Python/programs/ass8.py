if __name__ == "__main__":
    input_str = input("Enter a string: ")
    if len(input_str) < 2:
        print("Nothing to change if the string has one or fewer characters.")
        exit(0)

    first_char = input_str[0]
    modified_str = first_char + input_str[1:].replace(first_char, '$')
    
    print("Output:", modified_str)
