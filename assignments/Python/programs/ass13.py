def add_matrices(matrix1, matrix2):
    result = [[matrix1[i][j] + matrix2[i][j] for j in range(len(matrix1[0]))] for i in range(len(matrix1))]
    return result

def subtract_matrices(matrix1, matrix2):
    result = [[matrix1[i][j] - matrix2[i][j] for j in range(len(matrix1[0]))] for i in range(len(matrix1))]
    return result

def print_matrix(matrix):
    for row in matrix:
        print(row)

rows = int(input("Enter the number of rows: "))
columns = int(input("Enter the number of columns: "))

matrix1 = [[int(input(f"Enter element at position {i+1},{j+1} for matrix 1: ")) for j in range(columns)] for i in range(rows)]
matrix2 = [[int(input(f"Enter element at position {i+1},{j+1} for matrix 2: ")) for j in range(columns)] for i in range(rows)]

while True:
    print("\n1. Add Matrices")
    print("2. Subtract Matrices")
    print("3. Exit")

    choice = input("Enter your choice: ")

    if choice == '1':
        result_matrix = add_matrices(matrix1, matrix2)
        print("\nResultant Matrix:")
        print_matrix(result_matrix)

    elif choice == '2':
        result_matrix = subtract_matrices(matrix1, matrix2)
        print("\nResultant Matrix:")
        print_matrix(result_matrix)

    elif choice == '3':
        print("Exiting the program.")
        break

    else:
        print("Please enter a valid option.")
