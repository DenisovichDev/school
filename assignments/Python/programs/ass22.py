if __name__ == "__main__":
    array1 = list(map(int, input("Enter elements of the first array separated by space: ").split()))
    array2 = list(map(int, input("Enter elements of the second array separated by space: ").split()))

    intersection = list(filter(lambda x: x in array1, array2))

    print("Intersection of the Arrays:", intersection)
