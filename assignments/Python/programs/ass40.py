class Student:
    def __init__(self, roll_no, name, age, grade):
        self.roll_no = roll_no
        self.name = name
        self.age = age
        self.grade = grade

class StudentManagementSystem:
    def __init__(self):
        self.students = []

    def accept_student(self):
        roll_no = input("Enter Roll No: ")
        name = input("Enter Name: ")
        age = int(input("Enter Age: "))
        grade = input("Enter Grade: ")

        student = Student(roll_no, name, age, grade)
        self.students.append(student)
        print("Student added successfully!\n")

    def display_students(self):
        if not self.students:
            print("No students in the system.")
            return

        print("\nStudent List:")
        print("Roll No\tName\tAge\tGrade")
        for student in self.students:
            print(f"{student.roll_no}\t{student.name}\t{student.age}\t{student.grade}")
        print()

    def search_student(self, roll_no):
        for student in self.students:
            if student.roll_no == roll_no:
                print(f"\nStudent found:\nRoll No: {student.roll_no}\nName: {student.name}\nAge: {student.age}\nGrade: {student.grade}\n")
                return
        print(f"\nStudent with Roll No {roll_no} not found.\n")

    def delete_student(self, roll_no):
        for student in self.students:
            if student.roll_no == roll_no:
                self.students.remove(student)
                print(f"\nStudent with Roll No {roll_no} deleted successfully.\n")
                return
        print(f"\nStudent with Roll No {roll_no} not found.\n")

    def update_student(self, roll_no):
        for student in self.students:
            if student.roll_no == roll_no:
                print(f"\nUpdate Student {student.name} ({student.roll_no}):")
                student.name = input("Enter new Name: ")
                student.age = int(input("Enter new Age: "))
                student.grade = input("Enter new Grade: ")
                print(f"\nStudent updated successfully:\nRoll No: {student.roll_no}\nName: {student.name}\nAge: {student.age}\nGrade: {student.grade}\n")
                return
        print(f"\nStudent with Roll No {roll_no} not found.\n")

if __name__ == "__main__":
    student_system = StudentManagementSystem()

    while True:
        print("Student Management System Menu:")
        print("1. Accept Student")
        print("2. Display Students")
        print("3. Search Student")
        print("4. Delete Student")
        print("5. Update Student")
        print("6. Exit")

        choice = input("Enter your choice (1-6): ")

        if choice == "1":
            student_system.accept_student()
        elif choice == "2":
            student_system.display_students()
        elif choice == "3":
            roll_no = input("Enter Roll No to search: ")
            student_system.search_student(roll_no)
        elif choice == "4":
            roll_no = input("Enter Roll No to delete: ")
            student_system.delete_student(roll_no)
        elif choice == "5":
            roll_no = input("Enter Roll No to update: ")
            student_system.update_student(roll_no)
        elif choice == "6":
            print("Exiting Student Management System. Goodbye!")
            break
        else:
            print("Invalid choice. Please enter a number from 1 to 6.")
