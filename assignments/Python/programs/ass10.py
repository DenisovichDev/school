if __name__ == "__main__":
    n = int(input("Enter the number of elements in the list: "))
    num_list = [int(input(f"Enter element {i+1}: ")) for i in range(n)]

    odd_numbers = [num for num in num_list if num % 2 != 0]
    divisible_by_3 = [num for num in num_list if num % 3 == 0]

    print("Original List of Numbers:")
    print(num_list)

    print("Odd Numbers:")
    print(odd_numbers)

    print("Numbers Divisible by 3:")
    print(divisible_by_3)
