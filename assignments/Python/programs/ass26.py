from itertools import product

def generate_combinations(dictionary):
    keys = dictionary.keys()
    combinations = list(product(*[dictionary[key] for key in keys]))
    return combinations

if __name__ == "__main__":
    input_dict_str = input("Enter a dictionary (in the format key:letters, separated by commas):")
    input_dict_items = [item.split(':') for item in input_dict_str.split(',')]
    input_dict = {key.strip(): value.strip() for key, value in input_dict_items}

    combinations = generate_combinations(input_dict)
    print("\nAll Combinations:")
    for combo in combinations:
        print(''.join(combo))
