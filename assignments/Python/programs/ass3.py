def fact(n):
    if (n):
        return n * fact(n - 1);
    return 1

def krishnamurti(n):
    s = 0;
    dig = 0;
    temp = n;
    while (temp):
        dig = temp % 10;
        temp = int(temp / 10);
        s += fact(dig);

    if (s == n):
        return True;
    return False;

if (__name__ == "__main__"):
    l, n = input("Enter a range to find all the Krishnamurthy numbers: ").split();
    l = int(l);
    n = int(n);
    if (l < 0 or n < 0):
        print("Please enter positive numbers")
        exit(1);

    print("The krishnamurti numbers in range", l, "->", n, "are:");
    for i in range(l, n + 1):
        if(krishnamurti(i)):
            print(i);
