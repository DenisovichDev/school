def extract_consecutive_digits(input_str, k):
    result = ''
    kc = k
    
    for char in input_str:
        if char.isdigit():
            result += char
            kc -= 1
            if kc == 0:
                break
        else:
            result = ''  # Reset if a non-digit character is encountered
            kc = k
    
    if result:
        return int(result)
    else:
        return None  # Return None if no consecutive digits are found

if __name__ == "__main__":
    user_input = input("Enter a string: ")
    k_value = int(input("Enter the value of K: "))

    result_number = extract_consecutive_digits(user_input, k_value)

    if result_number is not None:
        print(f"The first {k_value} consecutive digits are: {result_number}")
    else:
        print(f"No consecutive digits found in the input.")
