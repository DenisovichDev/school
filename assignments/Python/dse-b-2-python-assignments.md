---
author: Bhaswar Chakraborty
date: Fall, 2023
title: University of Calcutta
paper: "CMS-A-DSE-B--2-P (Python Lab)"
geometry:
- margin=1in
fontsize: 11pt
listings: false
header-includes: |
    \usepackage{tikzit}
    \input{styles.tikzstyles}
    \usepackage[document]{ragged2e}
...

\maketitle
\newpage

\section*{\centering\underline{Assignment 1}}

\begin{flushright}
    \textit{Date: 20.09.23}
\end{flushright}

## Objective

_Write a program to find GCD of two numbers._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass1.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass1.py
Enter two numbers: 12 6
The GCD of 12 6 is: 6
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass1.py
Enter two numbers: 15 6
The GCD of 15 6 is: 3
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass1.py
Enter two numbers: 1347 23
The GCD of 1347 23 is: 1
\end{lstlisting}

\newpage

\section*{\centering\underline{Assignment 2}}

\begin{flushright}
    \textit{Date: 20.09.23}
\end{flushright}

## Objective

_Write a program that flips a coin 100 times and then tells you the number of heads and tails._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass2.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass2.py
Flipping a coin 100 times...

Number of heads: 58
Number of tails: 42
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass2.py
Flipping a coin 100 times...

Number of heads: 57
Number of tails: 43
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass2.py
Flipping a coin 100 times...

Number of heads: 49
Number of tails: 51
\end{lstlisting}

\newpage

\section*{\centering\underline{Assignment 3}}

\begin{flushright}
    \textit{Date: 20.09.23}
\end{flushright}

## Objective

_Write a program that  will find all the Krishnamurthy numbers in a certain range._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass3.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass4.py
Enter a range to find all the Krishnamurthy numbers: 100 200
The krishnamurti numbers in range 100 -> 200 are:
145
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass4.py
Enter a range to find all the Krishnamurthy numbers: 100 10000
The krishnamurti numbers in range 100 -> 10000 are:
145
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass4.py
Enter a range to find all the Krishnamurthy numbers: 100 50000
The krishnamurti numbers in range 100 -> 50000 are:
145
40585
\end{lstlisting}

\newpage

\section*{\centering\underline{Assignment 4}}

\begin{flushright}
    \textit{Date: 20.09.23}
\end{flushright}

## Objective

_Write a program that  will find all the Armstrong numbers in a certain range._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass4.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass3.py
Enter a range to find all the Armstrong numbers: 100 500
The Armstrong numbers in range 100 -> 500 are:
153
370
371
407
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass3.py
Enter a range to find all the Armstrong numbers: 100 50000
The Armstrong numbers in range 100 -> 50000 are:
153
370
371
407
1634
8208
9474
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass3.py
Enter a range to find all the Armstrong numbers: 10000 100000
The Armstrong numbers in range 10000 -> 100000 are:
54748
92727
93084
\end{lstlisting}

\newpage

<!-- ----------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 5}}

\begin{flushright}
    \textit{Date: 30.09.2023}
\end{flushright}

## Objective

_Write a program in Python that will print a string of text with a step length taken from the user and also can print from backwards with a step length taken from the user without slicing technique._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass5.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass5.py
Enter a string to slice into parts: gibbon giblet
Enter a step length: 3
Result 1: gib bon  gi ble t
Result 2: tel big  no bbi g
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass5.py
Enter a string to slice into parts: take me to church I'll worship like a dog
Enter a step length: 2
Result 1: ta ke  m e  to  c hu rc h  I' ll  w or sh ip  l ik e  a  do g
Result 2: go d  a  ek il  p ih sr ow  l l' I  hc ru hc  o t  em  e ka t
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass5.py
Enter a string to slice into parts: moondance
Enter a step length: 3
Result 1: moo nda nce
Result 2: ecn adn oom
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 6}}

\begin{flushright}
    \textit{Date: 30.09.2023}
\end{flushright}

## Objective

_Write a program in Python that will take a string and number $K$ from the user and extract the first $K$ consecutive digits making a number._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass6.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass6.py
Enter a string: kjbeaf65aefjbh
Enter the value of K: 2
The first 2 consecutive digits are: 65
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass6.py
Enter a string: kajbfek43kjnkbfe678eafjkh
Enter the value of K: 2
The first 2 consecutive digits are: 43
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass6.py
Enter a string: ekjabfae56kjaefn765hbjeaf
Enter the value of K: 3
The first 3 consecutive digits are: 765
\end{lstlisting}

\newpage

<!-- ----------------------------------------------------- -->
<!-- % Assignment 7 to 22 template filling -->

\section*{\centering\underline{Assignment 7}}

\begin{flushright}
    \textit{Date: 30.09.2023}
\end{flushright}

## Objective

_Write a program in Python that will check whether a string is a palindrome or not._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass7.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass7.py
Enter a string: bhaswar
The string is not a palindrome.
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass7.py
Enter a string: gibbonobbig
The string is a palindrome.
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass7.py
Enter a string: beebloolbeeb
The string is a palindrome.
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->


\section*{\centering\underline{Assignment 8}}

\begin{flushright}
    \textit{Date: 30.09.2023}
\end{flushright}

## Objective

_Write a Python program to get a string from a given string where all occurrences of its first char have been changed to ‘\$’, except the first char itself. Sample String : ‘restart’ Expected Result : ‘resta\$t’_

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass8.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass8.py
Enter a string: it's the light it is the obstacle that casts it
Output: it's the l$ght $t $s the obstacle that casts $t
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass8.py
Enter a string: rude riddles rattle rodents
Output: rude $iddles $attle $odents
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass8.py
Enter a string: there was a time I was a tidy white man
Output: there was a $ime I was a $idy whi$e man
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 9}}

\begin{flushright}
    \textit{Date: 30.09.2023}
\end{flushright}

## Objective

_Write a Python function that takes a list of words and returns the length of the longest one._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass9.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass9.py
Enter a list of words separated by space: foo fighters radio head beatles oppurtunity
The length of the longest word is: 11
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass9.py
Enter a list of words separated by space: giblet gibbon upon the hill
The length of the longest word is: 6
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass9.py
Enter a list of words separated by space: my son you are a wonder boy
The length of the longest word is: 6
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 10 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 10}}

\begin{flushright}
    \textit{Date: 03.10.2023}
\end{flushright}

## Objective

_Use list comprehension to find all the odd numbers and numbers divisible by 3 from a list of numbers._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass10.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass10.py
Enter the number of elements in the list: 10
Enter element 1: 3
Enter element 2: 10
Enter element 3: 8
Enter element 4: 16
Enter element 5: 5
Enter element 6: 2
Enter element 7: 9
Enter element 8: 7
Enter element 9: 4
Enter element 10: 65
Original List of Numbers:
[3, 10, 8, 16, 5, 2, 9, 7, 4, 65]
Odd Numbers:
[3, 5, 9, 7, 65]
Numbers Divisible by 3:
[3, 9]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass10.py
Enter the number of elements in the list: 3
Enter element 1: 1
Enter element 2: 2
Enter element 3: 3
Original List of Numbers:
[1, 2, 3]
Odd Numbers:
[1, 3]
Numbers Divisible by 3:
[3]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 11}}

\begin{flushright}
    \textit{Date: 03.10.2023}
\end{flushright}

## Objective

_Using while loops to do Gaussian addition on a list having an even number of numbers. Print each partial sum._

_Eg: if the list is [1, 2, 3, 4, 5, 6], the program should output “1 + 6”, “2 + 5”, and “3+4” in separate lines, and the result of the addition “21”. Extend it to handle lists of odd length._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass11.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass11.py
Enter a list of numbers separated by space: 1 3 5 7 9 9
1 + 9
3 + 9
5 + 7

The result of the Gaussian addition is: 34
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass11.py
Enter a list of numbers separated by space: 1 2 3 4 5 6 7 8 9 10
1 + 10
2 + 9
3 + 8
4 + 7
5 + 6

The result of the Gaussian addition is: 55
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass11.py
Enter a list of numbers separated by space: 6 7 83 5 5
The list should have an even number of numbers.
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 12}}

\begin{flushright}
    \textit{Date: 03.10.2023}
\end{flushright}

## Objective

_Write a program in Python to generate a list of primes within a user-given range._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass12.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass12.py
Enter the lower bound: 10
Enter the upper bound: 100
Prime numbers in the given range are [11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass12.py
Enter the lower bound: 100
Enter the upper bound: 10
Prime numbers in the given range are []
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass12.py
Enter the lower bound: 599
Enter the upper bound: 800
Prime numbers in the given range are [599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 13 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 13}}

\begin{flushright}
    \textit{Date: 18.11.2023}
\end{flushright}

## Objective

_Write a menu-driven program in Python to add or subtract two matrices._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass13.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass13.py
Enter the number of rows: 2
Enter the number of columns: 2
Enter element at position 1,1 for matrix 1: 3
Enter element at position 1,2 for matrix 1: 4
Enter element at position 2,1 for matrix 1: 5
Enter element at position 2,2 for matrix 1: 6
Enter element at position 1,1 for matrix 2: 7
Enter element at position 1,2 for matrix 2: 6
Enter element at position 2,1 for matrix 2: 3
Enter element at position 2,2 for matrix 2: 4

1. Add Matrices
2. Subtract Matrices
3. Exit
Enter your choice: 1

Resultant Matrix:
[10, 10]
[8, 10]

1. Add Matrices
2. Subtract Matrices
3. Exit
Enter your choice: 2

Resultant Matrix:
[-4, -2]
[2, 2]

1. Add Matrices
2. Subtract Matrices
3. Exit
Enter your choice: 3
Exiting the program.
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass13.py
Enter the number of rows: 3
Enter the number of columns: 3
Enter element at position 1,1 for matrix 1: 1
Enter element at position 1,2 for matrix 1: 2
Enter element at position 1,3 for matrix 1: 3
Enter element at position 2,1 for matrix 1: 4
Enter element at position 2,2 for matrix 1: 5
Enter element at position 2,3 for matrix 1: 6
Enter element at position 3,1 for matrix 1: 6
Enter element at position 3,2 for matrix 1: 7
Enter element at position 3,3 for matrix 1: 8
Enter element at position 1,1 for matrix 2: 9
Enter element at position 1,2 for matrix 2: 3
Enter element at position 1,3 for matrix 2: 4
Enter element at position 2,1 for matrix 2: 53
Enter element at position 2,2 for matrix 2: 21
Enter element at position 2,3 for matrix 2: 5
Enter element at position 3,1 for matrix 2: 6
Enter element at position 3,2 for matrix 2: 2
Enter element at position 3,3 for matrix 2: 4

1. Add Matrices
2. Subtract Matrices
3. Exit
Enter your choice: 1

Resultant Matrix:
[10, 5, 7]
[57, 26, 11]
[12, 9, 12]

1. Add Matrices
2. Subtract Matrices
3. Exit
Enter your choice: 2

Resultant Matrix:
[-8, -1, -1]
[-49, -16, 1]
[0, 5, 4]

1. Add Matrices
2. Subtract Matrices
3. Exit
Enter your choice: 3
Exiting the program.
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 14 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 14}}

\begin{flushright}
    \textit{Date: 18.11.2023}
\end{flushright}

## Objective

_Write a program in Python to multiply two matrices._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass14.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass14.py
Enter the number of rows for matrix 1: 2
Enter the number of columns for matrix 1: 2
Enter element at position 1,1 for matrix 1: 5
Enter element at position 1,2 for matrix 1: 6
Enter element at position 2,1 for matrix 1: 7
Enter element at position 2,2 for matrix 1: 8
Enter the number of rows for matrix 2: 2
Enter the number of columns for matrix 2: 4
Enter element at position 1,1 for matrix 2: 5
Enter element at position 1,2 for matrix 2: 6
Enter element at position 1,3 for matrix 2: 3
Enter element at position 1,4 for matrix 2: 6
Enter element at position 2,1 for matrix 2: 3
Enter element at position 2,2 for matrix 2: 7
Enter element at position 2,3 for matrix 2: 4
Enter element at position 2,4 for matrix 2: -9

Product Matrix:
[43, 72, 39, -24]
[59, 98, 53, -30]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass14.py
Enter the number of rows for matrix 1: 2
Enter the number of columns for matrix 1: 3
Enter element at position 1,1 for matrix 1: 3
Enter element at position 1,2 for matrix 1: 2
Enter element at position 1,3 for matrix 1: 5
Enter element at position 2,1 for matrix 1: 4
Enter element at position 2,2 for matrix 1: 3
Enter element at position 2,3 for matrix 1: 7
Enter the number of rows for matrix 2: 3
Enter the number of columns for matrix 2: 4
Enter element at position 1,1 for matrix 2: -1
Enter element at position 1,2 for matrix 2: 5
Enter element at position 1,3 for matrix 2: -9
Enter element at position 1,4 for matrix 2: 5
Enter element at position 2,1 for matrix 2: 2
Enter element at position 2,2 for matrix 2: 6
Enter element at position 2,3 for matrix 2: 8
Enter element at position 2,4 for matrix 2: 6
Enter element at position 3,1 for matrix 2: 9
Enter element at position 3,2 for matrix 2: 6
Enter element at position 3,3 for matrix 2: 2
Enter element at position 3,4 for matrix 2: 3

Product Matrix:
[46, 57, -1, 42]
[65, 80, 2, 59]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 15 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 15}}

\begin{flushright}
    \textit{Date: 18.11.2023}
\end{flushright}

## Objective

_Write a program in Python to obtain the transpose of a matrix._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass15.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass15.py
Enter the number of rows: 3
Enter the number of columns: 3
Enter element at position 1,1: 2
Enter element at position 1,2: 3
Enter element at position 1,3: 4
Enter element at position 2,1: 5
Enter element at position 2,2: 6
Enter element at position 2,3: 7
Enter element at position 3,1: 8
Enter element at position 3,2: 6
Enter element at position 3,3: 1

Original Matrix:
[2, 3, 4]
[5, 6, 7]
[8, 6, 1]

Transposed Matrix:
[2, 5, 8]
[3, 6, 6]
[4, 7, 1]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass15.py
Enter the number of rows: 4
Enter the number of columns: 3
Enter element at position 1,1: 6
Enter element at position 1,2: 7
Enter element at position 1,3: -1
Enter element at position 2,1: 2
Enter element at position 2,2: 4
Enter element at position 2,3: -9
Enter element at position 3,1: 6
Enter element at position 3,2: 4
Enter element at position 3,3: -4
Enter element at position 4,1: 6
Enter element at position 4,2: 3
Enter element at position 4,3: 9

Original Matrix:
[6, 7, -1]
[2, 4, -9]
[6, 4, -4]
[6, 3, 9]

Transposed Matrix:
[6, 2, 6, 6]
[7, 4, 4, 3]
[-1, -9, -4, 9]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 16 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 16}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to remove an empty tuple(s) from a list of tuples._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass16.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass16.py
Enter number of tuples: 4
Enter tuple 1 elements separated by space: 2 3 4
Enter tuple 2 elements separated by space:
Enter tuple 3 elements separated by space: 4 5
Enter tuple 4 elements separated by space:

List of Tuples after Removing Empty Tuples:
[(2, 3, 4), (4, 5)]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass16.py
Enter number of tuples: 10
Enter tuple 1 elements separated by space: 3 4
Enter tuple 2 elements separated by space: 7 87
Enter tuple 3 elements separated by space:
Enter tuple 4 elements separated by space:
Enter tuple 5 elements separated by space:
Enter tuple 6 elements separated by space: 7 6
Enter tuple 7 elements separated by space: 0
Enter tuple 8 elements separated by space:
Enter tuple 9 elements separated by space: 4
Enter tuple 10 elements separated by space: 45

List of Tuples after Removing Empty Tuples:
[(3, 4), (7, 87), (7, 6), (0,), (4,), (45,)]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 17 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 17}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to count the elements in a list until an element is a tuple._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass17.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass17.py
Enter the number of elements in the list: 6
Enter element 1 (Python expression): (8, 7)
Enter element 2 (Python expression): [1]
Enter element 3 (Python expression): [2, 4]
Enter element 4 (Python expression): (7)
Enter element 5 (Python expression): [9, 6, 1]
Enter element 6 (Python expression): [5, 6]

The number of elements until a tuple is encountered: 0
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass17.py
Enter the number of elements in the list: 3
Enter element 1 (Python expression): [8, 4]
Enter element 2 (Python expression): (9, 0)
Enter element 3 (Python expression): (8, 7)

The number of elements until a tuple is encountered: 1
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass17.py
Enter the number of elements in the list: 6
Enter element 1 (Python expression): [1, 2, 3]
Enter element 2 (Python expression): []
Enter element 3 (Python expression): []
Enter element 4 (Python expression): []
Enter element 5 (Python expression): (9)
Enter element 6 (Python expression): [9]

The number of elements until a tuple is encountered: 6
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 18 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 18}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to remove duplicates from a list._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass18.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass18.py
Enter number of elements: 4
Enter element 1: 1
Enter element 2: 2
Enter element 3: 3
Enter element 4: 2

Original List:
[1, 2, 3, 2]

Duplicate Removed List
[1, 2, 3]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass18.py
Enter number of elements: 8
Enter element 1: 2
Enter element 2: 4
Enter element 3: 6
Enter element 4: 8
Enter element 5: 6
Enter element 6: 3
Enter element 7: 2
Enter element 8: 4

Original List:
[2, 4, 6, 8, 6, 3, 2, 4]

Duplicate Removed List
[2, 3, 4, 6, 8]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 19 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 19}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a program in Python to find a string and replace it with another string taken from the user as input._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass19.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass19.py
Enter the string: hat hat hat hat hat cat
Enter the substring to find: hat
Enter the replacement string: cat

Original String:
hat hat hat hat hat cat

String after Replacement:
cat cat cat cat cat cat
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass19.py
Enter the string: i was an eagle and you were a dove. i was an eagle and you were a dove. you were a dove and
i rose above you and preyed.
Enter the substring to find: i
Enter the replacement string: giblet

Original String:
i was an eagle and you were a dove. i was an eagle and you were a dove. you were a dove and i rose above you a
nd preyed.

String after Replacement:
giblet was an eagle and you were a dove. giblet was an eagle and you were a dove. you were a dove and giblet r
ose above you and preyed.
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 20 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 20}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to add two given lists and find the difference between lists. Use map() function._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass20.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass20.py
Enter number of elements: 4
Enter the fist list
6
5
7
8
Enter the second list
3
2
5
6
[9, 7, 12, 14]
[3, 3, 2, 2]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass20.py
Enter number of elements: 3
Enter the fist list
6
5
4
Enter the second list
9
8
7
[15, 13, 11]
[-3, -3, -3]
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass20.py
Enter number of elements: 5
Enter the fist list
1
2
3
4
5
Enter the second list
9
8
7
6
5
[10, 10, 10, 10, 10]
[-8, -6, -4, -2, 0]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignments 21 to 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 21}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to compute the sum of integers, use map() function._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass21.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass21.py
Enter first number: 3
Enter second number: 4
7
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass21.py
Enter first number: 8
Enter second number: 6
14
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass21.py
Enter first number: -1
Enter second number: 2
1
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

<!-- % Continue the above structure for Assignment 22 ... -->

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 22}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to find the intersection of two given arrays using Lambda._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass22.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass22.py
Enter elements of the first array separated by space: 1 2 3 4 5 6 7 8 9
Enter elements of the second array separated by space: 2 4 6 10 11 12 8
Intersection of the Arrays: [2, 4, 6, 8]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass22.py
Enter elements of the first array separated by space: 7 5 4 2 3 4 8 6 4 8
Enter elements of the second array separated by space: 3 8 6 7 2
Intersection of the Arrays: [3, 8, 6, 7, 2]
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass22.py
Enter elements of the first array separated by space: 1 2 3 4 5
Enter elements of the second array separated by space: 4 5 6 7 8
Intersection of the Arrays: [4, 5]
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 23}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python program to find palindromes in a given list of strings using Lambda._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/messup.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass24.py
Enter a list of strings (comma-separated values): bhaswar,pushkin,beebeeb

Original List of Strings: ['bhaswar', 'pushkin', 'beebeeb']
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass24.py
Enter a list of strings (comma-separated values): open,bhahb,assignmentnemngissa

Original List of Strings: ['open', 'bhahb', 'assignmentnemngissa']
Palindromes: ['bhahb', 'assignmentnemngissa']
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass24.py
Enter a list of strings (comma-separated values): hallo,loop,noob

Original List of Strings: ['hallo', 'loop', 'noob']
Palindromes: []
\end{lstlisting}

\newpage

<!-- % ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 24}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included) and the values are the square of the keys._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass23.py}

## Output

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass24.py
{1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64, 9: 81, 10: 100, 11: 121, 12: 144, 13: 169, 14: 196, 15: 225}
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 25}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Objective

_Write a Python program to remove duplicates from a dictionary._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass24.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass25.py
Enter number of elements in dict: 4
Enter key: 1
Enter value: w
Enter key: 2
Enter value: e
Enter key: 3
Enter value: w
Enter key: 4
Enter value: w
Initial dict:
{'1': 'w', '2': 'e', '3': 'w', '4': 'w'}
Final dict:
{'1': 'w', '2': 'e'}
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass25.py
Enter number of elements in dict: 3
Enter key: 1
Enter value: gibbon
Enter key: 2
Enter value: giblet
Enter key: 3
Enter value: gibbon
Initial dict:
{'1': 'gibbon', '2': 'giblet', '3': 'gibbon'}
Final dict:
{'1': 'gibbon', '2': 'giblet'}
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass25.py
Enter number of elements in dict: 2
Enter key: d
Enter value: adam
Enter key: w
Enter value: adam
Initial dict:
{'d': 'adam', 'w': 'adam'}
Final dict:
{'d': 'adam'}
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 26}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Objective

\begin{itshape}
Write a Python program to combine two dictionaries, adding values for common keys. Example:
\begin{lstlisting}[style=nolinenumber]
d1 = {'a': 100, 'b': 200, 'c': 300}
d2 = {'a': 300, 'b': 200, 'd': 400}
\end{lstlisting}
Sample output:
\begin{lstlisting}[style=nolinenumber]
Counter({'a': 400, 'b': 400, 'd': 400, 'c': 300})
\end{lstlisting}
\end{itshape}

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass25.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass26.py
Enter the first dictionary (in the format key:value, separated by comm
as): a:100,b:200,c:300
Enter the second dictionary (in the format key:value, separated by com
mas): a:300,b:200,d:400

Original Dictionary 1:
{'a': 100, 'b': 200, 'c': 300}

Original Dictionary 2:
{'a': 300, 'b': 200, 'd': 400}

Combined Dictionary:
{'a': 400, 'b': 400, 'c': 300, 'd': 400}
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass26.py
Enter the first dictionary (in the format key:value, separated by comm
as): a:7,b:7
Enter the second dictionary (in the format key:value, separated by com
mas): b:7,k:9,j:8

Original Dictionary 1:
{'a': 7, 'b': 7}

Original Dictionary 2:
{'b': 7, 'k': 9, 'j': 8}

Combined Dictionary:
{'a': 7, 'b': 14, 'k': 9, 'j': 8}
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 27}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Objective

_Write a Python program to create and display all combinations of letters, selecting each letter from a different key in a dictionary._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass26.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass27.py
Enter a dictionary (in the format key:letters, separated by commas):1:AB,2:FG

All Combinations:
AF
AG
BF
BG
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass27.py
Enter a dictionary (in the format key:letters, separated by commas):1:AB,2:FG

All Combinations:
AF
AG
BF
BG
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass27.py
Enter a dictionary (in the format key:letters, separated by commas):1:io,2:op

All Combinations:
io
ip
oo
op
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 28}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Objective

\begin{itshape}
Invert a dictionary such that the previous keys become values and previous values become keys.\break
Example: if the initial and inverted dictionaries are d1 and d2, where d1 = \{1: ‘a’, 2: ‘b’, 3: 120\}, then d2 = \{‘a’: 1, 2: ‘b’, 120: 3\}
\end{itshape}

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass27.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass28.py
Enter a dictionary (in the format key:value, separated by commas): 
15:225,14:196,13:169,12:144,11:121

Original Dictionary:
{'15': '225', '14': '196', '13': '169', '12': '144', '11': '121'}

Inverted Dictionary:
{'225': '15', '196': '14', '169': '13', '144': '12', '121': '11'}

\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass28.py
Enter a dictionary (in the format key:value, separated by commas): 1:'bcd',2:'efg',3:'hij'

Original Dictionary:
{'1': "'bcd'", '2': "'efg'", '3': "'hij'"}

Inverted Dictionary:
{"'bcd'": '1', "'efg'": '2', "'hij'": '3'}

\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass28.py
Enter a dictionary (in the format key:value, separated by commas): 1:a, 2:b, 3:120

Original Dictionary:
{'1': 'a', '2': 'b', '3': '120'}

Inverted Dictionary:
{'a': '1', 'b': '2', '120': '3'}
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->
\section*{\centering\underline{Assignment 29}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Objective

_Emulate the Unix `cp`, `grep`, `cat` programs in Python. In each case, the user should pass the arguments to the program as command line arguments._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass28.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass29.py cp source.txt destination.txt
File 'source.txt' copied to 'destination.txt'.
assignments $ cat source.txt
Hi this is the source text.
assignments $ cat destination.txt
Hi this is the source text.
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass29.py
Usage: python program.py <command> <arguments>
assignments $ py ass29.py grep if test.py
Error: File 'test.py' not found.
assignments $ py ass28.py grep "if" ass28.py
ass28.py:18:if re.search(pattern, line):
ass28.py:35:if _name_ == "_main_":
ass28.py:36:if len(sys.argv) < 3:
ass28.py:41:if command == 'cp':
ass28.py:42:if len(sys.argv) != 4:
ass28.py:47:elif command == 'grep':
ass28.py:48:if len(sys.argv) != 4:
ass28.py:53:elif command == 'cat':
ass28.py:54:if len(sys.argv) != 3:
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass29.py cat source.txt
Hi this is the source text.
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 30}}

\begin{flushright}
    \textit{Date: 04.12.2023}
\end{flushright}

## Objective

_Write a Python program to combine each line from the first file with the corresponding line in the second file. Use an exception to handle the errors._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass29.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass30.py
Enter the first filename: file1.txt
Enter the second filename: file2.txt
Hi, this is file 1 speaking Hi this is the second file, thank you very much.
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass30.py
Enter the first filename: file2.txt
Enter the second filename: file3.txt
Hi this is the second file, thank you very much. Why don't you stop and have some tea,
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass30.py
Enter the first filename: file.txt
Enter the second filename: file1.txt
Error: file.txt not found.
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 31}}

\begin{flushright}
    \textit{Date: 04.12.2023}
\end{flushright}

## Objective

_Write a Python program to count the frequency of words in a file. Use an exception to handle the errors._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass30.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass31.py file1.txt
Word Frequency:
london: 2
bridge: 2
is: 2
falling: 4
down,: 3
donw,: 1
my: 1
fair: 1
lady: 1
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass31.py file2.txt
Word Frequency:
hi: 1
this: 1
is: 1
the: 1
second: 1
file,: 1
thank: 1
you: 1
very: 1
much.: 1
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass31.py
Usage: python word_frequency_counter.py <file_path>
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 32}}

\begin{flushright}
    \textit{Date: 04.12.2023}
\end{flushright}

## Objective

_Write a Python program that takes a text file as input and returns the number of words of a given text file. Use an exception to handle the errors._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass31.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass32.py
Usage: python word_frequency_counter.py <file_path>
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass32.py file1.txt
Enter the filename: file1.txt
Number of words in 'file1.txt': 17
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass32.py file2.txt
Enter the filename: file2.txt
Number of words in 'file2.txt': 10
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 33}}

\begin{flushright}
    \textit{Date: 04.12.2023}
\end{flushright}

## Objective

_Write a Python program to create a Fibonacci series up to n using Lambda._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass32.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass33.py
Enter the value of n: 3
Fibonacci series up to 3: [0, 1, 1]
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass33.py
Enter the value of n: 4
Fibonacci series up to 4: [0, 1, 1, 2]
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass33.py
Enter the value of n: 15
Fibonacci series up to 15: [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377]
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 34}}

\begin{flushright}
    \textit{Date: 04.12.2023}
\end{flushright}

## Objective

_Write a program in Python to print Pascal’s Triangle. The character or string that will be printed should be taken from the user. You cannot use a triple-quoted string._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass33.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass34.py
Enter the number of rows for Pascal's Triangle: 5
Enter the character or string to be printed: b
       b
      b b
     b bb b
  b bbb bbb b
b bbbb bbbbbb bbbb b
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass34.py
Enter the number of rows for Pascal's Triangle: 3
Enter the character or string to be printed: 1
    1
   1 1
  1 11 1
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass34.py
Enter the number of rows for Pascal's Triangle: 5
Enter the character or string to be printed: *
       *
      * *
     * ** *
  * *** *** *
* **** ****** **** *
\end{lstlisting}


\newpage

<!-- ------------------------------------------------------ -->


\section*{\centering\underline{Assignment 35}}

\begin{flushright}
    \textit{Date: 17.11.2023}
\end{flushright}

## Objective

_Here you have to calculate the percentage of marks obtained in three subjects (each out of 100) by student A and in four subjects (each out of 100) by student B. Create a class 'Marks' with a method 'getPercentage'. It is inherited by two other classes 'A' and 'B', each having a method with the same name which returns the percentage of the students. The constructor of student A takes the marks in three subjects as its parameters and the marks in four subjects as its parameters for student B. Create an object for each of the two classes and print the percentage of marks for both the students._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass35.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass35.py
Average marks in percentage for A: 67.67%
Average marks in percentage for B: 56.50%
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 36}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Objective

_Here create a base class ‘Square’ having instance variable side:double. Initiate variable using constructor, a method ‘getVolume() : double’ that calculates volume and print it. Create a derived class ‘Cylinder’ having instance variable height:double. Initiate variables of both classes through constructor, override method ‘getVolume() : double’ to calculate the volume of the cylinder taking ‘side’ variable of the base class as ‘radius’ and print it._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass36.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass36.py
Volume: 125
Volume: 216
Volume: 8
Volume: 512
Volume: 125
Volume: 216
Volume: 8
Volume: 512
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 37}}

\begin{flushright}
    \textit{Date: 24.11.2023}
\end{flushright}

## Objective

_Write a Python program to create a class representing a stack data structure. Include methods for pushing, popping and displaying elements._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass37.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass37.py
The current stack is:
[4, 8, 9]
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 38}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Objective

_Write a Python program to create a class that represents a shape. Include methods to calculate its area and perimeter. Implement subclasses for different shapes like a circle, triangle, and square._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass38.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass38.py
Circle Area: 78.53981633974483
Circle Perimeter: 31.41592653589793
Triangle Area: 6.0
Triangle Perimeter: 18
Square Area: 16
Square Perimeter: 16
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 39}}

\begin{flushright}
    \textit{Date: 06.12.2023}
\end{flushright}

## Objective

_Write a Python program to create a class representing a linked list data structure. Include methods for displaying linked list data, inserting and deleting nodes._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass39.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass39.py
Head -> None
Head -> 4 -> None
Head -> 7 -> 3 -> 6 -> 5 -> 4 -> None
Head -> 7 -> 3 -> 6 -> 5 -> None
Head -> None
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->

\section*{\centering\underline{Assignment 40}}

\begin{flushright}
    \textit{Date: 15.12.2023}
\end{flushright}

## Objective

_Write a program to build a simple Student Management System using Python which can perform the following operations: Accept, Display, Search, Delete, Update._

## Source Code

\lstinputlisting[language=Python, style=usual]{./programs/ass40.py}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ py ass40.py
Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 1
Enter Roll No: 101
Enter Name: John Doe
Enter Age: 20
Enter Grade: A
Student added successfully!

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 1
Enter Roll No: 102
Enter Name: Jane Smith
Enter Age: 22
Enter Grade: B
Student added successfully!

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 2

Student List:
Roll No	Name	Age	Grade
101	John Doe	20	A
102	Jane Smith	22	B

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 3
Enter Roll No to search: 101

Student found:
Roll No: 101
Name: John Doe
Age: 20
Grade: A

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 4
Enter Roll No to delete: 102

Student with Roll No 102 deleted successfully.

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 2

Student List:
Roll No	Name	Age	Grade
101	John Doe	20	A

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 5
Enter Roll No to update: 101

Update Student John Doe (101):
Enter new Name: John Updated
Enter new Age: 21
Enter new Grade: A+
Student updated successfully:
Roll No: 101
Name: John Updated
Age: 21
Grade: A+

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 2

Student List:
Roll No	Name	Age	Grade
101	John Updated	21	A+

Student Management System Menu:
1. Accept Student
2. Display Students
3. Search Student
4. Delete Student
5. Update Student
6. Exit
Enter your choice (1-6): 6
Exiting Student Management System. Goodbye!
\end{lstlisting}

\newpage

<!-- ------------------------------------------------------ -->
