---
author: Bhaswar Chakraborty
date: Summer, 2023
title: University of Calcutta
paper: "CMS-A-CC-4-9-P (Algorithms Lab)"
geometry:
- margin=1in
fontsize: 11pt
listings: false
header-includes: |
    \usepackage{tikzit}
    \usepackage{multirow}
    \usepackage{rotating}
    \input{styles.tikzstyles}
    \usepackage[document]{ragged2e}
---

\maketitle
\newpage

\section*{\centering\underline{Assignment 1}}

\begin{flushright}
    \textit{Date: 25.08.23}
\end{flushright}

## Problem Statement

_Write a program which will take a range and find the prime numbers between that range._  
_[use `DataInputStream` class for taking inputs]_

## Source Code

`PrimeNumber.java`
\lstinputlisting[language=Java, style=usual]{programs/ass1/PrimeNumber.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java PrimeNumber
Please enter the lower bound of the range: 10
Please enter the upper bound of the range: 100
Here are the prime numbers between the given range of [10, 100]
11, 13, 15, 17, 19, 23, 25, 29, 31, 35, 37, 41, 43, 47, 49, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97,
\end{lstlisting}

### Set II
\begin{lstlisting}[style=nolinenumber]
assignments $ java PrimeNumber
Please enter the lower bound of the range: 100
Please enter the upper bound of the range: 4
\end{lstlisting}

### Set III
\begin{lstlisting}[style=nolinenumber]
assignments $ java PrimeNumber
Please enter the lower bound of the range: 10.5
Please enter valid integers.
\end{lstlisting}

\newpage
<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 2}}

\begin{flushright}
    \textit{Date: 30.08.23}
\end{flushright}

## Problem Statement

_Write a program which will take a range and find the twin prime numbers between that range._  
_[use `Scanner` class for taking inputs]_

## Source Code

`TwinPrime.java`
\lstinputlisting[language=Java, style=usual]{programs/ass2/TwinPrime.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java TwinPrime
Please enter the lower bound of the range: 1
Please enter the upper bound of the range: 100
Here are the twin prime number pairs between the given range of [1, 100]
(2, 4)
(3, 5)
(4, 6)
(5, 7)
(6, 8)
(7, 9)
(9, 11)
(11, 13)
(13, 15)
(15, 17)
(17, 19)
(23, 25)
(29, 31)
(35, 37)
(41, 43)
(47, 49)
(59, 61)
(71, 73)
\end{lstlisting}

### Set II
\begin{lstlisting}[style=nolinenumber]
assignments $ java TwinPrime
Please enter the lower bound of the range: 450
Please enter the upper bound of the range: 490
Here are the twin prime number pairs between the given range of [450, 490]
(461, 463)
\end{lstlisting}

### Set III
\begin{lstlisting}[style=nolinenumber]
assignments $ java TwinPrime
Please enter the lower bound of the range: 54
Please enter the upper bound of the range: 30
Invalid range. Try inputting a valid upper and lower bounds.
\end{lstlisting}

\newpage
<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 3}}

\begin{flushright}
    \textit{Date: 06.09.23}
\end{flushright}

## Problem Statement

_Write a program which will take a number and find out the factors of that number._

## Source Code

`Factors.java`
\lstinputlisting[language=Java, style=usual]{programs/ass3/Factors.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java Factors
Please enter the number: 64
The factors of 64 are
64
32
16
8
4
2
1
\end{lstlisting}

### Set II
\begin{lstlisting}[style=nolinenumber]
assignments $ java Factors
Please enter the number: 5.4
Please enter valid integers.
\end{lstlisting}

### Set III
\begin{lstlisting}[style=nolinenumber]
assignments $ java Factors
Please enter the number: 5
The factors of 5 are
5
1
\end{lstlisting}

\newpage

<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 4}}

\begin{flushright}
    \textit{Date: 13.09.23}
\end{flushright}

## Problem Statement

_Write a program in java to create `Box` class with parameterized constructor with an object argument to initialize length, breadth and height also create a function volume which returns the volume of the box and print it in main method._

## Source Code

`BoxVolume.java`
\lstinputlisting[language=Java, style=usual]{programs/ass4/BoxVolume.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java BoxVolume
The volume of the box is: 294.538608
\end{lstlisting}

\newpage

<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 5}}

\begin{flushright}
    \textit{Date: 15.09.23}
\end{flushright}

## Problem Statement


\begin{itshape}
    A class called \texttt{MyPoint}, which models a 2D point with x and y coordinates. It contains:
    \begin{itemize}
        \item{Two instance variables x (int) and y (int).}
        \item{A default (or "no-argument" or "no-arg") constructor that construct a point at the default location of (0, 0).}
        \item{A overloaded constructor that constructs a point with the given x and y coordinates.}
        \item{A method \texttt{setXY()} to set both x and y.}
        \item{A method \texttt{getXY()} which returns the x and y.}
        \item{A method called distance(int x, int y) that returns the distance from this point to another point at  the given (x, y) coordinates}
    \end{itemize}
    Also write a test driver (called \texttt{TestMyPoint}) to test all the public methods defined in the class.
\end{itshape}

## Source Code

`TestMyPoint.java`
\lstinputlisting[language=Java, style=usual]{programs/ass5/TestMyPoint.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java TestMyPoint
Coordinates of the first point: (0,0)
Coordinates of the second point: (3,4)
New Coordinates of the first point: (1, 2)
Distance between the first point and the second point: 2.8284271247461903
\end{lstlisting}

\newpage

<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 6}}

\begin{flushright}
    \textit{Date: 11.10.23}
\end{flushright}

## Problem Statement

_Write a program in java that sorts half of element in ascending and rest half of the elements in descending order._

## Source Code

`Sort.java`
\lstinputlisting[language=Java, style=usual]{programs/ass6/Sort.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java Sort
Enter no of elements of the matrix:
7
Enter elements:
12
23
1
76
5
4
9
The matrix is:
12 23 1 76 5 4 9
First half sorted in ascending order is:
1 12 23
Second half sorted in descending order is:
76 9 5 4
The sorted matrix is:
1 12 23 76 9 5 4
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java Sort
Enter no of elements of the matrix:
4
Enter elements:
8
5
3
9
The matrix is:
8 5 3 9
First half sorted in ascending order is:
5 8
Second half sorted in descending order is:
9 3
The sorted matrix is:
5 8 9 3
\end{lstlisting}

\newpage


<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 7}}

\begin{flushright}
    \textit{Date: 12.10.23}
\end{flushright}

## Problem Statement

_Write a program in java that accepts a 2D matrix and prints the matrix with row minimum and column minimum values._

## Source Code

`Min.java`
\lstinputlisting[language=Java, style=usual]{programs/ass7/Min.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java Min
Enter no of rows of the matrix:
3
Enter no of columns of the matrix:
3
Enter elements:
1
2
3
4
5
6
7
8
9
The matrix is:
1 2 3
4 5 6
7 8 9
The matrix with row minimum values is:
1
4
7
The matrix with column minimum values is:
1 2 3

\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java Min
Enter no of rows of the matrix:
2
Enter no of columns of the matrix:
2
Enter elements:
1
5
3
8
The matrix is:
1 5
3 8
The matrix with row minimum values is:
1
3
The matrix with column minimum values is:
1 5
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ java Min
Enter no of rows of the matrix:
4
Enter no of columns of the matrix:
3
Enter elements:
8
5
2
6
3
7
1
3
5
67
3
9
The matrix is:
8 5 2
6 3 7
1 3 5
67 3 9
The matrix with row minimum values is:
2
3
1
3
The matrix with column minimum values is:
1 3 2 
\end{lstlisting}

\newpage


<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 8}}

\begin{flushright}
    \textit{Date: 16.10.23}
\end{flushright}

## Problem Statement

_Write a Java program to find the longest repeating sequence in a string._

_[I/P : str = "acbdfghybdf"
O/P : bdf]_

## Source Code

`LongSquence.java`
\lstinputlisting[language=Java, style=usual]{programs/ass8/LongSquence.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java LongSquence
Enter the string:
abcejhrkjhrabceu
Longest repeating sequence: abce
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java LongSquence
Enter the string:
abcabcabctheabcabctheabc
Longest repeating sequence: abcabctheabc
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ java LongSquence
Enter the string:
eakfberfgbagjrhbgjaenfljaefhuakrfja
Longest repeating sequence: jae
\end{lstlisting}

\newpage

<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 9}}

\begin{flushright}
    \textit{Date: 24.11.23}
\end{flushright}

## Problem Statement

_Write a Java program to display the lower triangular matrix._

## Source Code

`LowerTriangle.java`
\lstinputlisting[language=Java, style=usual]{programs/ass9/LowerTriangle.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java LowerTriangle
Enter rows of the matrix:
3
Enter columns of the matrix:
3
Enter elements:
1
2
3
4
5
6
7
8
9
The matrix is:
1 2 3
4 5 6
7 8 9
The lower triangular matrix is:
1 0 0
4 5 0
7 8 9
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java LowerTriangle
Enter rows of the matrix:
4
Enter columns of the matrix:
5
Lower triangular matrix not possible since it is not a square matri
x
\end{lstlisting}

\newpage


<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 10}}

\begin{flushright}
    \textit{Date: 05.12.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Create a class Employee with the following members:
\begin{itemize}
    \item{Name}
    \item{Address}
    \item{PAN number}
    \item{Base Salary}
\end{itemize}
And the following methods:
\begin{itemize}
    \item{ShowDetails()}
    \item{getGrossSalary()}
\end{itemize}
Along with getters and setters (accessors and mutators) for the member variables.
Derive two classes from it: Manager: with the extra members commission and department.
ProjectLeader: with the extra members projectId and projectAllowance.
Override getGrossSalary to include the commission in case of managers, and show all details for both of the derived class by overriding the
showDetails method.
\end{itshape}

## Source Code

`EmployeeTest.java`
\lstinputlisting[language=Java, style=usual]{programs/ass10/EmployeeTest.java}

## Output

\begin{lstlisting}[style=nolinenumber]
assignments $ java EmployeeTest
Employee Detail:
-------
Name: Sayan Saha
Address: null
PAN Number: 1234
Base Salary: 0
\end{lstlisting}

\newpage


<!-- --------------------------------------------------------------------------------------------------------- -->
\section*{\centering\underline{Assignment 11}}

\begin{flushright}
    \textit{Date: 08.12.23}
\end{flushright}

## Problem Statement

_Create a base class ‘Square’ having instance variable side:double. Initiate variable using constructor, a method ‘getVolume() : double’ that calculates volume and print it. Create a derived class ‘Cylinder’ having instance variable height:double. Initiate variables of both classes through constructor, override method_

## Source Code

`Objects.java`
\lstinputlisting[language=Java, style=usual]{programs/ass11/Objects.java}

## Output

\begin{lstlisting}[style=nolinenumber]
assignments $ java Objects
Square 1:
Assuming you meant a cube, not a 'square'.
The volume of the 'square' is: 125.0
Square 2:
Assuming you meant a cube, not a 'square'.
The volume of the 'square' is: 2744.0
Cylinder 1:
The volume of the cylinder is: 141.37154999999998
Cylinder 2:
The volume of the cylinder is: 785.3975
Cylinder 3:
The volume of the cylinder is: 25.13272

\end{lstlisting}

\newpage


<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 12}}

\begin{flushright}
    \textit{Date: 18.12.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Create a class Student with following operations
    \begin{itemize}
        \item{create parameterized constructor to initialize the objects.}
        \item{create a function isEqual() to check whether the two objects are equal or not which returns the Boolean value and gets two objects.}
        \item{print the result in main method if objects are equal or not}
    \end{itemize}
(take variables as your assumption)
\end{itshape}

## Source Code

`Student.java`
\lstinputlisting[language=Java, style=usual]{programs/ass12/Student.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java Student
Enter details of the first Student object:
Enter name:
Bhaswar
Enter age:
23
Enter roll number:
193
Enter details of the second Student object:
Enter name:
Sagnik
Enter age:
22
Enter roll number:
173
The two students are not equal.

\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java Student
Enter details of the first Student object:
Enter name:
Moumi
Enter age:
84
Enter roll number:
183
Enter details of the second Student object:
Enter name:
Moumi
Enter age:
84
Enter roll number:
183
The two students are equal.
\end{lstlisting}

### Set III

\begin{lstlisting}[style=nolinenumber]
assignments $ java Student
Enter details of the first Student object:
Enter name:
Gibbon Giblet
Enter age:
5
Enter roll number:
143
Enter details of the second Student object:
Enter name:
Giblet Gibbon
Enter age:
43
Enter roll number:
134
The two students are not equal.
\end{lstlisting}

\newpage


<!-- --------------------------------------------------------------------------------------------------------- -->

\section*{\centering\underline{Assignment 13}}

\begin{flushright}
    \textit{Date: 22.12.23}
\end{flushright}

## Problem Statement

_Create an abstract class employee, having its properties and abstract function for calculating net salary and displaying the information. Derive manager and clerk class from this abstract class and implement the abstract method net salary and override the display method._

## Source Code

`Assign13.java`
\lstinputlisting[language=Java, style=usual]{programs/ass13/Assign13.java}

## Output

\begin{lstlisting}[style=nolinenumber]
assignments $ java Assign13
Enter 1 to get details of Manager
Enter 2 to get details of Clerk
Enter 3 to end
Enter choice:
1
Manager Details
Enter name:
Manager Chowdhuri
Enter salary:
10000
Enter unique id:
1
Enter tax payable(in %):
10
Position: Manager
Name: Manager Chowdhuri
Salary: 10000.0
Unique id:1
Tax Payable(in %):10.0
Net Salary:9000.0
Enter 1 to get details of Manager
Enter 2 to get details of Clerk
Enter 3 to end
Enter choice:
2
Clerk Details
Enter name:
Clerk maity
Enter salary:
3000
Enter unique id:
3
Enter tax payable(in %):
12
Position: Clerk
Name: Clerk maity
Salary: 3000.0
Unique id:3
Tax Payable(in %):12.0
Net Salary:2640.0
Enter 1 to get details of Manager
Enter 2 to get details of Clerk
Enter 3 to end
Enter choice:
1
Manager Details
Enter name:
Manager Chowdhuri
Enter salary:
10000
Enter unique id:
1
Enter tax payable(in %):
10
Position: Manager
Name: Manager Chowdhuri
Salary: 10000.0
Unique id:1
Tax Payable(in %):10.0
Net Salary:9000.0
Enter 1 to get details of Manager
Enter 2 to get details of Clerk
Enter 3 to end
Enter choice:
3
\end{lstlisting}

\newpage

<!-- ----------------------------------------------------- -->

\section*{\centering\underline{Assignment 1}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Problem Statement

_Write a program in Java that handles both `ArrayIndexOutOfBoundsException` and `ArithmeticException`._

## Source Code

`ExceptionHandlingExample.java`
\lstinputlisting[language=Java, style=usual]{programs/ass14/ExceptionHandlingExample.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java ExceptionHandlingExample
Enter the index to access in the array:
5
ArrayIndexOutOfBoundsException: Index 5 out of bounds for length 5
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java ExceptionHandlingExample
Enter the index to access in the array:
2
Value at index 2: 3
Enter a divisor for 10:
0
ArithmeticException: / by zero
\end{lstlisting}

\newpage

<!-- ----------------------------------------------------- -->


\section*{\centering\underline{Assignment 2}}

\begin{flushright}
    \textit{Date: 22.11.2023}
\end{flushright}

## Problem Statement

_Write a program to create your own exception as `NegativeSizeException` whenever negative values are put in an array._

## Source Code

`CustomExceptionExample.java`
\lstinputlisting[language=Java, style=usual]{programs/ass15/CustomExceptionExample.java}

## Output

### Set I

\begin{lstlisting}[style=nolinenumber]
assignments $ java CustomExceptionExample
Enter a non-negative size for the array:
-5
Invalid input. Please enter a valid non-negative integer.
-2
Invalid input. Please enter a valid non-negative integer.
3
Array created successfully with size: 3
\end{lstlisting}

### Set II

\begin{lstlisting}[style=nolinenumber]
assignments $ java CustomExceptionExample
Enter a non-negative size for the array:
0
Array created successfully with size: 0
\end{lstlisting}

\newpage

<!-- ----------------------------------------------------- -->


\section*{\centering\underline{Assignment 3}}

\begin{flushright}
    \textit{Date: 29.11.2023}
\end{flushright}

## Problem Statement

_Write a program in Java that handles `StringIndexOutOfBound` Exception._

## Source Code

`StringIndexOutOfBoundsExample.java`
\lstinputlisting[language=Java, style=usual]{programs/ass16/StringIndexOutOfBoundsExample.java}

## Output

\begin{lstlisting}[style=nolinenumber]
assignments $ java StringIndexOutOfBoundsExample
Exception caught: String index out of range: 15
\end{lstlisting}

\newpage

<!-- ----------------------------------------------------- -->


\section*{\centering\underline{Assignment 4}}

\begin{flushright}
    \textit{Date: 04.12.2023}
\end{flushright}

## Problem Statement

_Write a program in Java that handles `NullPointer` Exception._

## Source Code

`NullPointerExceptionExample.java`
\lstinputlisting[language=Java, style=usual]{programs/ass17/NullPointerExceptionExample.java}

## Output

\begin{lstlisting}[style=nolinenumber]
assignments $ java NullPointerExceptionExample
Exception caught: null
\end{lstlisting}

<!-- ----------------------------------------------------- -->


