import java.util.*;

class Min {
    public static void main(String args[]) {
        int r, c;
        Scanner sc = new Scanner(System.in);
        Min ob = new Min();
        System.out.println("Enter no of rows of the matrix:");
        r = sc.nextInt();
        System.out.println("Enter no of columns of the matrix:");
        c = sc.nextInt();
        int a[][] = new int[r][c];
        if (r <= 0 || c <= 0) // invalid input
        {
            System.out.println("Entered no of rows and columns should be greater than 0");
        } else {
            ob.input(a, r, c);
            System.out.println("The matrix is:");
            ob.print(a, r, c);
            System.out.println("The matrix with row minimum values is:");
            ob.minrow(a, r, c);
            System.out.println("The matrix with column minimum values is:");
            ob.mincol(a, r, c);
        }
    }

    void input(int a[][], int r, int c)// takes input of matrix
    {
        int i, j;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter elements:");
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                a[i][j] = sc.nextInt();
            }
        }
    }

    void print(int a[][], int r, int c) // prints matrix
    {
        int i, j;
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

    }

    void minrow(int a[][], int r, int c) // find row minimum values
    {
        int minr, i, j;
        for (i = 0; i < r; i++) {
            minr = a[i][0];
            for (j = 0; j < c; j++) {
                if (a[i][j] < minr)
                    minr = a[i][j];
            }
            System.out.println(minr);
        }
    }

    void mincol(int a[][], int r, int c) // find column minimum values
    {
        int minc, i, j;
        for (j = 0; j < c; j++) {
            minc = a[0][j];
            for (i = 0; i < r; i++) {
                if (a[i][j] < minc)
                    minc = a[i][j];
            }
            System.out.print(minc + " ");
        }
    }

}