import java.util.Scanner;

public class ExceptionHandlingExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            // Handling ArrayIndexOutOfBoundsException
            int[] numbers = {1, 2, 3, 4, 5};
            System.out.println("Enter the index to access in the array:");
            int index = scanner.nextInt();
            int result = numbers[index];
            System.out.println("Value at index " + index + ": " + result);

            // Handling ArithmeticException
            System.out.println("Enter a divisor for 10:");
            int divisor = scanner.nextInt();
            int quotient = 10 / divisor;
            System.out.println("Quotient of 10 divided by " + divisor + ": " + quotient);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("ArithmeticException: " + e.getMessage());
        } catch (Exception e) {
            // Generic catch block for any other exceptions
            System.out.println("An unexpected error occurred: " + e.getMessage());
        } finally {
            scanner.close();
        }
    }
}

