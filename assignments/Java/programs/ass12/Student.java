import java.util.*;

class Student{
    private String name;
    private int age;
    private String rollNumber;
    public Student(String name, int age, String rollNumber){
        this.name = name;
        this.age = age;
        this.rollNumber = rollNumber;
    }
    public boolean isEqual(Student otherStudent){
        return this.name.equals(otherStudent.name) &&
               this.age == otherStudent.age &&
               this.rollNumber.equals(otherStudent.rollNumber);
    }

    // Main method to test the isEqual function
    public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("Enter details of the first Student object:");
	System.out.println("Enter name:");
	String nm=sc.nextLine();
	System.out.println("Enter age:");
	int age=sc.nextInt();
	sc.nextLine();
	System.out.println("Enter roll number:");
	String roll=sc.nextLine();
	System.out.println("Enter details of the second Student object:");
	System.out.println("Enter name:");
	String nm1=sc.nextLine();
	System.out.println("Enter age:");
	int age1=sc.nextInt();
	sc.nextLine(); 
	System.out.println("Enter roll number:");
	String roll1=sc.nextLine();
    Student student1 = new Student(nm,age,roll);
    Student student2 = new Student(nm1,age1,roll1);
      if (student1.isEqual(student2))
            System.out.println("The two students are equal.");
      else
        System.out.println("The two students are not equal.");
    }
}
