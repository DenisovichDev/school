import java.io.*;

class PrimeNumber {

    private static boolean checkIfPrime(int n) {
        if (n <= 1) return false;
        int sqrti = (int) Math.sqrt(n);
        for (int i = 2; i < sqrti; i++) {
            if (n % i == 0) return false;
        }
        return true;
    }

    private static void findPrimeNumbersInRange(int l, int u) {
        System.out.println("Here are the prime numbers between the given range of [" + l + ", " + u + "]");
        for (int i = l; i <= u; i++) {
            if (checkIfPrime(i)) {
                System.out.print(i);
                System.out.print(", ");
            }
        }
        System.out.print("\n");
    }

    public static void main(String args[]) {
        DataInputStream inputStream = new DataInputStream(System.in);

        try {
            System.out.print("Please enter the lower bound of the range: ");
            int lowerBound = Integer.parseInt(inputStream.readLine());
            System.out.print("Please enter the upper bound of the range: ");
            int upperBound = Integer.parseInt(inputStream.readLine());
            if (lowerBound >= upperBound) {
                System.out.println("Invalid range. Try inputting a valid upper and lower bounds."); 
                return;
            }
            findPrimeNumbersInRange(lowerBound, upperBound);
        } catch (IOException | NumberFormatException e) {
            System.out.println("Please enter valid integers.");
        }
    }
}
