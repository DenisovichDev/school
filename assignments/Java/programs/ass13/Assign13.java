import java.util.*;

abstract class Employee {
	String name;
	double salary;
	int uni;

	public Employee(String name, double salary, int uni) {
		this.name = name;
		this.salary = salary;
		this.uni = uni;
	}

	public double getSal() {
		return salary;
	}

	// Abstract method for calculating net salary
	public abstract double calculateNetSalary();

	// Display information method
	public void display() {
		System.out.println("Name: " + name);
		System.out.println("Salary: " + salary);
		System.out.println("Unique id:" + uni);
	}
}

class Manager extends Employee {
	double tax;

	public Manager(String name, double salary, int uni) {
		super(name, salary, uni);
		this.tax = tax;
	}

	// setting details
	public void setter() {
		Scanner s1 = new Scanner(System.in);
		System.out.println("Enter name:");
		name = s1.nextLine();
		System.out.println("Enter salary:");
		salary = s1.nextDouble();
		System.out.println("Enter unique id:");
		uni = s1.nextInt();
		System.out.println("Enter tax payable(in %):");
		tax = s1.nextDouble();
	}

	// Implementation of abstract method for calculating net salary for a manager
	public double calculateNetSalary() {
		double d;
		d = getSal();
		return (d - (d * (tax / 100)));
	}

	public void display() {
		System.out.println("Position: Manager");
		super.display();
		System.out.println("Tax Payable(in %):" + tax);
		System.out.println("Net Salary:" + calculateNetSalary());
	}
}

class Clerk extends Employee {
	double tax;

	public Clerk(String name, double salary, int uni) {
		super(name, salary, uni);
		this.tax = tax;
	}

	public void set() {
		Scanner s2 = new Scanner(System.in);
		System.out.println("Enter name:");
		name = s2.nextLine();
		System.out.println("Enter salary:");
		salary = s2.nextDouble();
		System.out.println("Enter unique id:");
		uni = s2.nextInt();
		System.out.println("Enter tax payable(in %):");
		tax = s2.nextDouble();
	}

	// Implementation of abstract method for calculating net salary for a clerk
	public double calculateNetSalary() {
		double d;
		d = getSal();
		return (d - (d * (tax / 100)));
	}

	public void display() {
		System.out.println("Position: Clerk");
		super.display();
		System.out.println("Tax Payable(in %):" + tax);
		System.out.println("Net Salary:" + calculateNetSalary());
	}
}

class Assign13 {
	public static void main(String[] args) {
		String nm = " ";
		double sal = 0.0;
		int uni = 0;
		int ch;
		int d = 1;
		while (d == 1) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter 1 to get details of Manager");
			System.out.println("Enter 2 to get details of Clerk");
			System.out.println("Enter 3 to end");
			System.out.println("Enter choice:");
			ch = sc.nextInt();
			switch (ch) {
				case 1:
					System.out.println("Manager Details");
					Manager ob = new Manager(nm, sal, uni);
					ob.setter();
					ob.calculateNetSalary();
					ob.display();
					break;
				case 2:
					System.out.println("Clerk Details");
					Clerk ob1 = new Clerk(nm, sal, uni);
					ob1.set();
					ob1.calculateNetSalary();
					ob1.display();
					break;
				case 3:
					System.exit(0);
					break;
				default:
					System.out.println("Wrong choice");
			}
		}
	}
}
