import java.util.Scanner;

class TwinPrime {

    private static boolean checkIfPrime(int n) {
        if (n <= 1) return false;
        int sqrti = (int) Math.sqrt(n);
        for (int i = 2; i < sqrti; i++) {
            if (n % i == 0) return false;
        }
        return true;
    }

    private static void findTwinPrimeNumbersInRange(int l, int u) {
        System.out.println("Here are the twin prime number pairs between the given range of [" + l + ", " + u + "]");
        for (int i = l; i < u - 1; i++) {
            if (checkIfPrime(i) && checkIfPrime(i + 2)) {
                System.out.println("(" + i + ", " + (i + 2) + ")");
            }
        }
        
    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter the lower bound of the range: ");
        int lowerBound = scanner.nextInt();
        System.out.print("Please enter the upper bound of the range: ");
        int upperBound = scanner.nextInt();
        if (lowerBound >= upperBound) {
            System.out.println("Invalid range. Try inputting a valid upper and lower bounds."); 
            return;
        }
        findTwinPrimeNumbersInRange(lowerBound, upperBound);
    }
}
