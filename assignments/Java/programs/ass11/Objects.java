class Square {
    double side;
    
    public Square(double side) {
        this.side = side;
    }

    public void getVolume() {
        System.out.println("Assuming you meant a cube, not a 'square'.");
        double vol = this.side * this.side * this.side;
        System.out.println("The volume of the 'square' is: " + vol);
    }
}

class Cylinder extends Square {
    double height;
    
    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public void getVolume() {
        double vol = 3.14159 * this.side * this.side * this.height;
        System.out.println("The volume of the cylinder is: " + vol);
    }
}

class Objects {
    public static void main(String[] args) {
        Square s1 = new Square(5);
        Square s2 = new Square(14);

        Cylinder c1 = new Cylinder(3, 5);
        Cylinder c2 = new Cylinder(5, 10);
        Cylinder c3 = new Cylinder(2, 2);

        System.out.println("Square 1:");
        s1.getVolume();
        System.out.println("Square 2:");
        s2.getVolume();
        
        System.out.println("Cylinder 1:");
        c1.getVolume();
        System.out.println("Cylinder 2:");
        c2.getVolume();
        System.out.println("Cylinder 3:");
        c3.getVolume();
    }
}
