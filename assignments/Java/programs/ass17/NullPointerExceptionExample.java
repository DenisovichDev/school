public class NullPointerExceptionExample {
    public static void main(String[] args) {
        try {
            String str = null;
            int length = getStringLength(str);
            System.out.println("Length of the string: " + length);
        } catch (NullPointerException e) {
            System.out.println("Exception caught: " + e.getMessage());
        }
    }

    private static int getStringLength(String str) {
        return str.length();
    }
}
