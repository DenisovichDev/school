import java.io.*;

class Factors {

    private static void findFactors(int n) {
        System.out.println("The factors of " + n + " are");
        for (int i = n; i >= 1; i--) {
            if (n % i == 0)
                System.out.println(i);
        }
    }

    public static void main(String args[]) {
        DataInputStream inputStream = new DataInputStream(System.in);

        try {
            System.out.print("Please enter the number: ");
            int num = Integer.parseInt(inputStream.readLine());

            findFactors(num);
        } catch (IOException | NumberFormatException e) {
            System.out.println("Please enter valid integers.");
        }
    }
}
