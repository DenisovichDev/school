import java.util.*;

class Sort {

  public static void main(String args[]) {
    int r, k;
    Scanner sc = new Scanner(System.in);
    Sort ob = new Sort();
    System.out.println("Enter no of elements of the matrix:");
    r = sc.nextInt();
    int a[] = new int[r];
    if (r <= 0) {
      System.out.println("Number of elements should be more than 0");
    } else {
      ob.input(a, r);
      System.out.println("The matrix is:");
      ob.print(a, 0, r);
      k = (int) r / 2; //mid index
      ob.sortasce(a, k - 1);
      System.out.println("First half sorted in ascending order is:");
      ob.print(a, 0, k);
      ob.sortdesc(a, k, r);
      System.out.println("Second half sorted in descending order is:");
      ob.print(a, k, r);
      System.out.println("The sorted matrix is:");
      ob.print(a, 0, r);
    }
  }

  void input(int a[], int r) {
    int i;
    Scanner sc = new Scanner(System.in);
    System.out.println("Enter elements:");
    for (i = 0; i < r; i++) {
      a[i] = sc.nextInt();
    }
  }

  void print(int a[], int m, int r) {
    int i;
    for (i = m; i < r; i++) {
      System.out.print(a[i] + " ");
    }
    System.out.println(" ");
  }

  void sortasce(int a[], int n) {
    int i, j, temp;
    for (i = 0; i <= n; i++) {
      for (j = i + 1; j <= n; j++) {
        temp = 0;
        if (a[i] > a[j]) {
          temp = a[i];
          a[i] = a[j];
          a[j] = temp;
        }
      }
    }
  }

  void sortdesc(int a[], int m, int n) { //sorting in descending order
    int i, j, temp;
    for (i = m; i < n; i++) {
      for (j = i + 1; j < n; j++) {
        temp = 0;
        if (a[i] < a[j]) {
          temp = a[i];
          a[i] = a[j];
          a[j] = temp;
        }
      }
    }
  }
}
