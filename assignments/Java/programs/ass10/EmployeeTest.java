class Employee {
    protected String name;
    protected String address;
    protected int PAN;
    protected int baseSalary;

    protected void setName(String newName) {
        this.name = newName;
    }

    protected void setAddress(String address) {
        this.address = address;
    }

    protected void setPAN(int PAN) {
        this.PAN = PAN;
    }

    protected void setBaseSalary(int baseSalary) {
        this.baseSalary = baseSalary;
    }

    protected void showDetails() {
        System.out.println("Employee Detail:\n-------");
        System.out.println("Name: " + this.name);
        System.out.println("Address: " + this.address);
        System.out.println("PAN Number: " + this.PAN);
        System.out.println("Base Salary: " + this.baseSalary);
    }

    protected void getGrossSalary() {
        System.out.println("The gross salary: " + this.baseSalary);
    }

}

class Manager extends Employee {
    protected float commissionPercent;
    protected String department;
    double gross = this.baseSalary + this.commissionPercent * 0.01 * this.baseSalary;

    Manager(int commissionPercent) {
        this.commissionPercent = commissionPercent;
    }

    protected void getGrossSalary() {
        System.out.println("The gross salary: " + this.gross);
    }

    protected void showDetails() {
        System.out.println("Employee Detail:\n-------");
        System.out.println("Name: " + this.name);
        System.out.println("Address: " + this.address);
        System.out.println("PAN Number: " + this.PAN);
        System.out.println("Base Salary: " + this.baseSalary);
        System.out.println("Total Salary: " + this.gross);
    }

}

class ProjectLeader extends Employee {
    protected int projectID;
    protected int projectAllowance;

    protected void showDetails() {
        System.out.println("Employee Detail:\n-------");
        System.out.println("Name: " + this.name);
        System.out.println("Address: " + this.address);
        System.out.println("PAN Number: " + this.PAN);
        System.out.println("Base Salary: " + this.baseSalary);
        System.out.println("Project ID: " + this.projectID);
        System.out.println("Project Allowance: " + this.projectAllowance);
    }
}

class EmployeeTest {
    public static void main(String args[]) {
        Employee sayan = new Employee();
        sayan.setName("Sayan Saha");
        sayan.setPAN(1234);

        sayan.showDetails();
    }
}