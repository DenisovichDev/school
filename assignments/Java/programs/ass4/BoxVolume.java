class Box {
    private double length;
    private double breadth;
    private double height;

    public Box(double length, double breadth,double height) {
        this.length = length;
        this.breadth = breadth;
        this.height = height;
    }

    public double volume() {
        return this.length * this.breadth * this.height;
    }
}

class BoxVolume {
    public static void main(String args[]) {
        Box b = new Box(5.46, 7.98, 6.76);
        System.out.println("The volume of the box is: " + b.volume());
    }
}
