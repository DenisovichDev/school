class MyPoint {
    private int x;
    private int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int[] getXY() {
        return new int[]{this.x, this.y};
    }

    public double distance(int x1, int y1) {
        int dx = this.x - x1;
        int dy = this.y - y1;
        return Math.sqrt(dx * dx + dy * dy);
    }
}

class TestMyPoint {
    public static void main(String[] args) {
        MyPoint point1 = new MyPoint();
        System.out.println("Coordinates of the first point: ("+point1.getXY()[0]+ ","+point1.getXY()[1]+")");
        MyPoint point2 = new MyPoint(3, 4);
        System.out.println("Coordinates of the second point: ("+point2.getXY()[0]+","+point2.getXY()[1]+")");
        point1.setXY(1, 2);
        System.out.println("New Coordinates of the first point: (" + point1.getXY()[0] + ", " + point1.getXY()[1] + ")");
        double distance = point1.distance(point2.getXY()[0], point2.getXY()[1]);
        System.out.println("Distance between the first point and the second point: " + distance);
    }
}
