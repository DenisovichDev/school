import java.util.*;

class LowerTriangle {
    public static void main(String args[]) {
        int r, c, i, j;
        Scanner sc = new Scanner(System.in);
        LowerTriangle ob = new LowerTriangle();
        System.out.println("Enter rows of the matrix:");
        r = sc.nextInt();
        System.out.println("Enter columns of the matrix:");
        c = sc.nextInt();
        int a[][] = new int[r][c];
        if (r == c) {
            ob.input(a, r, c);
            System.out.println("The matrix is:");
            ob.print(a, r, c);
            ob.lowertriangle(a, r, c);
            System.out.println("The lower triangular matrix is:");
            ob.print(a, r, c);
        } else {
            System.out.println("Lower triangular matrix not possible since it is not a square matrix");
        }
    }

    void input(int a[][], int r, int c) // takes input of the matrix
    {
        int i, j;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter elements:");
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                a[i][j] = sc.nextInt();
            }
        }
    }

    void print(int a[][], int r, int c) // prints the matrix
    {
        int i, j;
        for (i = 0; i < r; i++) {
            for (j = 0; j < c; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    void lowertriangle(int a[][], int r, int c) // perform lower triangular matrix operation
    {
        int count = 1, i, j;
        for (i = 0; i < r; i++) {
            for (j = c - 1; j >= count; j--) {
                a[i][j] = 0;
            }
            count++;
        }
    }
}