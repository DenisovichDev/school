import java.util.Scanner;

class NegativeSizeException extends Exception {
    public NegativeSizeException(String message) {
        super(message);
    }
}

public class CustomExceptionExample {
    public static void main(String[] args) {
        try {
            int arraySize = getArraySizeFromUser();
            if (arraySize < 0) {
                throw new NegativeSizeException("Array size cannot be negative.");
            }

            int[] array = new int[arraySize];
            System.out.println("Array created successfully with size: " + arraySize);
        } catch (NegativeSizeException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private static int getArraySizeFromUser() {
        Scanner scanner = new Scanner(System.in);
        int size;
        do {
            System.out.println("Enter a non-negative size for the array:");
            while (!scanner.hasNextInt()) {
                System.out.println("Invalid input. Please enter a valid non-negative integer.");
                scanner.next(); // consume the invalid input
            }
            size = scanner.nextInt();
        } while (size < 0);

        scanner.close();
        return size;
    }
}

