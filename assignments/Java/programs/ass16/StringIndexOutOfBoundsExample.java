public class StringIndexOutOfBoundsExample {
    public static void main(String[] args) {
        try {
            String str = "Hello, World!";
            char character = getCharacterAtIndex(str, 15);
            System.out.println("Character at index 15: " + character);
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("Exception caught: " + e.getMessage());
        }
    }

    private static char getCharacterAtIndex(String str, int index) {
        return str.charAt(index);
    }
}
