---
author: Bhaswar Chakraborty
title: University of Calcutta
subtitle: B.Sc. Computer Science
date: "CMS-A-CC-5-11-P (DBMS Lab)"
geometry:
- margin=1in
fontsize: 11pt
listings: false
header-includes: |
    \usepackage{tikzit}
    \input{styles.tikzstyles}
    \usepackage[document]{ragged2e}
    \usepackage{array}
    \usepackage{enumitem}
...

\maketitle
\newpage

\section*{\centering\underline{Assignment 1}}

\begin{flushright}
    \textit{Date: 18.09.23}
\end{flushright}

## Question 1

_Create the following tables with appropriate constraints  using SQL command._

Table: Member

Column Name | Data Type | Description
------------|-----------|------------
`member_id` | `int(5)` | Unique Member ID
`member_name` | `varchar(50)` | Name of the library member
`member_address` | `varchar(50)` | Address of the member
`acc_open_date` | `date` | Date of membership
 `membership_type` | `varchar(20)` | Type of the membership, such as 'Lifetime', 'Annual', 'Half Yearly', 'Quarterly'
 `fees_paid` | `int(4)` | Membership fees paid
 `max_books_allowed` | `int(2)` | Total number of books that can be issued to the member
 `penalty_amount` | `decimal(7, 2)` | Penalty amount due

### Constraints

a. `member_id` : Primary Key
a. `member_name` : NOT NULL
a. `membership_type` : 'Lifetime', 'Annual', 'Half Yearly', 'Quarterly'
a. `max_books_allowed` < 7
a. `penalty_amount` < 1000


Table: Books

Column Name | Data Type | Description
------------|-----------|------------
`book_no` | `int(6)` | Book identification number
`book_name` | `varchar(30)` | Name of the book
`author_name` | `varchar(30)` | Author of the book
`cost` | `decimal(7, 2)` | Cost of the book
`category` | `char(10)` | Category like Science, Fiction etc.

### Constraints

a. `book_no` : Primary Key
a. `book_name` : NOT NULL
a. `category` : Science, Database, System, Others

Table: Books

Column Name | Data Type | Description
------------|-----------|------------
`lib_issue_id` | `int(10)` | Library book issue no.
 `book_no` | `int(6)` | The ID of book which is issued
 `member_id` | `int(5)` | Member that issued the book
 `issue_date` | `date` | Date of issue
 `return_date` | `date` | Return date

### Constraints

a. `lib_issue_id` : Primary Key
a. `book_no` : Foreign Key
a. `member_id` : Foreign Key

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass1/a1q1.sql}

## Output

![Schema of table "Member"](./figures/a1q1f1.png)

![Schema of table "Books"](./figures/a1q1f2.png)

![Schema of table "Issue"](./figures/a1q1f3.png)


## Question 2

_Insert the following data to the appropriate table using SQL command._

### Table: Member

<!-- member_id | member_name |  member_address |  acc_open_date | membership_type | fees_paid |  max_books_allowed |  penalty_amount -->

    
\begin{tabular}{|p{3.2em}|l|p{3.5em}|p{5em}|p{5em}|l|p{4em}|p{4em}|}
    \hline
     member \_id & member\_name & member \_address & acc\_open \_date & membership \_type & fees\_paid & max \_books \_allowed & penalty \_amount \\
    \hline
    1 & Sayantan Sinha & Pune & 2010-12-10 & Lifetime & 2000 & 6 & 50 \\
    2 & Abhirup Sarkar & Kolkata & 2011-01-19 & Annual & 1400 & 3 & 0 \\
    3 & Ritesh Bhuniya & Gujarat & 2011-02-20 & Quarterly & 350 & 2 & 100 \\
    4 & Paresh Sen & Tripura & 2011-03-21 & Half Yearly & 700 & 1 & 200 \\
    5 & Sohini Halder & Birbhum & 2011-04-11 & Lifetime & 2000 & 6 & 10 \\
    6 & Suparna Biswas & Kolkata & 2011-04-12 & Half Yearly & 700 & 1 & 0 \\
    7 & Suranjana Basu & Purulia & 2011-06-30 & Annual & 1400 & 3 & 50 \\
    8 & Arpita Roy & Kolkata & 2011-07-31 & Half Yearly & 700 & 1 & 0 \\
    \hline
\end{tabular}

### Table: Books

\begin{tabular}{|l|l|l|l|l|}
    \hline
    book\_no & book\_name & author\_name & cost & category \\
    \hline
    101 & The C Programing Language & Brian Kernighan & 450 & Others \\
    102 & Oracle - Complete Reference & Loni & 550 & Database \\
    103 & Visual Basic 10 & BPB & 700 & Others \\
    104 & Mastering SQL & Loni & 450 & Database \\
    105 & PL SQL Reference & Scott Urman & 760 & Database \\
    106 & UNIX & Sumitava Das & 300 & System \\
    107 & Optics & Ghatak & 600 & Science \\
    108 & Data Structure & G.S. Baluja & 350 & Others \\
    \hline
\end{tabular}

### Table: Issue

\begin{tabular}{|l|l|l|l|l|}
    \hline
    libe\_issue\_id & book\_no & member\_id & issue\_date & return\_date \\
    \hline
    7001 & 101 & 1 & 2011-01-10 &  \\
    7002 & 102 & 2 & 2011-01-23 &  \\
    7003 & 104 & 1 & 2011-02-01 &  \\
    7004 & 104 & 2 & 2011-03-15 &  \\
    7005 & 101 & 4 & 2011-04-04 &  \\
    7006 & 108 & 5 & 2011-04-12 &  \\
    7007 & 101 & 8 & 2011-08-01 &  \\
    \hline
\end{tabular}

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass1/a1q2.sql}

## Output

![Data of table "Member"](./figures/a1q2f1.png)

![Data of table "Books"](./figures/a1q2f2.png)

![Data of table "Issue"](./figures/a1q2f3.png)

\newpage

\section*{\centering\underline{Assignment 2}}

\begin{flushright}
    \textit{Date: 25.09.23}
\end{flushright}


## Question 1

_Retrieve the Name of Book and Cost of the book with the Maximum cost._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q1.sql}

## Output

![](./figures/a2q1f1.png)

<!-- -------- -->

## Question 2

_Calculate the Minimum cost, Average cost, and Total cost value in BOOKS table and Rename the resulting attributes._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q2.sql}

## Output

![](./figures/a2q2f1.png)

<!-- -------- -->

## Question 3

_Retrieve the Name and ID of Members who issued a book between 26th January 2011 and 14th April 2011._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q3.sql}

## Output

![](./figures/a2q3f1.png)

<!-- -------- -->

## Question 4

_Retrieve Book Name, Author Name, and Category whose Category is not ‘OTHERS’._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q4.sql}

## Output

![](./figures/a2q4f1.png)

<!-- -------- -->

## Question 5

_Retrieve the Book name and Author Name where the 5th letter of the Author name is ’t’._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q5.sql}

## Output

![](./figures/a2q5f1.png)

<!-- -------- -->

## Question 6

_How many Books are available whose Cost is greater than 350._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q6.sql}

## Output

![](./figures/a2q6f1.png)

<!-- -------- -->

## Question 7

_How many different Authors' names are available in the BOOKS table._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q7.sql}

## Output

![](./figures/a2q7f1.png)

<!-- -------- -->

## Question 8

\begin{itshape}
Calculate the following Numeric functions.
\begin{enumerate}[label=(\alph*)]
    \item What is the absolute value of -167.
    \item Calculate $8^6$.
    \item Round up to 2 decimal points (134.56789).
    \item What is the square root of 144.
    \item Floor and Ceil value of 13.15.
\end{enumerate}
\end{itshape}

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q8.sql}

## Output

![](./figures/a2q8f1.png)

<!-- -------- -->

## Question 9

_Extract Year, Month, Day from System Date._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q9.sql}

## Output

![](./figures/a2q9f1.png)

<!-- -------- -->

## Question 10

_What is the greatest value between 4, 5, and 17._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q10.sql}

## Output

![](./figures/a2q10f1.png)

<!-- -------- -->

## Question 11

_What is the Least value between '4', '5', and '17' and Express why resulting value of last two queries are same._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q11.sql}

## Output

![](./figures/a2q11f1.png)

<!-- -------- -->

## Question 12

_Extract 4 letters from 3rd position of this word 'INFOSYS'._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q12.sql}

## Output

![](./figures/a2q12f1.png)

<!-- -------- -->

## Question 13

_What is the ASCII value of 'a' and 'S'._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q13.sql}

## Output

![](./figures/a2q13f1.png)

<!-- -------- -->

## Question 14

_What is Length of this word 'INFOSYS' AND change 'S' with 'T'._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q14.sql}

## Output

![](./figures/a2q14f1.png)

![](./figures/a2q14f2.png)

<!-- -------- -->

## Question 15

_Retrieve the Names and Address of the Members who belong to Kolkata._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q15.sql}

## Output

![](./figures/a2q15f1.png)

<!-- -------- -->

## Question 16

_Retrieve the Name of Books, where Cost prices are between 300 and 500._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q16.sql}

## Output

![](./figures/a2q16f1.png)

<!-- -------- -->

## Question 17

_List the Name of the Members whose Membership type is “HALF YEARLY”._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q17.sql}

## Output

![](./figures/a2q17f1.png)

<!-- -------- -->

## Question 1

_List the Name of the Members who opened their account in the year 2011._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q18.sql}

## Output

![](./figures/a2q18f1.png)

<!-- -------- -->

## Question 19

_Retrieve the Penalty Amount of the Members who have taken the book “LET US C”._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q19.sql}

## Output

![](./figures/a2q19f1.png)

<!-- -------- -->

## Question 20

_Retrieve the number of Max books allowed to a Member, who has issued books on January._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q20.sql}

## Output

![](./figures/a2q20f1.png)

<!-- -------- -->

## Question 21

_Give the Names of the Members who have not issued any books._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q21.sql}

## Output

![](./figures/a2q21f1.png)

<!-- -------- -->

## Question 22

_Give the Name and Category of the books whose cost is not recorded._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q22.sql}

## Output

![](./figures/a2q22f1.png)

<!-- -------- -->

## Question 23

_List all the books that are written by Author ‘’Loni’’ and has Price less than 600._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q23.sql}

## Output

![](./figures/a2q23f1.png)

<!-- -------- -->

## Question 24

_List the Issue details for the Books that are not returned yet._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q24.sql}

## Output

![](./figures/a2q24f1.png)

<!-- -------- -->

## Question 25

_List all the Books that belong to any one of the following categories Science, Database._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q25.sql}

## Output

![](./figures/a2q25f1.png)

<!-- -------- -->

## Question 26

_List all the Members in the descending order of Penalty due on them._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q26.sql}

## Output

![](./figures/a2q26f1.png)

<!-- -------- -->

## Question 27

_List all the Books in ascending order of category and descending order of price._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q27.sql}

## Output

![](./figures/a2q27f1.png)

<!-- -------- -->

## Question 28

_List all the Books that contain the word ‘SQL’ in the Name of the Book._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q28.sql}

## Output

![](./figures/a2q28f1.png)

<!-- -------- -->

## Question 29

_List all the Members whose Name starts with S._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q29.sql}

## Output

![](./figures/a2q29f1.png)

<!-- -------- -->

## Question 30

_List all the Members whose Name starts with S or A and contains the letter T in it._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q30.sql}

## Output

![](./figures/a2q30f1.png)

<!-- -------- -->

## Question 31

_List the Entire Book name in INIT CAP and Author Name in UPPER case in the descending order of the Book Name._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q31.sql}

## Output

![](./figures/a2q31f1.png)

<!-- -------- -->

## Question 32

_List the data in the book table with category data displayed as ‘D’ for Database, ‘S’ for Science, ‘R’ for RDBMS and ‘O’ for all the others._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q32.sql}

## Output

![](./figures/a2q32f1.png)

<!-- -------- -->

## Question 33

_List all the Members that became the Member in the year 2011._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass2/a2q33.sql}

## Output

![](./figures/a2q33f1.png)

<!-- -------- -->

\newpage

\section*{\centering\underline{Assignment 3}}

\begin{flushright}
    \textit{Date: 03.10.23}
\end{flushright}

## Question 1

_List the various CATEGORIES and COUNT OF BOOKS in each category._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q1.sql}

## Output

![](./figures/a3q1f1.png)

<!-- -------- -->

## Question 2

_List the BOOK_NO and the NUMBER OF TIMES the Book is issued in the descending order of COUNT._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q2.sql}

## Output

![](./figures/a3q2f1.png)

<!-- -------- -->

## Question 3

_Display the MEMBER ID and the NO OF BOOKS for each member that has issued more than 1 book._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q3.sql}

## Output

![](./figures/a3q3f1.png)

<!-- -------- -->

## Question 4

_Display the MEMBER ID, BOOK NO and NO OF TIMES the same book is issued by the MEMBER in the descending order of count._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q4.sql}

## Output

![](./figures/a3q4f1.png)

<!-- -------- -->

## Question 5

_Select all information of Book with 2nd Maximum Cost._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q5.sql}

## Output

![](./figures/a3q5f1.png)

<!-- -------- -->

## Question 6

_Copy the MEMBER, BOOKS and ISSUE tables and MEMBER_COPY, BOOKS_COPY, ISSUE_COPY respectively._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q6.sql}

## Output

![Table: "member_copy"](./figures/a3q6f1.png)

![Table: "books_copy"](./figures/a3q6f2.png)

![Table: "issue_copy"](./figures/a3q6f3.png)

<!-- -------- -->

## Question 7

_Create a table named QUERY1 with two attributes as BOOK_NO, ISSUE_DATE and copy all information about BOOK_NO and ISSUE_DATE from ISSUE table to QUERY1 table._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q7.sql}

## Output

![](./figures/a3q7f1.png)

<!-- -------- -->

## Question 8

_Create and store BOOK_NAME, AUTHOR_NAME and COST to QUERY2 table where cost is greater than 300._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q8.sql}

## Output

![](./figures/a3q8f1.png)

<!-- -------- -->

## Question 9

_Insert BOOK_NAME, AUTHOR_NAME and COST to QUERY2 table where cost is less than 300._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q9.sql}

## Output

![](./figures/a3q9f1.png)

<!-- -------- -->

## Question 10

_ADD an attribute name AVAILABLE with data type NUMBER (5) in the BOOKS_COPY table and fill up the new attribute with data._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q10.sql}

## Output

![](./figures/a3q10f1.png)

<!-- -------- -->

## Question 11

_Change the Data type of CATEGORY attribute to VARCHAR2 (15) in BOOKS_COPY table._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q11.sql}

## Output

![](./figures/a3q11f1.png)

<!-- -------- -->

## Question 12

_Create a SAVE POINT after that do some delete or update operation and roll back the previous scenario._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q12.sql}

## Output

![](./figures/a3q12f1.png)

<!-- -------- -->

## Question 13

_Create a New USER, Log in to new USER; provide permission of selection on BOOK table to the new USER. Then provide DBA privilege to the new USER._

## SQL Commands

\lstinputlisting[language=SQL, style=usual]{./programs/ass3/a3q13.sql}

## Output

![](./figures/a3q13f1.png)

<!-- -------- -->

<!-- ----------------------- AKC ---------------------- -->

\newpage

\section*{\centering\underline{Assignment 1}}

\begin{flushright}
    \textit{Date: 20.09.2023}
\end{flushright}

## Problem Statement

\begin{itshape}
Create a table “Student” in MySQL data base containing the fields \texttt{name}, \texttt{roll}, \texttt{city}, \texttt{email} and \texttt{date\_of\_birth} through PHP script. Write PHP script that will do the following:
\begin{enumerate}[label=(\alph*)]
    \item{Insert 5 records into the same table and display them through PHP script.}
    \item{Display the records in a separate table of all students who are born in between 01/01/2000 to 31/12/2005.}
    \item{Design a login table to take username and password. A user being authenticate if the username and password exists in the MYSQL database “Login” table. Design a login form and check valid username and password to display student details using PHP script. Also write a PHP script to change username and password depending on old password.}
    \item{Design a HTML form to take a valid email from user and search the corresponding student from the database using PHP script and display the details of that student in tabular form.}
    \item{Insert, delete and update records in a table.}
\end{enumerate}
\end{itshape}

## Source Code

The file structure created for the problem is shown below.

```
.
|--- backend
|   |--- dashboard.php
|   |--- delete.php
|   |--- insert.php
|   |--- login.php
|   |--- update.php
|--- delete.html
|--- entries.php
|--- index.html
|--- insert.html
|--- pswd.html
|--- q2.php
|--- style.css
|--- update.html
```

The files and their corresponding code is shown below

`./backend/dashboard.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/backend/dashboard.php}

`./backend/delete.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/backend/delete.php}

`./backend/insert.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/backend/insert.php}

`./backend/login.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/backend/login.php}

`./backend/update.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/backend/update.php}

`./delete.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass1/delete.html}

`./entries.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/entries.php}

`./index.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass1/index.html}

`./insert.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass1/insert.html}

`./pswd.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass1/pswd.html}

`./q2.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass1/q2.php}

`./style.css`
\lstinputlisting[style=usual]{./programs/akc/ass-style.css}

`./update.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass1/update.html}

## Structure of the Tables

`DESC validity;`\break
![](./figures/akc/db/q1t1.png)\break

`DESC student;`\break
![](./figures/akc/db/q1t2.png)\break

## Records of the Tables

`SELECT * FROM validity;`\break
![](./figures/akc/db/q1d1.png)\break

`SELECT * FROM student;`\break
![](./figures/akc/db/q1d2.png)\break


## Output Screenshots

![Page from assignment 1](./figures/akc/a1f1.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f2.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f3.png){ width=300px }

![Page from assignment 1](./figures/akc/a1f4.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f5.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f6.png){ width=300px }

![Page from assignment 1](./figures/akc/a1f7.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f8.png){ width=300px }

![Page from assignment 1](./figures/akc/a1f9.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f10.png){ width=300px }

![Page from assignment 1](./figures/akc/a1f11.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f12.png){ height=300px }

![Page from assignment 1](./figures/akc/a1f13.png){ width=300px }


<!-- ---------------------- -->

\newpage

\section*{\centering\underline{Assignment 2}}

\begin{flushright}
    \textit{Date: 04.10.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Suppose a table “Food\_Details” containing the fields Food\_items and Price\_per\_item. Design a menu to select different food items and input the quantity ordered by customer. Generate a bill containing Customer name, Food items, quantity, price and Total price. Net price should be the total price plus 15\% GST of total price. Create another table “Customer\_details” containing the fields customer\_name, total\_amount\_paid, date\_of\_payment. Design a menu to select different food items and input the quantity ordered by customer. Generate a bill containing Customer name, Food items, quantity, and Total price. Also insert the record i.e. customer name, total amount paid and date of payment in “Customer\_details” table. Net price should be the total price plus 15\% GST of total price.
\end{itshape}

## Source Code

The file structure created for the problem is shown below.

```
.
|--- bill.php
|--- index.php
|--- style.css
```

The files and their corresponding code is shown below

`./bill.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass2/bill.php}

`./index.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass2/index.php}

`./style.css`
\lstinputlisting[style=usual]{./programs/akc/ass-style.css}

## Structure of the Tables

`DESC customer_details;`\break
![](./figures/akc/db/q2t1.png)\break

`DESC food_details;`\break
![](./figures/akc/db/q2t2.png)\break

## Records of the Tables

`SELECT * FROM customer_details;`\break
![](./figures/akc/db/q2d1.png)\break

`SELECT * FROM food_details;`\break
![](./figures/akc/db/q2d2.png)\break

## Output Screenshots

![Page from assignment 2](./figures/akc/a2f1.png){ height=300px }

![Page from assignment 2](./figures/akc/a2f2.png){ height=300px }


<!-- ---------------------- -->


<!-- ---------------------- -->

\newpage

\section*{\centering\underline{Assignment 3}}

\begin{flushright}
    \textit{Date: 17.10.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Suppose a table “Salary” contained the fields EmpName, BasicPay, HRA, DA and Professional\_tax. Record should exists only for EmpName and BasicPay. Design a HTML form to take input for HRA percentage, DA percentage, and Professional tax. Write PHP scripts that will do the following:
PHP script containing the following fields.
\begin{enumerate}[label=(\alph*)]
    \item{Generate the DA, HRA and Professional tax of every employee and store them into Salary table with respect to every employee.}
    \item{Design a HTML form to take inputs from user and store the records into a table “Employee” using}
    \item{Design a login form that will accept employee id and password to authenticate the entries.}
    \item{Display the names and salary of employees in each department.}
\end{enumerate}
\end{itshape}

## Source Code

The file structure created for the problem is shown below.

```
.
|--- backend.php
|--- display.php
|--- index.html
|--- update_salary.html
|--- update_salary.php
|--- style.css
```

The files and their corresponding code is shown below

`./backend.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass3/backend.php}

`./display.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass3/display.php}

`./index.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass3/index.html}

`./update_salary.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass3/update_salary.html}

`./update_salary.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass3/update_salary.php}

`./style.css`
\lstinputlisting[style=usual]{./programs/akc/ass-style.css}


## Structure of the Tables

`DESC employee;`\break
![](./figures/akc/db/q3t1.png)\break

`DESC salary;`\break
![](./figures/akc/db/q3t2.png)\break

## Records of the Tables

`SELECT * FROM employee;`\break
![](./figures/akc/db/q3d1.png)\break

`SELECT * FROM salary;`\break
![](./figures/akc/db/q3d2.png)\break

## Output Screenshots

![Page from assignment 3](./figures/akc/a3f1.png){ height=300px }

![Page from assignment 3](./figures/akc/a3f2.png){ height=300px }

![Page from assignment 3](./figures/akc/a3f3.png){ width=300px }

![Page from assignment 3](./figures/akc/a3f4.png){ height=300px }

![Page from assignment 3](./figures/akc/a3f5.png){ width=300px }

![Page from assignment 3](./figures/akc/a3f6.png){ width=300px }

![Page from assignment 3](./figures/akc/a3f7.png){ height=400px }

![Page from assignment 3](./figures/akc/a3f8.png){ height=300px }

![Page from assignment 3](./figures/akc/a3f9.png){ width=300px }

![Page from assignment 3](./figures/akc/a3f10.png){ height=400px }


<!-- ---------------------- -->


<!-- ---------------------- -->

\newpage

\section*{\centering\underline{Assignment 4}}

\begin{flushright}
    \textit{Date: 22.11.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Design a HTML form to take inputs from user and store the records into a table “Employee” using PHP script containing the following fields. Employee table consists of the following attributes:
\begin{enumerate}[label=(\alph*)]
    \item{Ename : textbox}
    \item{Address: textarea}
    \item{Phno: textbox}
    \item{Salary: textbox}
    \item{Category(GEN,SC,ST,OBC): radio buttons}
    \item{Language: checkbox (Multiple languages have must be concatenated into a string separated by commas and then store into database like “Bengali, English, Hindi”)}
\end{enumerate}
Write a PHP script to display the employee records of the above “Employee” table order by user’s choice field. The field have to be choices through drop down menu containing all fields of the table. Perform all database operations like select, insert, delete and update through PHP.
\end{itshape}

## Source Code

The file structure created for the problem is shown below.

```
.
|--- backend.php
|--- index.html
|--- style.css
```

The files and their corresponding code is shown below

`./backend.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass4/backend.php}

`./index.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass4/index.html}

`./style.css`
\lstinputlisting[style=usual]{./programs/akc/ass-style.css}

## Structure of the Tables

`DESC employee;`\break
![](./figures/akc/db/q4t1.png)\break

## Records of the Tables

`SELECT * FROM employee;`\break
![](./figures/akc/db/q4d1.png)\break

## Output Screenshots

![Page from assignment 4](./figures/akc/a4f1.png){ width=300px }

![Page from assignment 4](./figures/akc/a4f2.png){ width=300px }

![Page from assignment 4](./figures/akc/a4f3.png){ width=300px }

![Page from assignment 4](./figures/akc/a4f4.png){ width=300px }

![Page from assignment 4](./figures/akc/a4f5.png){ width=300px }

![Page from assignment 4](./figures/akc/a4f6.png){ width=300px }

![Page from assignment 4](./figures/akc/a4f7.png){ height=300px }


<!-- ---------------------- -->


<!-- ---------------------- -->

\newpage

\section*{\centering\underline{Assignment 5}}

\begin{flushright}
    \textit{Date: 29.11.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Create a table “Sports” in MySQL data base containing the fields Players, TestRuns, ODIRuns, T20IRuns. Insert and display the record with total individual runs of every player (like Test Runs + ODI Runs + T20I Runs) through PHP script. Display the record of highest run scorer in Test or T20I or ODI according to user’s choice through a drop down menu. Write PHP script to insert, update and delete records through PHP script.
\end{itshape}

## Source Code

The file structure created for the problem is shown below.

```
.
|--- backend
|   |--- dashboard.php
|   |--- delete.php
|   |--- insert.php
|   |--- score.php
|   |--- update.php
|--- delete.html
|--- display.php
|--- index.html
|--- insert.html
|--- q2.php
|--- update.html
|--- style.css
```

The files and their corresponding code is shown below

`./backend/dashboard.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/backend/dashboard.php}

`./backend/delete.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/backend/delete.php}

`./backend/insert.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/backend/insert.php}

`./backend/score.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/backend/score.php}

`./backend/update.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/backend/update.php}

`./delete.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass5/delete.html}

`./display.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/display.php}

`./index.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass5/index.html}

`./insert.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass5/insert.html}

`./q2.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass5/q2.php}

`./update.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass5/update.html}

`./style.css`
\lstinputlisting[style=usual]{./programs/akc/ass-style.css}


## Structure of the Tables

`DESC sports;`\break
![](./figures/akc/db/q5t1.png)\break

## Records of the Tables

`SELECT * FROM sports;`\break
![](./figures/akc/db/q5d1.png)\break

## Output Screenshots

![Page from assignment 5](./figures/akc/a5f1.png){ height=300px }

![Page from assignment 5](./figures/akc/a5f2.png){ height=300px }

![Page from assignment 5](./figures/akc/a5f3.png){ width=400px }

![Page from assignment 5](./figures/akc/a5f4.png){ width=400px }

![Page from assignment 5](./figures/akc/a5f5.png){ width=400px }

![Page from assignment 5](./figures/akc/a5f6.png){ width=400px }

![Page from assignment 5](./figures/akc/a5f7.png){ height=400px }

![Page from assignment 5](./figures/akc/a5f8.png){ width=300px }

![Page from assignment 5](./figures/akc/a5f9.png){ height=400px }

![Page from assignment 5](./figures/akc/a5f10.png){ width=300px }

![Page from assignment 5](./figures/akc/a5f11.png){ height=300px }

![Page from assignment 5](./figures/akc/a5f12.png){ width=300px }


<!-- ---------------------- -->


<!-- ---------------------- -->

\newpage

\section*{\centering\underline{Assignment 6}}

\begin{flushright}
    \textit{Date: 19.12.23}
\end{flushright}

## Problem Statement

\begin{itshape}
Create MySQL database table “Dept“ with field dno, dame where dno is a primary key, and create another database table “Employee“ with field eno, ename, city, salary, join\_date, dno where eno is a primary key and dno is Foreign Key references Dept table and include other constraints like not null, check etc.

Employee(\underline{\textbf{eno}}, ename, city, salary, join\_date, dno) \break
Department(\underline{\textbf{dno}}, dname)

Write PHP script to do the following:
\begin{enumerate}[label=(\alph*)]
    \item{Insert, Update, Delete operations using PHP.}
    \item{Create a login form that will validate the credentials and the session using session variables.}
    \item{Display the name of the employees who have been working for the past five years.}
    \item{Display the name of the employee and the department name who receives minimum salary in}
each department.
\end{enumerate}
\end{itshape}

## Source Code

The file structure created for the problem is shown below.

```
.
|--- backend.php
|--- department.html
|--- department.php
|--- employee.html
|--- employee.php
|--- index.php
|--- min_salary.php
|--- og_employee.php
|--- style.css
```

The files and their corresponding code is shown below

`./backend.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass6/backend.php}

`./department.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass6/department.html}

`./department.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass6/department.php}

`./employee.html`
\lstinputlisting[language=HTML, style=usual]{./programs/akc/ass6/employee.html}

`./employee.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass6/employee.php}

`./index.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass6/index.php}

`./min_salary.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass6/min_salary.php}

`./og_employee.php`
\lstinputlisting[language=PHP, style=usual]{./programs/akc/ass6/og_employee.php}

`./style.css`
\lstinputlisting[style=usual]{./programs/akc/ass-style.css}


## Structure of the Tables

`DESC dept;`\break
![](./figures/akc/db/q6t1.png)\break

`DESC employee;`\break
![](./figures/akc/db/q6t2.png)\break

## Records of the Tables

`SELECT * FROM dept;`\break
![](./figures/akc/db/q6d1.png)\break

`SELECT * FROM employee;`\break
![](./figures/akc/db/q6d2.png)\break

## Output Screenshots

![Page from assignment 6](./figures/akc/a6f1.png){ height=300px }

![Page from assignment 6](./figures/akc/a6f2.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f3.png){ height=300px }

![Page from assignment 6](./figures/akc/a6f4.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f5.png){ height=300px }

![Page from assignment 6](./figures/akc/a6f6.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f7.png){ height=300px }

![Page from assignment 6](./figures/akc/a6f8.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f9.png){ height=300px }

![Page from assignment 6](./figures/akc/a6f10.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f11.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f12.png){ height=400px }

![Page from assignment 6](./figures/akc/a6f13.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f14.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f15.png){ width=300px }

![Page from assignment 6](./figures/akc/a6f16.png){ height=300px }

![Page from assignment 6](./figures/akc/a6f17.png){ height=400px }


<!-- ---------------------- -->


