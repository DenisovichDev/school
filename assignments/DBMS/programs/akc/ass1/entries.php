<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DB Entries</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
	<?php 
		$usr = "root";
        $pswd = "";
        $dbname = "demo";
        $connect = new mysqli("localhost",$usr,$pswd,$dbname);
        if (!$connect)
        {
            die("Cannot Connect");
        }
	?>
	<table>
		<tr>
			<th>Roll No.</th>
			<th>Name</th>
			<th>City</th> 
			<th>Email</th> 
			<th>Date of Birth</th> 
		</tr>
		<?php 
			$sql = "SELECT * FROM `student`;";
			$result = $connect->query($sql);
			if($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$roll = $row['roll_no'];
					$name = $row['name'];
					$city = $row['city'];
					$email = $row['email'];
					$dob = $row['date_of_birth'];
		?>
					<tr>
						<td><?php echo $roll ?></td>
						<td><?php echo $name ?></td>
						<td><?php echo $city ?></td>
						<td><?php echo $email ?></td>
						<td><?php echo $dob ?></td>
					</tr>
		<?php 
				}
			}
			$connect->close();
		?>
	</table>
</body>
</html>
