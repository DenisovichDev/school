<link rel="stylesheet" href="../ass-style.css">
<?php
    if (isset($_POST['valid'])) {
        $user = "root";
        $pass = "";
        $dbname = "ass2";
        $connect = new mysqli("localhost", $user, $pass, $dbname);
        if (!$connect) {
            die("Connection failed");
        }
        $sql = "select * from Food_details;";
        $result = $connect->query($sql);
        $total = 0.0;
        $name = $_POST["name"];
        echo "<h3>Bill for '$name':</h3><div class=\"container monospace\"><table>";
        echo "<tr style='font-weight: bold'><td>Item</td><td>Quantity</td><td>Total price</td></tr>";
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $food = str_replace(" ", "_", $row["Food_items"]);
                $price = $row["Price_per_item"];
                $total += $_POST[$food] * $price;
                if ($_POST[$food] > 0) {
                    echo "<tr><td>".str_replace("_", " ", $food)."</td><td>".intval($_POST[$food])."</td><td>".$_POST[$food] * $price."</td></tr>";
                }
            }
        }
        echo "</table><br>";
        echo "<div class=\"customer\">";
        echo "Tax: ".(0.15*$total)."<br>";
        $total += $total * 0.15;
        echo "Total: ". $total."<br>";
        $date = date("Y-m-d");
        echo "Bill generated on ".$date."<br>";
        $sql = "INSERT INTO Customer_details (total_amount_paid, date_of_payment, customer_name) VALUES ($total, '$date', '$name');";
        echo "</div>";
        echo"<br>";
        echo "<div class=\"customer\">";
        if (($result = $connect->query($sql)) == TRUE) {
            if ($connect->affected_rows > 0) {
                echo "Customer data updated with bill amount";
            } else {
                echo "Problem encountered while updating user data";
            }
        }
        echo "</div>";
    }
?>
