<!DOCTYPE html>
<html>
    <head>
        <title>Menu</title>
        <link rel="stylesheet" href="../ass-style.css">
    </head>
    <body>
        <h1>Menu</h1>
        <div class="container monospace">
        <form method="post" class="alternate container" action="bill.php" >
        <input required type="text" placeholder="Name" name="name" />
        <table>
            <tr>
                <th>Food Item</th>
                <th>Price</th>
                <th>Quantity</th>
            </tr>
            <?php
                $user = "root";
                $pass = "";
                $dbname = "ass2";
                $connect = new mysqli("localhost", $user, $pass, $dbname);
                if (!$connect) {
                    die("Connection failed");
                }
                $sql = "select * from Food_details;";
                $result = $connect->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $food = $row["Food_items"];
                        echo "<tr>";
                        echo "<td>".$row['Food_items']."</td>";
                        echo "<td>".$row["Price_per_item"]."</td>";
                        echo "<td><input required type='number' class=\"short\" value=0 name='$food' /></td>";
                        echo "</tr>";
                    }
                }
            ?>
        </table>
        <div class="center">
            <input type="submit" value="Generate bill" name="valid">
        </div>
        </form>
        </div>
    </body>
</html>
