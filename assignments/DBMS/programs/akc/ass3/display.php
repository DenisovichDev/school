<link rel="stylesheet" href="../ass-style.css">
<?php
    $usr = "root";
    $pass = "";
    $dbname = "ass3";
    $connect = new mysqli("localhost", $usr, $pass, $dbname);
    if(!$connect) {
        die("Connection failed");
    }
    echo "<h1>Employee List</h1>";
    echo "<div class=\"container\"><table><tr><th>Name</th><th>Basic pay</th><th>HRA</th><th>DA</th><th>Professional tax</th><th>Total</th></tr>";
    $sql = "select * from Salary;";
    $result = $connect->query($sql);
    if($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $total = $row['BasicPay'] + $row['HRA'] + $row['DA'] + $row['Professional_tax'];
            echo "<tr><td>".$row['EmpName']."</td><td>". $row['BasicPay']."</td><td>". $row['HRA']."</td><td>". $row['DA']."</td><td>". $row['Professional_tax']."</td><td>".$total."</tr>";
        }
    }
    echo "</table></div>";
?>
