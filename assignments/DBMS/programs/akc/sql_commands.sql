/* SQL Queries */


/* ass1: */
create table validity (name varchar(20), password varchar(20));

insert into validity ("admin", "abcd");

CREATE TABLE student (
    roll_no INT(5) PRIMARY KEY,
    name VARCHAR(30),
    city VARCHAR(20),
    email VARCHAR(30),
    date_of_birth DATE
);

INSERT INTO student (roll_no, name, city, email, date_of_birth)
VALUES
    (1, 'John Cleese', 'New York', 'john.cleese@example.com', '1939-10-27'),
    (2, 'Eric Idle', 'Los Angeles', 'eric.idle@example.com', '1943-03-29'),
    (3, 'Michael Palin', 'Chicago', 'michael.palin@example.com', '1943-05-05'),
    (4, 'Terry Jones', 'San Francisco', 'terry.jones@example.com', '1942-02-01'),
    (5, 'Graham Chapman', 'Seattle', 'graham.chapman@example.com', '1941-01-08'),
    (6, 'Silly McLaughington', 'London', 'silly.mclaughington@example.com', '2001-04-15'),
    (7, 'Chucklina Gigglesworth', 'Paris', 'chucklina.gigglesworth@example.com', '2003-08-22'),
    (8, 'Whoopee Snortington', 'Berlin', 'whoopee.snortington@example.com', '2005-02-10');


/* ass3: */
CREATE TABLE Salary (
    EmpName VARCHAR(50) NOT NULL,
    BasicPay DECIMAL(10, 2) NOT NULL,
    HRA DECIMAL(10, 2) NOT NULL,
    DA DECIMAL(10, 2) NOT NULL,
    Professional_tax DECIMAL(10, 2) NOT NULL,
    PRIMARY KEY (EmpName)
);

INSERT INTO Salary (EmpName, BasicPay) VALUES
    ('Lin-Manuel Miranda', 50000.00),
    ('Leslie Odom Jr.', 60000.00),
    ('Phillipa Soo', 55000.00),
    ('Daveed Diggs', 52000.00),
    ('Renée Elise Goldsberry', 48000.00),
    ('Jonathan Groff', 65000.00),
    ('Christopher Jackson', 70000.00),
    ('Okieriete Onaodowan', 48000.00),
    ('Anthony Ramos', 55000.00),
    ('Jasmine Cephas Jones', 60000.00);

CREATE TABLE Employee (
    employee_id INT PRIMARY KEY,
    password VARCHAR(255) NOT NULL
);

INSERT INTO Employee (employee_id, password) VALUES
    (1, 'password123'),
    (2, 'securePass456'),
    (3, 'examplePass789');


/* ass4: */
CREATE TABLE Employee (
    Phno INT PRIMARY KEY,
    Ename VARCHAR(50) NOT NULL,
    Address VARCHAR(100),
    Language VARCHAR(50),
    Salary DECIMAL(10, 2),
    Category VARCHAR(50)
);

INSERT INTO Employee (Phno, Ename, Address, Language, Salary, Category) VALUES
    (5551111, 'George Washington', 'Mount Vernon', 'English', 75000.00, 'SC'),
    (5552222, 'Thomas Jefferson', 'Monticello', 'English', 70000.00, 'GEN'),
    (5553333, 'Benjamin Franklin', 'Philadelphia', 'English', 80000.00, 'OBC'),
    (5554444, 'Abigail Adams', 'Quincy', 'English', 65000.00, 'ST'),
    (5555555, 'John Adams', 'Quincy', 'English', 72000.00, 'GEN');


/* ass7: */
-- Create Dept table
CREATE TABLE Dept (
    dno INT PRIMARY KEY,
    dname VARCHAR(50) NOT NULL
);

-- Create Employee table
CREATE TABLE Employee (
    eno INT PRIMARY KEY,
    ename VARCHAR(50) NOT NULL,
    city VARCHAR(50),
    salary DECIMAL(10, 2),
    join_date DATE,
    dno INT,
    CONSTRAINT fk_dno FOREIGN KEY (dno) REFERENCES Dept(dno),
    CONSTRAINT chk_salary CHECK (salary >= 0)
);

-- Insert dummy data into Dept table
INSERT INTO Dept (dno, dname) VALUES
    (1, 'HR'),
    (2, 'IT'),
    (3, 'Finance'),
    (4, 'Marketing');

-- Insert dummy data into Employee table with varied join_date within the past 10 years
INSERT INTO Employee (eno, ename, city, salary, join_date, dno) VALUES
    (101, 'John Doe', 'New York', 60000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 1),
    (102, 'Jane Smith', 'San Francisco', 70000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 2),
    (103, 'Bob Johnson', 'Chicago', 55000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 1),
    (104, 'Alice Brown', 'Los Angeles', 80000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 3),
    (105, 'Chris Wilson', 'Boston', 75000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 2),
    (106, 'Eva Martinez', 'Seattle', 65000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 3),
    (107, 'David Lee', 'Austin', 72000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 4),
    (108, 'Grace White', 'Denver', 68000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 2),
   (109, 'Sam Taylor', 'Miami', 60000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 4),
    (110, 'Olivia Adams', 'Philadelphia', 78000.00, DATE_SUB(NOW(), INTERVAL FLOOR(RAND() * 3650) DAY), 1);



 
