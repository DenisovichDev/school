<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../ass-style.css">
        <title>Highest Score in Category</title>
    </head>
    <body>
        <h1>Highest Score in Category</h1>
        <form method="post" action="backend/score.php">
            <label>Enter the category</label>
            <select name="category" id="category">
              <option value="TestRuns">Test</option>
              <option value="ODIRuns">ODI</option>
              <option value="T20IRuns">T20</option>
            </select>
            <input type="submit" name="submit" value="Submit"><br>
        </form>

    </body>
</html>
