<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="../ass-style.css">
        <title>Display</title>
    </head>
    <body class="container">
        <?php 
		$usr = "root";
        $pswd = "";
        $dbname = "ass5";
        $connect = new mysqli("localhost",$usr,$pswd,$dbname);
        if (!$connect)
        {
            die("Cannot Connect");
        }
	?>
    <h1>All Players</h1><br>
	<table>
		<tr>
			<th>Player Name</th>
			<th>Test Runs</th>
			<th>ODI Runs</th> 
			<th>T20 Runs</th> 
		</tr>
		<?php 
			$sql = "SELECT * FROM `sports`;";
			$result = $connect->query($sql);
			if($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$player = $row['Players'];
					$test = $row['TestRuns'];
					$odi = $row['ODIRuns'];
					$t20 = $row['T20IRuns'];
		?>
					<tr>
						<td><?php echo $player ?></td>
						<td><?php echo $test ?></td>
						<td><?php echo $odi ?></td>
						<td><?php echo $t20 ?></td>
					</tr>
		<?php 
				}
			}
			$connect->close();
		?>
	</table>
    </body>
</html>
