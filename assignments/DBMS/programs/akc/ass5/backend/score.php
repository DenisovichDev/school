<link rel="stylesheet" href="../../ass-style.css">
<?php
/* SELECT Players, ODIRuns */ 
/* FROM Sports */ 
/* WHERE ODIRuns = (SELECT MAX(ODIRuns) FROM Sports); */

$usr = "root";
$pswd = "";
$dbname = "ass5";
$connect = new mysqli("localhost",$usr,$pswd,$dbname);
if (!$connect) {
    die("Cannot Connect");
}
$category = $_POST["category"];

// Query to retrieve player name and ODI runs
$sql = "SELECT Players, $category FROM Sports WHERE $category = (SELECT MAX($category) FROM Sports)";
$result = $connect->query($sql);

echo "<h3>Highest scorer in category: " . $_POST["category"] . "</h3><div class=\"container\">";
if ($result->num_rows > 0) {
    // Output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Player Name: " . $row["Players"]. ": " . $row["$category"]. "<br>";
    }
} else {
    echo "No results found";
}
echo "</div>";
$connect->close();
?>
