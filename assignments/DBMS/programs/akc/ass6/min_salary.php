<link rel="stylesheet" href="../ass-style.css">
<?php
    session_start();
    if(!array_key_exists("valid", $_SESSION)) {
        echo "Please authenticate first";
        die();
    }
    $usr = "root";
    $pswd = "";
    $dbname = "ass7";
    $connect = new mysqli("localhost", $usr, $pswd, $dbname);
    if (!$connect) {
        die("Cannot Connect");
    }
    echo "<h3>Minimum Salary in Departments</h3>";
    echo "<div class=\"container\"><table><tr><th>Department name</th><th>Employee</th><th>Salary</th></tr>";
    $sql = "select dept.dname as dname, min(employee.ename) as ename, min(employee.salary) as salary from dept join employee on dept.dno = employee.dno group by dept.dno;";
    if (($result = $connect->query($sql)) == TRUE) {
        if ($result->num_rows > 0) {
            while($row = mysqli_fetch_array($result)) {
                echo "<tr><td>".$row['dname']."</td><td>".$row["ename"]."</td><td>".$row["salary"]."</td></tr>";
            }
        }
    }
    echo "</table></div>"
?>
