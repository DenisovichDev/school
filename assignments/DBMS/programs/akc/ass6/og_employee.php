<link rel="stylesheet" href="../ass-style.css">
<?php
    session_start();
    if(!array_key_exists("valid", $_SESSION)) {
        echo "Please authenticate first";
        die();
    }
    $usr = "root";
    $pswd = "";
    $dbname = "ass7";
    $connect = new mysqli("localhost", $usr, $pswd, $dbname);
    if (!$connect) {
        die("Cannot Connect");
    }
    echo"<h3>Employees who joined in the last 5 years</h3>";
    echo "<div class=\"container\"><table><tr><th>Employee</th></tr>";
    $sql = "select ename from employee where join_date <= curdate() and join_date >= date_sub(curdate(), interval 5 year);;";
    if (($result = $connect->query($sql)) == TRUE) {
        if ($result->num_rows > 0) {
            while($row = mysqli_fetch_array($result)) {
                echo "<tr><td>".$row["ename"]."</td></tr>";
            }
        }
    }
    echo "</table></div>"
?>
