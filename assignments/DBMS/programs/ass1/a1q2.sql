INSERT INTO member VALUES
(1, 'Sayantan Sinha', 'Pune', '2010-12-10', 'Lifetime', 2000, 6, 50),
(2, 'Abhirup Sarkar', 'Kolkata', '2011-01-19', 'Annual', 1400, 3, 0),
(3, 'Ritesh Bhuniya', 'Gujarat', '2011-02-20', 'Quarterly', 350, 2, 100),
(4, 'Paresh Sen', 'Tripura', '2011-03-21', 'Half Yearly', 700, 1, 200),
(5, 'Sohini Halder', 'Birbhum', '2011-04-11', 'Lifetime', 2000, 6, 10),
(6, 'Suparna Biswas', 'Kolkata', '2011-04-12', 'Half Yearly', 700, 1, 0),
(7, 'Suranjana Basu', 'Purulia', '2011-06-30', 'Annual', 1400, 3, 50),
(8, 'Arpita Roy', 'Kolkata', '2011-07-31', 'Half Yearly', 700, 1, 0);

INSERT INTO books VALUES 
(101, 'The C Programing Language', 'Brian Kernighan', 450, 'Others'),
(102, 'Oracle - Complete Reference', 'Loni', 550, 'Database'),
(103, 'Visual Basic 10', 'BPB', 700, 'Others'),
(104, 'Mastering SQL', 'Loni', 450, 'Database'),
(105, 'PL SQL Reference', 'Scott Urman', 760, 'Database'),
(106, 'UNIX', 'Sumitava Das', 300, 'System'),
(107, 'Optics', 'Ghatak', 600, 'Science'),
(108, 'Data Structure', 'G.S. Baluja', 350, 'Others');

INSERT INTO issue VALUES 
(7001, 101, 1, '2011-01-10', ''),
(7002, 102, 2, '2011-01-23', ''),
(7003, 104, 1, '2011-02-01', ''),
(7004, 104, 2, '2011-03-15', ''),
(7005, 101, 4, '2011-04-04', ''),
(7006, 108, 5, '2011-04-12', ''),
(7007, 101, 8, '2011-08-01', '');

SELECT * FROM member;
SELECT * FROM books;
SELECT * FROM issue;
