CREATE DATABASE library;

CREATE TABLE member (member_id int(5),
					member_name varchar(30) NOT NULL,
					member_address varchar(50),
					acc_open_date date,
					membership_type varchar(20),
					fees_paid int(4),
					max_books_allowed int(2),
					penalty_amount decimal(7,2)
                    );

ALTER TABLE member ADD CONSTRAINT m1 PRIMARY KEY(member_id);
ALTER TABLE member ADD CONSTRAINT m2 CHECK (membership_type IN ('Lifetime', 'Annual', 'Half Yearly', 'Quarterly'));
ALTER TABLE member ADD CONSTRAINT m3 CHECK (max_books_allowed<7);
ALTER TABLE member ADD CONSTRAINT m4 CHECK (penalty_amount<=1000);

CREATE TABLE books (book_no int(6),
					book_name varchar(30) NOT NULL,
					author_name varchar(30),
					cost decimal(7,2),
					category char(10)
					);

ALTER TABLE books ADD CONSTRAINT b1 PRIMARY KEY (book_no);
ALTER TABLE books ADD CONSTRAINT b2 CHECK (category IN ('Science', 'Database', 'System', 'Others'));

CREATE TABLE issue (lib_issue_id int(10),
					book_no int(6),
					member_id int(5),
					issue_date date,
					return_date date
					);

ALTER TABLE issue ADD CONSTRAINT i1 PRIMARY KEY (lib_issue_id);
ALTER TABLE issue ADD CONSTRAINT i2 FOREIGN KEY (book_no) REFERENCES books (book_no);
ALTER TABLE issue ADD CONSTRAINT i3 FOREIGN KEY (member_id) REFERENCES member (member_id);

DESC member;
DESC books;
DESC issue;

