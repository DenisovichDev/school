SELECT m.member_name, m.max_books_allowed
FROM member m
JOIN issue i ON m.member_id = i.member_id
WHERE MONTH(i.issue_date) = 1;
