SELECT
    book_name,
    author_name,
    cost,
    CASE
        WHEN category = 'Database' THEN 'D'
        WHEN category = 'Science' THEN 'S'
        WHEN category = 'RDBMS' THEN 'R'
        ELSE 'O'
    END AS formatted_category
FROM books;
