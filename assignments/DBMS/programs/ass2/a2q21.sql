SELECT m.member_name
FROM member m
LEFT JOIN issue i ON m.member_id = i.member_id
WHERE i.member_id IS NULL;
