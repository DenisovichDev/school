SELECT CONCAT(UPPER(SUBSTRING(book_name, 1, 1)), LOWER(SUBSTRING(book_name, 2))) AS initcap_book_name,
       UPPER(author_name) AS upper_author_name
FROM books
ORDER BY initcap_book_name DESC;
