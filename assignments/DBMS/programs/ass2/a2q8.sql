SELECT ABS(-167) AS "abs(-167)",
POW(8, 6) AS "pow(8, 6)",
ROUND(134.56789, 2) AS "round(134.56789, 2)",
SQRT(144) AS "sqrt(144)",
FLOOR(13.15) AS "floor(13.15)",
CEIL(13.15) AS "ceil(13.15)";
