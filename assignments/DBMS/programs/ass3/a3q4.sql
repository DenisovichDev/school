SELECT member_id, book_no, COUNT(*) AS issue_count
FROM issue
GROUP BY member_id, book_no
ORDER BY issue_count DESC;
