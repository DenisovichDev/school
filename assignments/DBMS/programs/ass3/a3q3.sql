SELECT member_id, COUNT(*) AS books_issued_count
FROM issue
GROUP BY member_id
HAVING COUNT(*) > 1;
