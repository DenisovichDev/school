-- Create new user
CREATE USER 'new_user'@'localhost' IDENTIFIED BY 'password';

-- Login to the user, in terminal we can write the command
-- mysql -u new_user -p

-- providing SELECT privileges to the new user in books table
GRANT SELECT ON library.books TO 'new_user'@'localhost';

-- providing DBA privileges to the new user
GRANT ALL PRIVILEGES ON *.* TO 'new_user'@'localhost' WITH GRANT OPTION;
