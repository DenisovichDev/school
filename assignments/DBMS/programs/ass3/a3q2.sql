SELECT book_no, COUNT(*) AS issue_count
FROM issue
GROUP BY book_no
ORDER BY issue_count DESC;
