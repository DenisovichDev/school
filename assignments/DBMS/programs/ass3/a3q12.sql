-- Start a transaction and set a savepoint
START TRANSACTION;
SAVEPOINT my_savepoint;

-- Perform delete or update operations (replace this with your actual operations)
DELETE FROM BOOKS_COPY WHERE COST > 500;

-- rollback to savepoint
ROLLBACK TO my_savepoint;

-- If everything is successful, commit the transaction
COMMIT;

-- Show the books_copy table
SELECT * FROM BOOKS_COPY;
