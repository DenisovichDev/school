class Example {
    int i;
    static int count = 0;
    public Example(int i) {
        this.i = i;
        count++;
    }
}

class TestInstanceCount {
    public static void main(String args[]) {
        Example e1 = new Example(5);
        Example e2 = new Example(5);
        Example e3 = new Example(5);
        Example e4 = new Example(5);
        Example e5 = new Example(5);
        Example e6 = new Example(5);

        System.out.println("Number of Example object instances is " + Example.count);
    }
}
