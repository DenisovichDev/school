class Student {
    String name;
    static int age = 10;
    static {
        System.out.println("This is the static block");
        age = 50;
    }
    Student(String name) {
        this.name = name;
    }
    void incrAge() {
        this.age++;
    }
    static void addTen() {
       age += 10;
    }
}

class Practice {
    public static void main(String[] args) {
        Student s1 = new Student("Moumin");
        Student s2 = new Student("Bhaswar");
        System.out.println(s1.name + " " + s1.age);
        System.out.println(s2.name + " " + s2.age);
        s1.incrAge();
        System.out.println(s1.name + " " + s1.age);
        System.out.println(s2.name + " " + s2.age);

        s1.addTen();
        System.out.println(s1.name + " " + s1.age);
        System.out.println(s2.name + " " + s2.age);
    }
}
