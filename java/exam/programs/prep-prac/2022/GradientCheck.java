import java.util.*;

class MyPoint {
    int x;
    int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    void getData() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter x coordinate: ");
        this.x = sc.nextInt();
        System.out.println("Enter y coordinate: ");
        this.y = sc.nextInt();
    }

    void printData() {
        System.out.println(this.x + ", " + this.y);
    }

    static double linesegment(MyPoint m, MyPoint n) {
        return (double)(m.y - n.y) / (double)(m.x - n.x);
    }
}

class GradientCheck {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(7, 8);
        p1.printData();
        p1.getData();
        p1.printData();
        System.out.println(MyPoint.linesegment(p1, p2));
    }
}
