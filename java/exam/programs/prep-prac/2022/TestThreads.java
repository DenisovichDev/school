// class StringThread extends Thread {
//     private String message;
//     public StringThread (String message) {
//         this.message = message;
//     }

//     @Override
//     public void run() {
//         System.out.println(this.message);
//     }
// }
class StringRunnable implements Runnable {
    private String message;
    public StringRunnable (String message) {
        this.message = message;
    }

    public void run() {
        System.out.println(this.message);
    }

}


class TestThreads {
    public static void main(String[] args) {
        // StringThread t1 = new StringThread("Hello");
        // StringThread t2 = new StringThread("Hi");
        Thread t1 = new Thread(new StringRunnable("Hello"));
        Thread t2 = new Thread(new StringRunnable("Hi"));

        t1.start();
        t2.start();
    }
}
