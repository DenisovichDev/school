import java.io.*;
import java.util.Scanner;

class FileHandling {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter filename: ");
        String filename = sc.nextLine();
        System.out.println("Enter a String to enter: ");
        writeToFile(filename, sc.nextLine());
        System.out.println(readFromFile(filename));
    }

    private static void writeToFile(String filename, String content) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(content);
            System.out.println("Content written successfully");
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
    }

    private static String readFromFile(String filename) {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
        }
        return content.toString();
    }
}
