class LongCon {
    static void bubbleSort(int arr[], int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int a[] = {1, 30, 5, 2, 40, 31, 32, 33, 45, 42, 34, 35, 39, 6, 41, 36, 43, 37, 38, 44, 4, 5, 3};
        bubbleSort(a, a.length);
        int start, stop, l, longestLength;
        stop = -1; 
        start = 0;
        l = 1;
        longestLength = 1;
        for (int i = 1; i < a.length; i++) {
            if (a[i] - a[i-1] == 1) {
                l++;
                continue;
            }
            if (l >= longestLength) {
                longestLength = l;
                start = stop + 1;
                stop = i - 1;
            }
            l = 1;
            stop = i - 1;
        }

        for (int i = start; i <= stop; i++) {
            System.out.println(a[i] + " ");
        }
        
    }
}
