class Replace {
    static String replace(char c1, char c2, String ipstr) {
        String opstr = "";
        for (int i = 0; i < ipstr.length(); i++) {
            char c = ipstr.charAt(i);
            if (c == c1) {
                c = c2;
            }
            opstr = opstr + c;
        }
        return opstr;
    }
    public static void main(String[] args) {
        String given = "Beebloo is a wondergirl, she sways";
        System.out.println(replace('e', 'o', given));
    }
}
