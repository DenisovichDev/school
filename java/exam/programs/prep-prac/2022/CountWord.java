class StringSep {
    String st;
    String[] words;
    public StringSep(String s) {
        this.st = s;
    }

    int wordCount() {
        // String.split()
        this.words = this.st.split(" ");
        return words.length;
    }
    String revStr(String s) {
        String strOut = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            strOut = c + strOut;
        }
        return strOut;
    }
    void printRevWords() {
        for (String word : this.words) {
            System.out.println(revStr(word));
        }
    }
}

class CountWord {
    public static void main(String[] argv) {
        StringSep strsep = new StringSep("Hello world this is java");
        System.out.println(strsep.wordCount());
        strsep.printRevWords();
    }
}
