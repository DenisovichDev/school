import java.util.*;
// Negative Exception
class NegativeException extends Exception {
    public NegativeException(String s) {
        super(s);
    }
}

class CustomError {
    public static void main(String[] args) {
        int[] positive = new int[10];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < positive.length; i++) {
            try {
                int n = sc.nextInt();
                if (n < 0) {
                    throw new NegativeException("Err: Negative Exception");
                }
                positive[i] = n;


            } catch (NegativeException e) {
                System.out.println("Negative values are not allowed!");
                break;
            }
        }
    }
}
