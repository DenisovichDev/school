import java.util.Scanner;

class NotEqualToException extends Exception {
    public NotEqualToException(String s) {
        super(s);
    }
}

class Custom {
    public static void main(String[] args) {
        double pi;
        Scanner sc = new Scanner(System.in);
        try {
            final double PI_CONST = 3.14;
            pi = Float.parseFloat(sc.nextLine());
            System.out.println(pi);
            if (pi != PI_CONST)
                throw new NotEqualToException("Caught the exception");
        } catch (NotEqualToException e) {
            System.out.println("Not equal to PI");
        }
    }
}
