---
geometry:
- margin=1in
listings: false
header-includes: |
    \usepackage{tikzit}
    \usepackage{multirow}
    \usepackage{rotating}
    \input{styles.tikzstyles}
    \usepackage[document]{ragged2e}
---

# Java: The Complete Reference Notes

## Data Types, Variables, Arrays

**Data Types and Casting:**

- Strongly typed language: First, every variable has a type, every expression has a type, and every type is strictly defined. Second, all assignments, whether explicit or via parameter passing in method calls, are checked for type compatibility.
- Java defines **eight** primitive types of data: byte, short, int, long, char, float, double, boolean. These can be put to four groups: Integer (byte, short, int, long), Floating-point Numbers, Characters, Boolean.
- `long` : 64, `int` : 32, `short` : 16, `byte` : 8
- `double` : 64, `float` : 32
- At the time of Java’s creation, Unicode required 16 bits. Thus, in Java char is a **16-bit** type. The range of a char is 0 to 65,535.
- Automatic type conversion will take place when two conditions are met: 1. The two types are compatible, 2. The destination type is larger than the source type.
- Implicit type casting, also known as widening conversion, is done automatically by the compiler when there is no loss of data. Explicit type casting in Java, on the other hand, requires manual intervention and is used when there is a possibility of data loss.
- To create a conversion between two incompatible types, you must use a cast. A cast is simply an explicit type conversion.
- If the integer’s value is larger than the range of a
byte, it will be reduced modulo (the remainder of an integer division by the) byte’s range.

```
    int a = 128;
    byte b;
    // …
    b = (byte) a; // -128 (starts over)
    // Formula: cast value = (n % 128) - 128
```

- If a floating point value is assigned to an integer, the conversion that happens is called truncation. If the value 1.23 is assigned to an integer, the resulting value will simply be 1. The 0.23 will have been truncated. 

**Arrays**:

- 2D Array, left index determines row, right one column.
- Jagged array can be done by using `new` keyword with the array constructor like: `int[][] arr = new int[4][]`, and then assigning `arr[0][1] = new int[4]`.

## Control Statements

**Using `break` and `continue` as a form of `goto`**:

- The general form of labelled `break` statement is: `break label`

```
    // Using break as a civilized form of goto.
    class Break {
        public static void main(String[] args) {
            boolean t = true;
            first: {
                second: {
                    third: {
                        System.out.println("Before the break.");
                        if(t) break second; // break out of second block
                        System.out.println("This won't execute");
                    }
                    System.out.println("This won't execute");
                }
                System.out.println("This is after second block.");
            }
        }
    }
```

## A Closer Look at Methods and Classes

**Method Overloading**:

- It is possible to define two or more methods within the same class that share the same name, as long as their parameter declarations are different. This process is referred to as **Method Overloading**. It is one of the ways Java supports **polymorphism**. Overloading constructors is also very common. You can also use objects as parameters, so you can have a overloaded constructor definition where you copy the instance variables from another object to make a initialized copy.

**Arguments**:

- When you pass a *primitive type* to a method, it is *passed by value*. Thus, a copy of the argument is made, and what occurs to the parameter that receives the argument has no effect outside the method.
- When you pass an object on to a method, it becomes call-by-reference.

**Static and other quirks**:

- Instance variables declared as static are, essentially, global variables. When objects of its class are declared, no copy of a static variable is made. Instead, all instances of the class share the same static variable.
- `main()` is declared as static because it must be called before any objects exist.
- Static methods have several restrictions:
    1. They can only directly call other static methods of their class.
    1. They can only directly access static variables of their class.
    1. They cannot refer to this or super in any way. (The keyword super relates to inheritance and is described in the next chapter.)
- If you need to do computation in order to initialize your static variables, you can declare a **static block** that gets executed exactly once, when the class is first loaded.
- It is important to remember that before any object creation the memory for static variables and methods are allocated, and the static blocks are executed *during* class definition. 
- Static methods can be called before object creation, like: `ClassName.staticMethod()`.
- A field can also be `final`, making it a constant, essentially: `final int FILE_NEW = 1;`
- Classes can be nested, the inner class will have access to all the members, including private ones, of the classes in which it is nested.

**String Class**:

- Strings are immutable, and they can be declared in two ways: with string literal and with  `new` keyword. The string literal allocates the string in String Constant Pool, whereas `new` allocates it in heap. For same string content, string literal allocates only one instance.
- You can test two strings for equality by using `equals()`. You can obtain the length of a string by calling the `length()` method. You can obtain the character at a specified index within a string by calling `charAt()`. 

## Inheritance

- Although a subclass includes all of the members of its superclass, it cannot access those members of the superclass that have been declared as private.
- `super` can be used to call superclass constructor (`super()`, it can also be used to access members of the superclass (`super.member`)
- In a class hierarchy, when a method in a subclass has the same name and type signature as a method in its superclass, then the method in the subclass is said to override the method in the superclass. When an overridden method is called through its subclass, it will always refer to the version of that method defined by the subclass. The version of the method defined by the superclass will be hidden.

**Dynamic Method Dispatch**:

- Dynamic method dispatch is the mechanism by which a call to an overridden method is resolved at run time, rather than compile time. Dynamic method dispatch is important because this is how Java implements **run-time polymorphism**.
- When different types of objects are referred to, different versions of an overridden method will be called. In other words, it is the type of the object being referred to (not the type of the reference variable) that determines which version of an overridden method will be executed. Therefore, if a superclass contains a method that is overridden by a subclass, then when different types of objects are referred to through a superclass reference variable, different versions of the method are executed. Eg: If B and C extends A, then a reference to A will call methods of A's version, the same reference type when referred to B will call methods of B's version, etc.

**Abstract Class**:

- There are situations in which you will want to define a superclass that declares the structure of a given abstraction without providing a complete implementation of every method. Such a class determines the nature of the methods that the subclasses must implement. One way this situation can occur is when a superclass is unable to create a meaningful implementation for a method. 

- The abstract keyword is a non-access modifier, used for classes and methods:
    1. Abstract class is a restricted class that cannot be used to create objects (to access it, it must be inherited from another class).
    1. Abstract method: can only be used in an abstract class, and it does not have a body. The body is provided by the subclass (inherited from). An abstract class can have both abstract and regular methods:

```
    // A Simple demonstration of abstract.
    abstract class A {
        abstract void callme();
        // concrete methods are still allowed in abstract classes
        void callmetoo() {
            System.out.println("This is a concrete method.");
        }
    }
    class B extends A {
        void callme() {
            System.out.println("B's implementation of callme.");
        }
    }
    class AbstractDemo {
        public static void main(String[] args) {
            B b = new B();
            b.callme();
            b.callmetoo();
        }
    }
```

- If a method meth() is declared as `final`, it cannot be overridden in B. If you attempt to do so, a compile-time error will result. Methods declared as final cannot
be overridden. 

**Object Class**:

- There is one special class, Object, defined by Java. All other classes are subclasses of Object. That is, Object is a superclass of all other classes. This means that a reference variable of type Object can refer to an object of any other class. Also, since arrays are implemented as classes, a variable of type Object can also refer to any array.

| Method                      | Purpose                                                             |
|-----------------------------|----------------------------------------------------------------------|
| `Object clone( )`          | Creates a new object that is the same as the object being cloned.    |
| `boolean equals(Object object)` | Determines whether one object is equal to another.               |
| `void finalize( )`         | Called before an unused object is recycled. (Deprecated by JDK 9.)  |
| `Class<?> getClass( )`      | Obtains the class of an object at run time.                          |
| `int hashCode( )`          | Returns the hash code associated with the invoking object.            |
| `void notify( )`           | Resumes execution of a thread waiting on the invoking object.        |
| `void notifyAll( )`        | Resumes execution of all threads waiting on the invoking object.      |
| `String toString( )`       | Returns a string that describes the object.                          |
| `void wait( )`             | Waits on another thread of execution.                                 |
| `void wait(long milliseconds)` | Waits on another thread of execution for a specified time in milliseconds. |
| `void wait(long milliseconds, int nanoseconds)` | Waits on another thread of execution for a specified time in milliseconds and nanoseconds. |

## String Handling

**String Buffer**:

- `StringBuffer` supports a modifiable string. As you know, String represents fixed-length, immutable character sequences. In contrast, `StringBuffer` represents growable and writable character sequences. `StringBuffer` may have characters and substrings inserted in the middle or appended to the end. `StringBuffer` will automatically grow to make room for such additions and often has more characters preallocated than are actually needed, to allow room for growth
- The current length of a `StringBuffer` can be found via the `length()` method, while the total allocated capacity can be found through the `capacity()` method. They have the following general forms:

```
    int length()
    int capacity()
```
- The syntax is straight forward: `StringBuffer sb = new StringBuffer("Hello");`
- `StringBuilder` is similar to `StringBuffer` except for one important difference: it is not synchronized, which means that it is not thread-safe. The advantage of `StringBuilder` is faster performance. However, in cases in which a mutable string will be accessed by multiple threads, and no external synchronization is employed, you must use `StringBuffer` rather than `StringBuilder`.

## Packages and Interfaces

- Packages are containers for classes. They are used to keep the class namespace compartmentalized into manageable chunks and can be stored in a **hierarchical** manner.
- The package statement defines a namespace in which classes are stored. Omitting it puts the class name into the default package which has no name (the same unnamed package for all files). Every class is a part of some package. 
- If `package pkg;` is added to a program to make it a part of pkg, its .class file must be kept in a directory called pkg. Packages are mirrored by directories.
- When we compile a file that uses a class (or interface) that is not defined in the same file, the Java compiler uses
    + the name of the class
    + the names of imported packages (if any)
    + the name of the current package

    to try to locate the class definition. 
- `CLASSPATH` to tell the compiler where to look for the classes.

## Exception Handling

- The `finally` block always executes when the `try` block exits. It is used to open a block of code regardless of whether an exception was thrown or not. A `finally` block is often used to restore state, for instance by releasing resources acquired in the preceding `try` block. Both `catch` and `finally` are optional, but we must use one of them. In case the error is thrown but there is no `catch` block to handle it then the control will exit from the program when the exception occurs and JVM will show the resulting message as the output. To execute the statements after the `try` block, they must be enclosed in the `finally` block.
- `Unchecked Exceptions`: These are not explicitly checked by the compiler i.e., even if the programmers do not handle the exception themselves, the runtime environment (JRE) will show the in-built error message. For example, `ArthmeticException` on diving by 0.
- In Java, all objects returned by errors and exceptions (that can or cannot be controlled by programmers e.g. fault with the OS is beyond the control of a programmer) are of type, or subtype, `Throwable`. The objects returned contain stack information that is used for debugging.
- An `Error` is thrown to indicate a serious problem that the application should not try to resolve. It is also regarded as a type of exception. An error is not under the control of the programmer, but of the OS/system. It is usually related to memory. `StackOverflowError`, `OutOfMemoryError` etc.
- An `Exception`, on the other hand is for a condition that a reasonable application might want to catch. Exception and all its subclasses (except `RuntimeException`) are regarded as checked exceptions.
- The `RuntimeException` is a subclass of Exception. It is special because it is an unchecked exception. `RuntimeExceptions` are used for conditions that an application typically don’t catch, such as programming errors.

## Threads

- Threads allows a program to operate more efficiently by doing multiple things at the same time. Threads can be used to perform complicated tasks in the background without interrupting the main program.
- Can be implemented in two ways:
    1. It can be created by extending the `Thread` class and overriding its `run()` method. Then simply make an instance of that subclass and call the `start()` method.
    1. Implement the `Runnable` interface, with the definition of a `run()` method, then passing an instance of it to a `Thread` object's constructor, and calling its `start()` method.

- Thread synchronization is the coordination of multiple threads to ensure that they do not interfere with each other's execution when accessing shared resources or critical sections of code. Synchronization helps maintain data consistency and prevents race conditions, where the outcome of the program depends on the timing of thread execution. It can be done in Java in several ways:
- You can use the `synchronized` keyword to declare a method as synchronized. When a thread invokes a synchronized method, it locks the object's monitor (also known as intrinsic lock or mutex) associated with that method's object, preventing other threads from executing synchronized methods of the same object concurrently.
- Synchronized blocks allow you to specify a particular section of code that needs to be synchronized. This allows for more granular control over synchronization compared to synchronizing entire methods.
- Example:

```
    public class Counter {
        private int count = 0;

        // Method to increment the counter in a synchronized manner
        public synchronized void increment() {
            count++;
            System.out.println("Counter is now: " + count);
        }
    }

    public class IncrementThread extends Thread {
        private Counter counter;

        public IncrementThread(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            // Each thread will increment the counter 5 times
            for (int i = 0; i < 5; i++) {
                counter.increment();
            }
        }
    }

    public class Main {
        public static void main(String[] args) {
            // Create a shared counter object
            Counter counter = new Counter();

            // Create two threads to increment the counter
            IncrementThread thread1 = new IncrementThread(counter);
            IncrementThread thread2 = new IncrementThread(counter);

            // Start the threads
            thread1.start();
            thread2.start();
        }
    }
```
