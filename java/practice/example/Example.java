class ClassOne {
    public int a = 10;
}

class Example {
    // properties
    // variables in the context of the class
    private int prop_1 = 23;
    private int prop_2; // space got allocated without a value
    // constructor
    public Example(int arg) { // arguments are given while object creation: example- Example e = new Example(3);
        // gets executed during object creation
        this.prop_2 = arg;
        this.prop_1++;
    }
    // methods
    // just a function
    public void meth_1() {
        // this => "this" object that the control is within
        this.prop_1 = 23;
    }
    protected int meth_2(int n) {
        this.prop_2 += n;
        return this.prop_2;
    }
    
    // access specifier (public static), return type, arguments => commandline args array (which an array of strings)
    public static void main(String[] argv) {
        System.out.println("Beebloo");
        Example eg = new Example(2);
        Example eg_2 = new Example(2);
        System.out.println(eg.prop_1);
        System.out.println(eg_2.prop_1);
        eg_2.meth_1();
        System.out.println(eg.prop_1);
        System.out.println(eg_2.prop_1);

        // access
        ClassOne c = new ClassOne();
        System.out.println(c.a);
    }
}
