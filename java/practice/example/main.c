#include <stdio.h>

/* void getError() { */
/*     exit(1); */
/* } */
int main(int argc, char** argv) {
    /* getError(); */
    printf("argc: %d\n", argc);
    for (int i = 0; i < argc; ++i) {
        printf("argv[%d]: %s\n", i, argv[i]);
    }
    return 0;
}
