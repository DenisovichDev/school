# Inheritence

Inheritence in Java is a mechanism in which an object acquires all the properties and behaviors of a parent object.

It represents the IS-A relationship, which is also known as parent-child relationship. HAS-A relationship is represented by container class.

Used for:

- Code reusability
- Method overriding

```java
class SubClass extends SuperClass;
```

## Types of ingeritence in Java

- Single
- Multilevel
- Hierarchical

Java doesn't support multiple and hybrid inheritence. In case of hybrid inheritence, the problem that is introduced is called diamond problem.

