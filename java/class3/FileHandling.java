import java.io.*;

class FileHandling {
    public static void main(String args[]) {
        byte buffer[] = new byte[200];

        try {
            FileInputStream file = new FileInputStream("test1.txt");
            file.read(buffer, 0, 200);
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        String text = new String(buffer, 0);
        System.out.println(text);
    }
}
