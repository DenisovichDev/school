import java.io.*;

class FileSequence {
    public static void main(String args[]) {
        try {
            FileInputStream f1 = new FileInputStream("test.txt");
            FileInputStream f2 = new FileInputStream("test2.txt");

            SequenceInputStream merge = new SequenceInputStream(f1, f2);

            int c;
            while ((c = merge.read()) != -1) {
                System.out.print((char) c);
            }
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
}
