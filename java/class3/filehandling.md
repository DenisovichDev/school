# File Handling

Streams:

- Byte Stream
- Character Stream

## Byte Stream

Class | Description
------|------------
DataInputStream | Read input from console
DataOutputStream | Read output from console
FileInputStream | 
FileOutputStream |
SequenceInputStream |
SequenceOutputStream |
