class Marks:
    def getPercentage(self):
        pass

class A(Marks):
    def __init__(self, sub1, sub2, sub3):
        self.sub1 = sub1
        self.sub2 = sub2
        self.sub3 = sub3

    def getPercentage(self):
        return (self.sub1 + self.sub2 + self.sub3) / 3

class B(Marks):
    def __init__(self, sub1, sub2, sub3, sub4):
        self.sub1 = sub1
        self.sub2 = sub2
        self.sub3 = sub3
        self.sub4 = sub4

    def getPercentage(self):
        return (self.sub1 + self.sub2 + self.sub3 + self.sub4) / 4

if __name__ == "__main__":
    a = A(45, 60, 98)
    b = B(67, 34, 25, 100)

    print("Average marks in percentage for A:", "%.2f" % a.getPercentage() + "%")
    print("Average marks in percentage for B:", "%.2f" % b.getPercentage() + "%")
