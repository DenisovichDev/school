# Write a Python program to create a class representing a stack data
# structure. Include methods for pushing, popping and displaying elements.


class Stack:
    def __init__(self):
        self.stack = [];

    def push(self, e):
        self.stack.append(e);
        return e;

    def pop(self):
        if len(self.stack) == 0: 
            return None;
        return self.stack.pop(-1);
    
    def show(self):
        print("The current stack is:")
        print(self.stack);

if __name__ == "__main__":
    s = Stack();
    s.push(4);
    s.push(3);
    s.push(6);
    s.pop();
    s.pop();
    s.push(8);
    s.push(9);
    s.show();
