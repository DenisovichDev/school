# Create a base class ‘Square’ having instance variable side:double. Initiate variable using
# constructor, a method ‘getVolume() : double’ that calculates volume and print it. Create a derived
# class ‘Cylinder’ having instance variable height:double. Initiate variables of both classes through
# constructor, override method ‘getVolume() : double’ to calculate volume of cylinder taking ‘side’
# variable of base class as ‘radius’ and print it.

class Square:
    def __init__(self, side):
        self.side = side;
    
    def getVolume(self):
        vol = self.side ** 3;
        print("Volume:", vol);


class Cylinder(Square):
    def __init__(self, height, radius):
        self.height = height;
        self.side = radius;

    def getVolume(self):
        vol = self.side * (math.pi ** 2) * self.height;
        print("Volume:", vol);

if __name__ == "__main__":
    pass
