# Write a Python program to create a class that represents a shape. Include methods to calculate its area and perimeter. Implement subclasses for different shapes like circle, triangle, and square.
import math

class Shape:
    def calcArea(self):
        pass

    def calcPerimeter(self):
        pass

class Circle(Shape):
    def __init__(self, r):
        self.r = r;

    def calcArea(self):
        return math.pi * (self.r ** 2)

    def calcPerimeter(self):
        return 2 * math.pi * self.r

class Triangle(Shape):
    def __init__(self, b, h, side=(0, 0, 0)):
        self.b = b
        self.h = h
        self.side = side

    def calcArea(self, b, h):
        return 0.5 * self.b * self.h

    def calcPerimeter(self):
        return side[0] + side[1] + side[2]

class Square(Shape):
    def __init__(self, s):
        self.s = s

    def calcArea(self):
        return self.s ** 2

    def calcPerimeter(self):
        return 4 * self.s
