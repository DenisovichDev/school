4. Write a Python program to create a class that represents a shape. Include methods to calculate its area and perimeter. Implement subclasses for different shapes like circle, triangle, and square.
5. Write a Python program to create a class representing a linked list data structure. Include methods for displaying linked list data, inserting and deleting nodes.
6. Write a program to build a simple Student Management System using Python which can perform the following operations:
Accept
Display
Search
Delete
Update
