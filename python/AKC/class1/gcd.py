def gcd(a, b):
    d = min(a, b);
    while (d):
        if (a % d == 0 and b % d == 0):
            break
        d -= 1
    return d

if __name__ == "__main__":
    n1, n2 = input("Enter two numbers: ").split();

    print("The GCD of", n1, n2, "is:", gcd(int(n1), int(n2)));

