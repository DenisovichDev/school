def armstrong(n):
    k = 0;
    temp = n;
    while (temp):
        k += 1;
        temp = int(temp / 10);

    s = 0;
    temp = n;
    while (temp):
        dig = temp % 10;
        temp = int(temp / 10);
        s += dig**k;

    if (s == n):
        return True;
    return False;



if (__name__ == "__main__"):
    l, n = input("Enter a range to find all the Armstrong numbers: ").split();
    l = int(l);
    n = int(n);
    if (l < 0 or n < 0):
        print("Please enter positive numbers")
        exit(1);

    print("The Armstrong numbers in range", l, "->", n, "are:");
    for i in range(l, n + 1):
        if(armstrong(i)):
            print(i);
