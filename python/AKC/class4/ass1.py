# 1. Write a Python script to concatenate dictionaries to create a new one
def concatDict(dictArray):
    n = len(dictArray)
    if n <= 1:
        return dictArray[0];
    catDict = dictArray[0].copy();
    for i in range(1, n):
        for key, value in dictArray[i].items():
            catDict[key] = value
    return catDict;

if __name__ == '__main__':
    n = eval(input("Enter the number of dictionaries you want to input: "))
    dictArray = []
    for i in range(n):
        m = eval(input("Enter number of elements in dict " + str(i+1) + ": "))
        tempDict = {}
        for j in range(m):
            key = input("Enter key: ")
            value = input("Enter value: ")
            tempDict[key] = value
        dictArray.append(tempDict)
    print(concatDict(dictArray))

    
