if __name__ == '__main__':
        n = eval(input("Enter number of elements in dict "))
        d = {}
        for j in range(n):
            key = input("Enter key: ")
            value = input("Enter value: ")
            d[key] = value
        print("Initial dict:")
        print(d)
        v = []
        cd = {}
        for key, value in d.items():
            if value in v:
                cd[key] = value
            else:
                v.append(value)
        for key, value in cd.items():
            del d[key]
        print("Final dict:")
        print(d)
