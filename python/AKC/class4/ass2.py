# 2. Write a Python script to print a dictionary where the keys are numbers between 1 and 15 (both included) and the values are square of keys

if __name__ == '__main__':
    d = {}
    for i in range(1, 16):
        value = i ** 2
        d[i] = value;
    print(d)

