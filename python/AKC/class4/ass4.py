def addDicts(d1, d2):
    for key, value in d1.items():
        if d2[key] is not None:
            d1[key] = eval(value) + eval(d2[key])
    for key, value in d2.items():
        if d1[key] is None:
            d1[key] = d2[key]

if __name__ == '__main__':
    pass

