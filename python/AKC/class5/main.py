# 5: Write a program in python that will print a string of text with step length taken from user and also can print from backwards with step length taken from user without slicing technique


inpString = input("Enter a string to slice into parts: ")
stepLen = int(input("Enter a step length: "))

outText = ""
count = 0
for c in inpString:
    if (count == stepLen):
        outText += " "
        count = 0
    outText += c
    count += 1

rev = ""
for i in inpString:
    rev = i + rev

count = 0
outTextRev = ""
for c in rev:
    if (count == stepLen):
        outTextRev += " "
        count = 0
    outTextRev += c
    count += 1

print(outText)
print(outTextRev)
