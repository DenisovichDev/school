5. Write a program in python that will print a string of text with step length taken from the user and also can print from backwards with step length taken from user (without slicing technique)
6. Write a program in Python that will take a string and number K from user and extract first K consecutive digits making a number
7. Write a program in Python that will check whether a string is palindrome or not
8. Write a Python program to get a string from a given string where all occurrences of its first character have been changed to "$", except the first character itself.
    Sample string: 'restart'
    Expected result: 'resta$t'
9. Write a Python function that takes a list of words and returns the length of the longest word
