---
geometry:
- margin=1in
fontsize: 11pt
listings: false
header-includes: |
    \usepackage{tikzit}
    \usepackage{multirow}
    \usepackage{rotating}
    \input{styles.tikzstyles}
    \usepackage[document]{ragged2e}
---

# Python Exam Notes

## Introduction

- Duck typing: “If it looks like a duck and quacks like a duck, it’s a duck”. The method and attribute of an object is more important than the object it defines
- Python is a dynamically typed language. It means that the type of a variable is allowed to change over its lifetime.
- In Static Typing, type checking is performed in compile time. Means the variable type is known at compile time. The variable type usually do not change.
- Strongly typed is a concept used to refer to a programming language that enforces strict restrictions on intermixing of values with differing data types. When such restrictions are violated and error (exception) occurs. Python is strongly typed.
- Aliasing is when two or more variables refer to the same object

## Strings

- Raw String: Python raw string treats the backslash character (\) as a literal character. Raw string is useful when a string needs to contain a backslash, such as for a regular expression or Windows directory path, and you don’t want it to be treated as an escape character.

```
    raw_s = r"Hello\nWorld"
    print(raw_s) # Hello\nWorld
```

- `format()` is a string method that replaces placeholders (defined inside `{}`), and formats it in the specified manner.

```
    txt1 = "My name is {fname}, I'm {age}".format(fname = "John", age = 36)
    txt2 = "My name is {0}, I'm {1}".format("John",36)
    txt3 = "My name is {}, I'm {}".format("John",36)
    txt = "You scored {:.0%}"
    print(txt.format(0.25))
```

- PEP 498 introduced a new string formatting mechanism known as Literal String Interpolation or more commonly as F-strings (because of the leading f character preceding the string literal). Works the same as `format()`.

```
    print(f"Hello, My name is {name} and I'm {age} years old.")
```

- [String methods](https://www.w3schools.com/python/python_ref_string.asp) (click)
- `join()` joins elements of iterables separated by the string.

## Lists

- **List comprehension** offers a shorter syntax when you want to create a new list based on the values of an existing list.

```
    # newlist = [expression for item in iterable if condition == True]
    newlist = [x for x in fruits if x != "apple"]
    newlist = [x for x in range(10) if x < 5]
```

## Tuples

- In python tuples are used to store immutable objects. Python Tuples are very similar to lists except to some situations. Python tuples are immutable means that they can not be modified in whole program.
- **Packing and unpacking** are tuple assignment features that assigns the right-hand side of values into the left-hand side. Packing is simply the assignment and unpacking is putting the tuple values into single variables.

```
    nums = 2, 3, 4                      # packing
    fruits = ("apple", "banana", "cherry")
    (green, yellow, red) = fruits       # unpacking
```

- If the number of variables is less than the number of values, you can add an * to the variable name and the values will be assigned to the variable as a list:

```
    fruits = ("apple", "mango", "papaya", "pineapple", "cherry")
    (green, *tropic, red) = fruits # tropic = mango, papaya, pineapple
```

- Adding two tuples with `+` operator:

```
    thistuple = ("apple", "banana", "cherry")
    y = ("orange",)
    thistuple += y
```

## User-defined Functions and Recursion

- `docstrings` are a way to properly document python code. It provides a convenient way of associating documentation with Python modules, functions, classes, and methods. The `docstrings` can be accessed using the `__doc__` attribute of the object or using the help function.
- If a function exceeds this limit, it can be increased using the `sys.setrecursionlimit(n)` function.

## Intro to Object Oriented Programming

- Python Magic methods are the methods starting and ending with double underscores `__`. They are defined by built-in classes in Python and commonly used for operator overloading. Built in classes define many magic methods, `dir()` function can show you magic methods inherited by a class.
- The members of a class that are declared protected are only accessible to a class derived from it. Data members of a class are declared protected by adding a single underscore `_` symbol before the data member of that class. Private is done with `__`
