import sys

# ternary operator
# myvar = 0 if False else 8;
# print(myvar)

# String formatting

# rishi_age = 987 * 0.3
rishi_age = 9
print(rishi_age)
print("Bhaswar Chakraborty is {age:.0%} years old".format(age = rishi_age))

print(f"Bhaswar Chakraborty is {rishi_age:f} years old")
st = r"Hello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorldHello\nWorld\tHello\nWorldHello\nWorld\tHello\nWorldHello\nWorldHello\nWorld"
print(st)



def even(x):
    return x % 2 == 0;

def main():
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    fun = lambda a, b: a + b
    print(fun(3, 4))

    f = filter(lambda x: x % 2 == 0, numbers)
    for n in f:
        print(n)

if __name__ == "__main__":
    # main()
    pass
