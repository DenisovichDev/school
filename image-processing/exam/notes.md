---
geometry:
- margin=1in
fontsize: 11pt
listings: false
numbersections: true
header-includes: |
    \usepackage{tikzit}
    \usepackage{multirow}
    \usepackage{rotating}
    \input{styles.tikzstyles}
    \usepackage{algorithm}
    \usepackage{algpseudocode}
    \usepackage{multicol}
    \usepackage{wrapfig}
---

# Introduction

**Digital Image**: A digital image is defined as a two-dimensional function, f(x, y), where x and y are spatial coordinates, and the amplitude of f at any point of coordinates (x, y) is called the intensity or Grey level of the image at that point (output of f). 

Digital Image Processing deals with the manipulation of digital images through a digital computer. 

**_Difference between Computer Graphics and Image Processing?_**  
Computer Graphics creates an image based on geometric, lighting information, materials, textures, etc. Image processing is concerned with the manipulation techniques of an existing Image.  
One begins with the knowledge of the different shapes and object in the 2D or 3D space. Image Processing requires no such prior knowledge, we are only concerned with a matrix of image data.

**Distance between two pixels**:

_Euclidean:_ $\sqrt{(x-s)^2+(y-t)^2}$  
_City-Block:_ $|x-s|+|y-t|$  
_Chessboard:_ $max(|x-s|,|y-t|)$



**Neighbourhood Metrics**: A pixel's neighbourhood refers to its surrounding pixels, impacting operations (such as filtering, edge detection). _Four Connectivity_ considers pixels above, below, left and right. _Eight Connectivity_ includes the diagonally adjacent pixels. The choice of connectivity influences the outcome of the neighbourhood-based operations.

**Sampling and Quantization**: You know this, this is not something to write in a short note, far to broad for that.

**Image Enhancement Techniques**:

![Image Enhancement Techniques](./figures/enhancement.png)

**Image Restoration**: Improving appearance of an image. Restoration is objective (enhancement is subjective) in the sense that it is to be based on mathematical or probabilistic model of degradation (as opposed to what constitutes a "good" enhancement result.

**Color Image Processing**: In an image, a great deal of extra information may be contained in the color, and this extra information can then be used to simplify image analysis, e.g., object identification and extraction based on color. Color Image Processing is concerned with these topics.

**Morphological Processing**: Tools for extracting image components that are useful in the representation and description of shape. E.g., Binary skeletonization (of fingerprints and so on)

**Segmentation**: Partitions an image into its constituent parts or _objects_. More difficult topics of DIP. More on it later.

**Feature Extraction**: It follows the output of a segmented stage, usually ray pixel data consisting a boundary or region. Feature extraction consists of feature detection (finding the feature in an image, region or boundary) and feature description (assigning quantitative attributes to the detected features). E.g., detecting a triangle in a segmented stage, and then describing it with quantitative attributes, like orientation and size.

**Image Pattern Classification**: The process that assigns a label to an object based on its feature descriptors. Implemented using deep neural networks.

---

# Spatial Domain Operations{#sec:spatial-domain-op}

The spatial domain processes we discuss are based on the expression:
$$
g(x,y) = T[f(x,y)]
$$
Where $g$ is the output image and $f$ is the input image, and $T$ is an operator on $f$ defined over a neighbourhood of point $(x,y)$. Typically, the neighborhood is rectangular, centered on an arbitrary point $(x_0,y_0)$ , and much smaller in size than the image.  
The smallest possible neighborhood is of size 1x1. In this case, $g$ depends only on the value of $f$ at a single point $(x,y)$ and $T$ in the equation becomes an *intensity* (also called a *gray-level*, or *mapping*) transformation function of the form:
$$
s=T(r)
$$
This is called a **single-pixel operation**. For simplicity we use $s$ and $t$ to denote, respectively, the intensity of $g$ and $f$ at a point. Approaches whose results depend only on the intensity at a point sometimes are called *point processing techniques*, as opposed to the *neighborhood processing*.

## Some Basic Intensity Transformation Functions

Intensity transformations are among the simplest of all image processing techniques. We denote the values of pixels, before and after processing, by r and s, respectively. These values are related by a transformation T that maps a pixel value r into a pixel value s. Because we deal with digital quantities, values of an intensity transformation function typically are stored in a table, and the mappings from r to s are implemented via table lookups. For an 8-bit image, a lookup table containing the values of T will have 256 entries.  
Some basic intensity transformations are shown in the next figure: linear (negative and identity transformations), logarithmic (log and inverse-log transformations), and power-law (nth power and nth root transformations). The identity function is the trivial case in which the input and output intensities are identical.

![Basic Intensity Transformations](./figures/intensity-transformations.png)

**_Image Negative_**: The negative of an image with intensity levels in the range $[0,L-1]$ is obtained by using the negative transformation function which has the form:
$$
s=L-1-r
$$
This type of processing is used, for example, in enhancing white or gray detail embedded in dark regions of an image, especially when the black areas are dominant in size.

***Log Transformations***: The general form if the log transformation is:
$$
s=c\log{(1+r)}
$$
where $c$ is a constant and it is assumed that $r\ge0$. The shape of the log curve in the above figure shows that this transformation maps a narrow range of low intensity values in the input into a wider range of output levels. For example, note how input levels in the range $[0,L/4]$ map to output levels to the range $[0, 3L/4]$. Conversely, higher values of input levels are mapped to a narrower range in the output. We use a transformation of this type to expand the values of dark pixels in an image, while compressing the higher-level values. The opposite is true of the inverse log (exponential) transformation.

![Fourier Spectrum displayed as a grayscale image (left) and result of applying log transformation with c = 1 (right)](./figures/log.png)

***Power-Law (Gamma) Transformations***: In power-law transformation we have the form:
$$
s=cr^{\gamma}
$$
Where $c$ and $\gamma$ are positive constants. Sometimes it is written as $s=c(r,\epsilon)^{\gamma}$ to account for offsets (that is, a measurable output when the input is zero). However, offsets typically are an issue of display calibration, and as a result they are normally ignored. Note in the following figure that a family of transformations can be obtained simply by varying g. Curves generated with values of $\gamma>1$ have exactly the opposite effect as those generated with values of $\gamma<1$. When $c=\gamma=1$ the equation reduces to the identity transformation.

![Plot of the gamma equation for various values of gamma](./figures/gamma.png){width=300px}


\begin{wrapfigure}{l}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{./figures/gamma-correction.png}
  \end{center}
  \caption{Gamma correction of an intensity ramp}
\end{wrapfigure}

The response of many devices used for image capture, printing, and display obey a power law. By convention, the exponent in a power-law equation is referred to as gamma. The process used to correct these power-law response phenomena is called **gamma correction** or gamma encoding. For example, cathode ray tube (CRT) devices have an intensity-to-voltage response that is a power function, with exponents varying from approximately 1.8 to 2.5. As the curve for $\gamma=2.5$, such display systems would tend to produce images that are darker than intended. Following figure illustrates this effect. The top-left is an image of an intensity ramp displayed in a monitor with a gamma of 2.5. As expected, the output of the monitor appears darker than the input, in the top-right image shows.

In this case, gamma correction consists of using the transformation $s=r^{1/2.5}=r^{0.4}$ to pre-process the image before inputting it into the monitor. Bottom-left is the result. When input into the same monitor, the gamma-corrected image produces an output that is close in appearance to the original image. A similar analysis as above would apply to other imaging devices, such as scanners and printers, the difference being the device-dependent value of gamma.

In addition to gamma correction, power-law transformations are useful for general-purpose contrast manipulation. It is useful for spotting fractures/deformities in MRI scans.

### Piecewise Linear Transformation Functions

Piecewise linear transformation is type of gray level (intensity) transformation that is used for image enhancement. It is a spatial domain method. It is used for manipulation of an image so that the result is more suitable than the original for a specific application.  
Some commonly used piece-wise linear transformations are:

***Contrast Stretching***: Low-contrast images can result from poor illumination, lack of dynamic range in the imaging sensor, or even the wrong setting of a lens aperture during image acquisition. Contrast stretching expands the range of intensity levels in an image so that it spans the ideal full intensity range of the recording medium or display device.

![Contrast Stretching Function\label{fig:contrast-stretching}](./figures/contrast-stretching.png)

Fig. \ref{fig:contrast-stretching} shows a typical transformation used for contrast stretching. The locations of points $(r_1,s_1)$ and $(r_2,s_2)$ control the shape of the transformation function. If $r_1=s_1$ and $r_2=s_2$ the transformation is a linear function that produces no changes in intensity. If $r_1=r_2$, $s_1 = 0$, and $s_2=L-1$ the transformation becomes a thresholding function that creates a binary image. Intermediate values of $(r_1,s_1)$ and $(s_2,r_2)$ produce various degrees of spread in the intensity levels of the output image, thus affecting its contrast. In general, $r_1\le r_2$ and $s_1\le s_2$ is assumed so that the function is single valued and monotonically increasing. This preserves the order of intensity levels, thus preventing the creation of intensity artifacts.

![Intensity level slicing methods (a) and (b)\label{fig:intensity-level}](./figures/intesity-level-slicing.png){width=350px}

***Intensity Level Slicing***: There are applications in which it is of interest to highlight a specific range of intensities in an image. Some of these applications include enhancing features in satellite imagery, such as masses of water, and enhancing flaws in X-ray images. The method, called *intensity-level slicing*, can be implemented in two general ways. One approach is to display all the values inside the range of interest in one value (say, white), and all other intensities in another (say, black). This transformation, shown in Fig. \ref{fig:intensity-level}(a), produces a binary image. The second approach, based on the transformation in Fig. \ref{fig:intensity-level}(b), brightens (or darkens) the desired range of intensities, but leaves all other intensity levels in the image unchanged.

The following image is an aortic angiogram near the kidney area. The objective of this example is to use intensity-level slicing to enhance the major blood vessels that appear lighter than the background, as a result of an injected contrast medium. The images from left to right show, respectively, the original image, application of the first kind of transformation, and the application of the second kind of transformation.

![Aortic angiogram and application of intensity level slicing](./figures/intensity-level-slicing-example.png)

\begin{wrapfigure}{R}{0.5\textwidth}
    \begin{center}
        \includegraphics[width=0.48\textwidth]{./figures/bit-plane-slicing.png}
    \end{center}
    \caption{Bit-Planes of an 8-bit image}\label{fig:bit-planes}
\end{wrapfigure}

***Bit-Plane Slicing***: Pixel values are integers composed of bits. For example, values in a 256-level grayscale image are composed of 8 bits (one byte). As Fig. \ref{fig:bit-planes} illustrates, an 8-bit image can be imagined being composed of 8 bit-planes, with plane 1 containing lowest order of bits of all the pixels and so on.

Decomposing an image into its bit planes is useful for analyzing the relative importance of each bit in the image, a process that aids in determining the adequacy of the number of bits used to quantize the image. Also, this type of decomposition is useful for image compression, in which fewer than all planes are used in reconstructing an image.

### Histogram Processing

If $r_k$ for $k=1(1)L-1$ denote the intensity values, the unnormalized histogram of an L-level image is defined by:
$$
h(r_k)=n_k
$$
Where $n_k$ is the pixel frequency of the intensity value $r_k$. Similarly, the normalized histogram is defined by:
$$
p(r_k)= \frac{h(r_k)}{MN} = \frac{n_k}{MN}
$$
$M$ and $N$ the number of rows and columns of the image. Naturally the sum of $p(r_k)$ for all values of $r_k$ is 1, and the components of $p(r_k)$ give us an estimate of the probabilities of intensity levels occurring in an image. Histogram shape is related to image appearance, hence histogram manipulation is a fundamental tool in image processing.

![Image apperance and image histograms\label{fig:histogram-example}](./figures/histogram-apperance.png)

In the right most image in Fig. \ref{fig:histogram-example}, we see that the components of the histogram of the high-contrast image cover a wide range of the intensity scale, and the distribution of pixels is not too far from uniform, with few bins being much higher than the others. Intuitively, it is reasonable to conclude that an image whose pixels tend to occupy the entire range of possible intensity levels and, in addition, tend to be distributed uniformly, will have an appearance of high contrast and will exhibit a large variety of gray tones. The net effect will be an image that shows a great deal of gray-level detail and has a high dynamic range.

***Histogram Equalization***: Histogram Equalization is a computer image processing technique used to improve contrast in images. It accomplishes this by effectively spreading out the most frequent intensity values, i.e. stretching out the intensity range of the image. This method usually increases the global contrast of images when its usable data is represented by close contrast values. This allows for areas of lower local contrast to gain a higher contrast.

![Histogram Equalization](./figures/histogram-equalization.png){width=450px}

To calculate it, we first need to calculate the PMF values of all the pixels of the image, that is just the normalized frequencies of each pixels ($p(r_k)$). Our next step involves calculation of CDF (cumulative distributive function), which is just cumulative values of PMF. Then we multiply the CDF with $L-1$, and round out the value to get the new set of intensity values. These sets are mapped to the same set of frequency values.  
The discrete form of the transformation functions used for histogram equalization is:
$$
s_k = (L-1)\sum_{j=0}^{k} p_r(r_k)\text{ where } k=0, 1, \dots, L-1
$$
Here $p_r(r_k) = n_k/MN$, the PMF. Since these values are fractional, we take rounded values as the new intensity values, $s_k$. The following table illustrates the process.

Table: Histogram equalization example

| $rk$    | $nk$  | $p_r(r_k) = n_k/MN$ | $s_k$  | $round(s_k)$ |
|---------|-------|---------------------|--------|--------------|
|   $r_0$ |   790 |   0.19              |   1.33 |   1          |
|   $r_1$ |   1023|   0.25              |   3.08 |   3          |
|   $r_2$ |   850 |   0.21              |   4.55 |   5          |
|   $r_3$ |   656 |   0.16              |   5.67 |   6          |
|   $r_4$ |   329 |   0.08              |   6.23 |   6          |
|   $r_5$ |   245 |   0.06              |   6.65 |   7          |
|   $r_6$ |   122 |   0.03              |   6.68 |   7          |
|   $r_7$ |   81  |   0.02              |   7.00 |   7          |

***Histogram Stretching***: There is another method of enhancing contrast. This method is called histogram stretching, and it is used to increase contrast, which is the difference between the highest and the lowest intensity values in the image. The formula for it is:
$$
g(x,y)= \frac{f(x,y) - fmin}{fmax - fmin}\times (2^{bpp}-1)
$$
Here $fmin$ and $fmax$ are the minimum and maximum pixel intensity of the image. For example, for an 8-bit image, bpp (bits per pixel) is 8, so 256 levels, and say $fmin$ is 0 and $fmax$ is 225. The formula in that case would be:
$$
g(x,y)= \frac{f(x,y) - 0}{225 - 0}\times 255
$$
This stretches the histogram (surprise, surprise). This method fails when the intensity values 0 and 255 (minimum and maximum) are present in the image, because then the formula becomes, $g(x,y)=f(x,y)$. So it's pretty useless.

## Spatial Filtering

Spatial filtering modifies an image by replacing the value of each pixel by a function of the values of the pixel and its neighbors. If the operation performed on the image pixels is linear, then the filter is called a linear spatial filter. Otherwise, the filter is a nonlinear spatial filter. Spatial filtering is done in the manner discussed in Section \ref{sec:spatial-domain-op}. Examples of spatial filtering include image smoothing and sharpening.

### The Mechanics of Linear Spatial Filtering

A linear spatial filter performs a sum-of-products operation between an image f and a _filter kernel, w_. The kernel is an array whose size defines the neighborhood of operation, and whose coefficients determine the nature of the filter.

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{center}
        \includegraphics[width=0.48\textwidth]{./figures/kernel.png}
    \end{center}
    \caption{Mechanics of linear spatial filtering using a 3x3 kernel}\label{fig:kernel}
\end{wrapfigure}

Figure \ref{fig:kernel} illustrates the mechanics of linear spatial filtering using a 3x3 kernel. At any point $(x,y)$ in the image, the response, $g(x,y)$, of the filter is the sum of products of the kernel coefficients and the image pixels encompassed by the kernel:

\begin{multline}
g(x,y)=w(-1,-1)f(x-1,y-1) + \\
\dots + w(1,1)f(x+1,y+1)
\end{multline}

As coordinates x and y are varied, the center of the kernel moves from pixel to pixel, generating the filtered image, $g$, in the process. In general, for a kernel of size  $M\times N$, where $M=2a+1$ and $N=2b+1$, $a$ and $b$ being nonnegetive integers (we need kernel of odd size in both direction), the spatial filtering is given by:
\begin{equation}
g(x,y)=\sum_{i=-a}^{a}\sum_{j=-b}^{b}w(i,j)f(x-i,y-j)
\end{equation}

***Spatial Correlation and Convolution***

Spatial correlation is illustrated graphically in Fig. \ref{fig:kernel} and mathematically in the last equation. Correlation consists of moving the center of a kernel over an image, and computing the sum of products at each location. The mechanics of spatial convolution are the same, except that the correlation kernel is rotated by 180°. Thus, when the values of a kernel are symmetric about its center, correlation and convolution yield the same result. The reason for rotating the kernel will become clear in the following discussion. The difference is shown with an example:

![](./figures/correlation-and-convolution.png)

One thing to notice is that correlating a kernel w with a function that contains all 0’s and a single 1 yields a copy of w, but rotated by 180°. A function that contains a single 1 with the rest being 0’s is called a discrete unit impulse. Correlating a kernel with a discrete unit impulse yields a rotated version of the kernel at the location of the impulse.

Whereas, for convolution the kernel is pre-rotated by 180 degrees. As the convolution in Fig. \ref{fig:convolution} shows, the result of pre-rotating the kernel is that now we have an exact copy of the kernel at the location of the unit
impulse. In fact, a foundation of linear system theory is that convolving a function with an impulse yields a copy of the function at the location of the impulse.

![Correlation and convolution with a 2D kernel\label{fig:convolution}](./figures/2D-correlation-convolution.png)

### Smoothing

Smoothing in image processing is a technique used to reduce noise and fine details in an image by applying a low-pass filter. This filter works by replacing each pixel value with an average value of its neighboring pixels. Smoothing can help to improve the visual quality of an image and make it easier to analyze by reducing the impact of small variations in pixel values.

Smoothing filters, also known as blurring filters, are a type of image filter that are commonly used in image processing to reduce noise and remove small details from an image. There are several types of smoothing filters such as, including mean filter, median filter, Gaussian filter, and bilateral filter.

Applications of image smoothing:

- **Noise Reduction**: Smoothing is commonly used to reduce noise in images. This filter averages the pixel values in the image, reducing the impact of isolated pixels and random patterns. Various types of filters can be used, including mean filter, median filter, and Gaussian filter, each with its own strengths and weaknesses.
- **Edge Preservation**: When smoothing an image, it's important to preserve the edges (see Section \ref{sec:edge-detection} in the image. If edges are lost during the smoothing process, the image can appear blurry or unclear. To preserve edges during the smoothing process, a filter can be applied that selectively smooths areas of the image that are away from edges, while leaving the edges untouched. Examples of edge-preserving filters include bilateral filter, guided filter, and anisotropic diffusion filter.
- **Pre-Processing**: Smoothing can also be used as a pre-processing step before applying other image-processing techniques, such as segmentation or feature extraction.
- **Aesthetic Purposes**: Smoothing can be used for aesthetic purposes, such as creating a blurred background effect in a portrait photograph.

\begin{note}
Real world signals usually contain departures from the ideal signal that would be produced by our model of the signal production process. Such departures are referred to as \textbf{noise}. Noise arises as a result of unmodelled or unmodellable processes going on in the production and capture of the real signal. It is not part of the ideal signal and may be caused by a wide range of sources, e.g. variations in the detector sensitivity, environmental variations, the discrete nature of radiation, transmission or quantization errors, etc.\break

Noise can be image independent, or dependent on image data. Image independent noise can often be described by an additive noise model, where the recorded image f(i,j) is the sum of the true image s(i,j) and the noise n(i,j):
$$
    f(i,j) = s(i,j) + n(i,j)
$$
The impact of the noise is given by SNR (signal to noise ratio) given by: $SNR = \frac{\sigma_s}{\sigma_n}$

In the second case of data-dependent noise, it is possible to model it with a multiplicative or non-linear model. These are mathematically complicated, hence, if possible, the noise is assumed to be data independent.

\begin{wrapfigure}{r}{0.3\textwidth}
    \begin{center}
        \includegraphics[width=0.28\textwidth]{./figures/gaussian-curve.png}
    \end{center}
    \caption{1D Gaussian Distribution with mean 0 and SD 1}\label{fig:gaussian-curve}
\end{wrapfigure}


\subsubsection{Detector Noise (Gaussian)}
The noise can be modelled with an additive model where the noise $n(i,j)$ has a zero-mean Gaussian distribution described by its standard deviation ($\sigma$), or variance. This means that each pixel in the noisy image is the sum of the true pixel value and a random, Gaussian distributed noise value.

\subsubsection{Salt and Pepper Noise}
Another common form of noise is data drop-out noise (commonly referred to as intensity spikes, speckle or salt and pepper noise). Here, the noise is caused by errors in the data transmission. The corrupted pixels are either set to the maximum value (which looks like snow in the image) or have single bits flipped over. In some cases, single pixels are set alternatively to zero or to the maximum value, giving the image a `salt and pepper' like appearance. Unaffected pixels always remain unchanged. The noise is usually quantified by the percentage of pixels which are corrupted.

\end{note}

#### Box Filter Kernels (Mean Filter)

The mean filter is a type of linear smoothing filter that replaces each pixel in the image with the average of its neighboring pixels. The size of the neighboring pixels is defined by the filter kernel or mask. These kernels are called _box kernels_. The simplest, separable lowpass filter kernel is the box kernel, whose coefficients have the same value (typically 1). The name “box kernel” comes from a constant kernel resembling a box when viewed in 3-D.

![Results of lowpass filtering with box kernels of sizes 3x3, 11x11, and 21x21, respectively.](./figures/box-filter.png){#fig:box-filter width=300px}

#### Low-pass Gaussian Filter Kernels

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{center}
        \includegraphics[width=0.48\textwidth]{./figures/gaussian-2d.png}
    \end{center}
    \caption{2D Gaussian Distribution with mean (0,0) and $\sigma=1$}\label{fig:gaussian-2d}
\end{wrapfigure}


The Gaussian filter is a type of linear smoothing filter that is based on the Gaussian distribution. The filter works by convolving the image with a Gaussian kernel. Pixels closer to the center of the kernel have a higher weight than pixels farther away. The Gaussian filter is a good choice for smoothing images while preserving edges and details. However, it is computationally expensive and can produce ringing artifacts around edges.

In 2-D, an isotropic (i.e. circularly symmetric) Gaussian has the form:
$$
G(x,y)=\frac{1}{2\pi\sigma^2}e^{-\frac{x^2+y^2}{2\sigma^2}}
$$
The idea of Gaussian smoothing is to use this 2-D distribution as a 'point-spread' function, and this is achieved by convolution. Since the image is stored as a collection of discrete pixels we need to produce a discrete approximation to the Gaussian function before we can perform the convolution. In theory, the Gaussian distribution is non-zero everywhere, which would require an infinitely large convolution kernel, but in practice it is effectively zero more than about three standard deviations from the mean, and so we can truncate the kernel at this point. Following kernel is a discrete approximation to a Gaussian function with $\sigma=1.0$:
$$
\frac{1}{273}
\begin{bmatrix}
    1 & 4 & 7 & 4 & 1 \\
    4 & 16 & 26 & 16 & 4 \\
    7 & 26 & 41 & 26 & 7 \\
    4 & 16 & 26 & 16 & 4 \\
    1 & 4 & 7 & 4 & 1 \\
\end{bmatrix}
$$

### Edge Detection Filters {#sec:edge-detection}

_Edge detection_ is a technique of image processing used to identify points in a digital image with discontinuities, simply to say, edges.

**Edge**: An edge in an image is a significant local change in the image intensity, usually associated with a discontinuity in either the image intensity or the first derivative of the image intensity.

An edge can be detected by analyzing the first derivative of the intensity profile, taken perpendicular to the edge. Similarly, an edge can be detected by determining the zero crossing of the second derivative.

![Derivatives in Edge Detection](./figures/edge.png){width=300px}

Edge detection operators based on the first derivative are known as the **"gradient" operators**.

**Roberts Operators**: The Roberts operators are along the diagonals:
$$
h_x =
\begin{bmatrix}
    +1 & 0 \\
    0 & -1 \\
\end{bmatrix}
h_y =
\begin{bmatrix}
    0 & +1 \\
    -1 & 0 \\
\end{bmatrix}
$$
Advantages:

- Detection of edges and orientation
- Diagonal direction points are preserved

Disadvantages:

- Very sensitive to noise
- Not accurate in edge detection

**Prewitt Operators**: Another variant are the Prewitt operators which deal with a kernel with an odd size:
 $$
h_x=
\begin{bmatrix}
    +1 & 0 & -1 \\
    +1 & 0 & -1 \\
    +1 & 0 & -1 \\
\end{bmatrix}
h_y=
\begin{bmatrix}
    +1 & +1 & +1 \\
    0 & 0 & 0 \\
    -1 & -1 & -1 \\
\end{bmatrix}
$$
Advantages:

- Good performance
- Best operator to detect orientation

Disadvantages:

- Diagonal points not always preserved
- Magnitude of coefficient fixed

**Sobel Operators**: Finally, the Sobel operators are a smoothed version of the Prewitt operators. Indeed, the coefficients reproduce a convolution by a Gaussian filter, which tends to play the role of a mean filter to attenuate noise:
$$
h_x=
\begin{bmatrix}
    +1 & 0 & -1 \\
    +2 & 0 & -2 \\
    +1 & 0 & -1 \\
\end{bmatrix}
h_y=
\begin{bmatrix}
    +1 & +2 & +1 \\
    0 & 0 & 0 \\
    -1 & -2 & -1 \\
\end{bmatrix}
$$
Advantages:

- Simple and easy computation
- Easy to search for smooth edges

Disadvantages:

- Diagonal points are not preserved always
- Sensitive to noise
- Not accurate


### Sharpening

Sharpening filters are used to highlight fine detail in an image or to enhance detail that has been blurred, either in error or as a natural effect of a particular method of image acquisition.

Different filters for sharpening:

- [(Click here)](https://homepages.inf.ed.ac.uk/rbf/HIPR2/log.htm) Laplacian
- High Boost
- [(Click here)](https://homepages.inf.ed.ac.uk/rbf/HIPR2/unsharp.htm) Unsharp
- First Derivative




---

# Thresholding

Image thresholding involves dividing an image into two or more regions based on intensity levels, allowing for easy analysis and extraction of desired features, by setting a threshold value. Pixels with intensities above or below the threshold can be classified accordingly This technique aids in tasks such as object detection, segmentation, and image enhancement.

**Global Thresholding**: A single threshold value is applied to an entire image. This technique  may not be suitable for images with varying lighting conditions or complex backgrounds.

**Local Thresholding**: Local thresholding addresses the limitations of global thresholding by considering smaller regions within the image. It calculates a threshold value for each region based on its local characteristics, such as mean or median intensity. It allows for better adaptability to varying lighting conditions and complex intensity distributions, resulting in more accurate segmentation of objects or regions with overlapping intensity values.

## Image Segmentation

Image segmentation is a method of dividing a digital image into subgroups called image segments, reducing the complexity of the image and enabling further processing or analysis of each image segment.

\begin{wrapfigure}{r}{0.5\textwidth}
    \begin{center}
        \includegraphics[width=0.48\textwidth]{./figures/growing.png}
    \end{center}
    \caption{Region Growing}\label{fig:Region-Growing}
\end{wrapfigure}

**Region Growing**: Region growing approach is the opposite of the split and merge approach:

- An initial set of small areas are iteratively merged according to similarity constraints.
- Start by choosing an arbitrary seed pixel and compare it with neighbouring pixels.
- Region is grown from the seed pixel by adding in neighbouring pixels that are similar, increasing the size of the region.
- When the growth of one region stops we simply choose another seed pixel which does not yet belong to any region and start again.

This whole process is continued until all pixels belong to some region. A _bottom up_ method. Region growing methods often give very good segmentations that correspond well to the observed edges.


**Region Splitting**: The basic idea of region splitting is to break the image into a set of disjoint regions which are coherent within themselves.

This process continues until no further splitting occurs. In the worst case this happens when the areas are just one pixel in size. This is a divide and conquer or top down method.

![Split and Merge and Quadtree Representation](./figures/splitnmerge.png)

To illustrate the basic principle of these methods let us consider an imaginary image.

- Let $I$ denote the whole image.
- Not all the pixels in $I$ are similar so the region is split.
- Assume that all pixels within regions $I_1$, $I_2$ and $I_3$ respectively are similar but those in $I_4$ are not.
- Therefore $I_4$ is split next as in Fig 35(c).
- Now assume that all pixels within each region are similar with respect to that region, and that after comparing the split regions, regions $I_{43}$ and $I_{44}$ are found to be identical.

We can describe the splitting of the image using a tree structure, using a modified quadtree. Each non-terminal node in the tree has at most four descendants, although it may have less due to merging.
