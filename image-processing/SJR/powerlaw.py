# g(x, y) = c * (f(x, y) ** gamma)

import cv2
import numpy as np
import math

if __name__ == "__main__":
    c = 1
    g = 0.9

    img = cv2.imread("lenna.png");
    plt = np.zeros(img.shape, np.uint8);
    plt = c * (img ** g)
    plt = plt.astype(np.uint8);
    cv2.imshow("Orignal", img);
    cv2.imshow("PLT", plt);

    cv2.waitKey(0);
    cv2.destroyAllWindows();
    
