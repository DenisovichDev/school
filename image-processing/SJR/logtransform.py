# g(x, y) = c * log(1 + f(x, y))
# c = 255 / log(256)

import cv2
import numpy as np
import math

if __name__ == "__main__":
    img = cv2.imread("baboon.png");
    log = np.zeros(img.shape, np.uint8);
    c = 255 / np.log(1 + img.max());
    log = c * np.log(1 + img);
    log = log.astype(np.uint8);
    cv2.imshow("Orignal", img);
    cv2.imshow("Logarithmic", log);

    cv2.waitKey(0);
    cv2.destroyAllWindows();
    
