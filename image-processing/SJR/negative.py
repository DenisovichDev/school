import cv2
import numpy as np

if (__name__ == "__main__"):
    img = cv2.imread("lenna.png");
    neg=np.zeros(img.shape, np.uint8);
    neg = 255 - img;
    cv2.imshow("Orignal", img);
    cv2.imshow("Negative", neg);

    cv2.waitKey(0);
    cv2.destroyAllWindows();
    
