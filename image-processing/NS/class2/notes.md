# Image Transformations

## Image Negative

If the range is [1, L - 1], the transformation is:

```
s = 1 - L - r
```

## Log Transformations

Scales the lower intensity levels to a higher interval of intensity values.

```
s = c * log(1 + r)
```

## Power-Law (Gamma) Transformations

Heavily used in the medical domain.


# Thresholding

The threshold value is ideally the average of the maximum and the minimum intensity value.

# Other Topics Covered

- Bit plane slicing

