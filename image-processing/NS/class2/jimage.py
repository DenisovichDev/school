import cv2
import numpy as np

if __name__ == "__main__":
    img1 = cv2.imread("../images/baboon.png");
    img2 = cv2.imread("../images/lenna.png");
    jimg = np.zeros(img1.shape, np.uint8);
    himg = np.zeros(img1.shape, np.uint8);

    for i in range(img1.shape[0]//2):
        for j in range(img1.shape[1]):
            jimg[j, i] = img1[j, i];
    for i in range(img2.shape[0]//2, img2.shape[1]):
        for j in range(img2.shape[1]):
            jimg[j, i] = img2[j, i];

    for i in range(img1.shape[0]//2):
        for j in range(img1.shape[1]):
            himg[i, j] = img1[i, j];
    for i in range(img2.shape[0]//2, img2.shape[1]):
        for j in range(img2.shape[1]):
            himg[i, j] = img2[i, j];

    cv2.imshow("Orignal", img1);
    cv2.imshow("Baboon", img2);
    cv2.imshow("Gebbles", jimg);
    cv2.imshow("Goobbles", himg);

    cv2.waitKey(0);
    cv2.destroyAllWindows();
    
