# https://drive.google.com/drive/folders/1fF0trpdFP4Nqd4555xugasIa3riF7MtH

import cv2;
import numpy as np;

def display(channel, color, img):
	col = ['Red', 'Green', 'Blue'];
	print("\nThe matrix for color channel", col[channel], "of", color, "rectangle:");
	for i in img:
		for j in i:
			print(j[channel], end='\t');
		print();

if (__name__ == "__main__"):
	img_r = cv2.imread('./images/rr.png', cv2.IMREAD_UNCHANGED);
	img_g = cv2.imread('./images/rg.png', cv2.IMREAD_UNCHANGED);
	img_b= cv2.imread('./images/rb.png', cv2.IMREAD_UNCHANGED);

	# print("Dimension is: ", img_r.shape);
	# cv2.imshow("Title of the window", img_r);

	img_r = cv2.cvtColor(img_r, cv2.COLOR_BGR2RGB);
	img_g = cv2.cvtColor(img_g, cv2.COLOR_BGR2RGB);
	img_b = cv2.cvtColor(img_b, cv2.COLOR_BGR2RGB);

	for i in range(3):
		display(i, 'red', img_r);
		display(i, 'green', img_g);
		display(i, 'blue', img_b);

	cv2.waitKey(0);
	cv2.destroyAllWindows();
