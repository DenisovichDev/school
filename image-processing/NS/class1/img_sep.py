# https://drive.google.com/drive/folders/1fF0trpdFP4Nqd4555xugasIa3riF7MtH

import cv2;
import numpy as np;

if (__name__ == "__main__"):
	img = cv2.imread('./images/veg.png');
	blue, green, red = cv2.split(img);

	# Define a zero matrix
	zeros = np.zeros(red.shape, np.uint8);

	# Merge the channels to see the image
	red_ch = cv2.merge([zeros, zeros, red]);

	cv2.imshow('Red Coco', red_ch);

	cv2.waitKey(0);
	cv2.destroyAllWindows();
