import cv2
import numpy as np

# Read the image in grayscale
image = cv2.imread('img/veg.png', cv2.IMREAD_GRAYSCALE)

# Define Sobel kernels for x and y directions
sobel_kernel_x = np.array([[-1, 0, 1],
                           [-2, 0, 2],
                           [-1, 0, 1]])

sobel_kernel_y = np.array([[-1, -2, -1],
                           [0, 0, 0],
                           [1, 2, 1]])

# Padding size
p = 1

# Pad the image
padded_image = np.pad(image, ((p, p), (p, p)), mode='constant')

# Create empty images for storing the results
sobel_x = np.zeros_like(image, dtype=np.float32)
sobel_y = np.zeros_like(image, dtype=np.float32)

# Apply Sobel operator manually in x-direction
for y in range(p, padded_image.shape[0] - p):
    for x in range(p, padded_image.shape[1] - p):
        sobel_x[y - p, x - p] = np.sum(padded_image[y - p:y + p + 1, x - p:x + p + 1] * sobel_kernel_x)

# Apply Sobel operator manually in y-direction
for y in range(p, padded_image.shape[0] - p):
    for x in range(p, padded_image.shape[1] - p):
        sobel_y[y - p, x - p] = np.sum(padded_image[y - p:y + p + 1, x - p:x + p + 1] * sobel_kernel_y)

# Normalize the results to display them properly
sobel_x = cv2.normalize(sobel_x, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
sobel_y = cv2.normalize(sobel_y, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)

# Display the edge-detected images for x and y directions
cv2.imshow('Sobel Edge Detection (X direction)', sobel_x)
cv2.imshow('Sobel Edge Detection (Y direction)', sobel_y)
cv2.waitKey(0)

# Close OpenCV windows
cv2.destroyAllWindows()

