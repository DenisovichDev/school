import numpy as np
import cv2

SOBEL, PREWITT, ROBERTS = 4, 3, 1


SOBEL_X_KERNEL= np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
SOBEL_Y_KERNEL = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])

PREWITT_X_KERNEL = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
PREWITT_Y_KERNEL = np.array([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])

ROBERTS_X_KERNEL = np.array([[1, 0], [0, -1]])
ROBERTS_Y_KERNEL = np.array([[0, 1], [-1, 0]])

def apply_edge_detection_operator(img, op):
    out_x = np.zeros_like(img, np.float32)
    out_y = np.zeros_like(img, np.float32)
    out_c = np.zeros_like(img, np.float32)
    r, c = img.shape
    
    if op == SOBEL:
        for x in range(1, r - 1):
            for y in range(1, c - 1):
                out_x[x, y] = np.sum(img[x-1:x+2, y-1:y+2] * SOBEL_X_KERNEL)
                out_y[x, y] = np.sum(img[x-1:x+2, y-1:y+2] * SOBEL_Y_KERNEL)
    elif op == PREWITT:
        for x in range(1, r - 1):
            for y in range(1, c - 1):
                out_x[x, y] = np.sum(img[x-1:x+2, y-1:y+2] * PREWITT_X_KERNEL)
                out_y[x, y] = np.sum(img[x-1:x+2, y-1:y+2] * PREWITT_Y_KERNEL)
    elif op == ROBERTS:
        for x in range(1, r - 1):
            for y in range(1, c - 1):
                out_x[x, y] = np.sum(img[x-1:x+1, y-1:y+1] * ROBERTS_X_KERNEL)
                out_y[x, y] = np.sum(img[x-1:x+1, y-1:y+1] * ROBERTS_Y_KERNEL)

    out_c = np.sqrt(out_x ** 2 + out_y ** 2)

    o_max, o_min = out_c.max(), out_c.min()
    theoratical_upper_limit = np.sqrt(((255 * op) ** 2) * 2)

    for x in range(r):
        for y in range(c):
            # out_c[x, y] = map(out_c[x, y], 0, theoratical_upper_limit, 0, 255)
            out_c[x, y] = map(out_c[x, y], o_min, o_max, 0, 255)

    return out_c.astype(np.uint8)

def norm(pix):
    return map(pix, -rng, rng, 0, 255)


def map(num, inMin, inMax, outMin, outMax):
    return outMin + (float(num - inMin) / float(inMax - inMin) * (outMax - outMin))
