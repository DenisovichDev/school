import cv2
import numpy as np 
import operators as ot

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

img = cv2.imread('../img/gagnik.png', cv2.IMREAD_GRAYSCALE)
row, col = img.shape

sobel_img = ot.apply_edge_detection_operator(img, ot.SOBEL)
prewitt_img = ot.apply_edge_detection_operator(img, ot.PREWITT)
roberts_img = ot.apply_edge_detection_operator(img, ot.ROBERTS)
        

titles = ['Original', 'Sobel Edge Detection', 'Prewitt Edge Detection', 'Roberts Edge Detection']
images = [img, sobel_img, prewitt_img, roberts_img] 
show_images(titles, images)

cv2.waitKey()
cv2.destroyAllWindows()
