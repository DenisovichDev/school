import cv2
import numpy as np

# Read the image
image = cv2.imread('../img/lenna.png', cv2.IMREAD_GRAYSCALE)

# Apply Sobel operator in x-direction
sobel_x = cv2.Sobel(image, cv2.CV_64F, 1, 0, ksize=3)

# Apply Sobel operator in y-direction
sobel_y = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=3)

# Combine the gradient images using magnitude calculation
sobel_combined = np.sqrt(sobel_x**2 + sobel_y**2)

# Normalize the result to convert it to the range [0, 255]
sobel_combined = cv2.normalize(sobel_combined, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)

# Display the original image and the edge-detected image
cv2.imshow('Original Image', image)
cv2.imshow('Sobel Edge Detection', sobel_combined)
cv2.waitKey(0)

# Close OpenCV windows
cv2.destroyAllWindows()

