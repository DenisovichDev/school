# Logical operations

import cv2
import numpy as np 

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

img1 = cv2.imread('img/lenna.png', cv2.IMREAD_GRAYSCALE)
img2 = cv2.imread('img/veg.png', cv2.IMREAD_GRAYSCALE)
img1 = cv2.resize(img1, (img2.shape[1], img2.shape[0]))

and_i = cv2.bitwise_and(img1, img2)
or_i = cv2.bitwise_or(img1, img2)
xor_i = cv2.bitwise_xor(img1, img2)
not_1 = cv2.bitwise_not(img1)
not_2 = cv2.bitwise_not(img2)

titles = ['Original 1', 'Original 2', 'AND', 'OR', 'XOR', 'NOT 1', 'NOT 2']
images = [img1, img2, and_i, or_i, xor_i, not_1, not_2]
show_images(titles, images)

cv2.waitKey()
cv2.destroyAllWindows()
