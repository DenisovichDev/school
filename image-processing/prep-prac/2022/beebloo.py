import cv2
import numpy as np 

img = cv2.imread('img/lenna.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

thr = gray.copy()
thr[thr < th1] = 0
thr[thr >= th1] = 255

neg = gray.copy()
neg = 255 - neg

c = 255 / np.log(1 + gray.max())
logimage = gray.copy()
logimage = c * np.log(1 + gray)
logimage = logimage.astype(np.uint8)

ret, inbuilt = cv2.threshold(gray, th1, 255, cv2.THRESH_BINARY)

# cv2.imshow('Threshold', thr)
# cv2.imshow('Negative', neg)
cv2.imshow(f'Log Transform: c = {c:.2f}', logimage)

cv2.waitKey()
