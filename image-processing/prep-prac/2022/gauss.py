import cv2
import numpy as np 
import random

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

img = cv2.imread('img/lenna.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
row, col = gray.shape

noisy = gray.copy()

mean, std_dev = 0, 128
gauss = np.random.normal(mean, std_dev, gray.shape).astype(np.uint8)
# Additive noise
# f(x,y) = s(x,y) + n(x,y)
noisy = cv2.add(gray, gauss)

titles = ['Original', 'Gaussian Noise']
images = [gray, noisy]
show_images(titles, images)

cv2.waitKey()
cv2.destroyAllWindows()
