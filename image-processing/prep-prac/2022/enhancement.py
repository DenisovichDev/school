import cv2
import numpy as np 

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

img = cv2.imread('img/lenna.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
row, col = gray.shape

p = 30
i_mat = np.ones(gray.shape, np.uint8)
bright = gray.copy()
dark = gray.copy()
bright = cv2.add(gray, p * i_mat)
dark = cv2.subtract(gray, p * i_mat)

titles = ['Original', 'Brightened', 'Dark']
images = [gray, bright, dark]
show_images(titles, images)

cv2.waitKey()
cv2.destroyAllWindows()
