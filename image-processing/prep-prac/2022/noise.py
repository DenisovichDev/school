import cv2
import numpy as np 
import random

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

def add_gaussian_noise(img, mean, std_dev):
    gauss = np.random.normal(mean, std_dev, img.shape).astype(np.uint8)
    # Additive noise
    # f(x,y) = s(x,y) + n(x,y)
    noisy_image = cv2.add(img, gauss)
    return noisy_image

def add_salt_and_pepper(img, salt_amount, pepper_amount):
    saltpepper = gray.copy()
    for i in range(salt_amount):
        r = random.randint(0, row - 1)
        c = random.randint(0, col - 1)
        saltpepper[r][c] = 255

    for j in range(pepper_amount):
        r = random.randint(0, row - 1)
        c = random.randint(0, col - 1)
        saltpepper[r][c] = 0

    return saltpepper

def mean_filter(img, x, y):
    # neighborhood = np.array([img[x-1,y-1], img[x, y-1], img[x+1, y-1], img[x-1, y], img[x,y], img[x+1,y], img[x-1, y+1], img[x,y+1], img[x+1,y+1]])
    n_array = []
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            n_array.append(img[x+i][y+j])
    neighborhood = np.array(n_array)
    return int(neighborhood.mean())

def apply_mean_filter(img):
    m_filter = np.zeros(img.shape, np.uint8)
    for i in range(row - 1):
        for j in range(col - 1):
            m_filter[i][j] = mean_filter(img, i, j)
    return m_filter

def median_filter(img, x, y):
    n_array = []
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            n_array.append(img[x+i][y+i])
    neighborhood = np.array(n_array)
    return sorted(neighborhood)[5]

def apply_median_filter(img):
    med_filter = np.zeros(img.shape, np.uint8)
    for i in range(row - 1):
        for j in range(col - 1):
            med_filter[i][j] = median_filter(img, i, j)
    return med_filter


def min_filter(img, x, y):
    n_array = []
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            n_array.append(img[x+i][y+i])
    neighborhood = np.array(n_array)
    return max(neighborhood)

def apply_min_filter(img):
    min_filt = np.zeros(img.shape, np.uint8)
    for i in range(row - 1):
        for j in range(col - 1):
            min_filt[i][j] = min_filter(img, i, j)
    return min_filt


img = cv2.imread('img/lenna.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
row, col = gray.shape

# sp_noise = add_salt_and_pepper(gray, 10000, 5000)
m_filter = apply_mean_filter(gray)
g_img = cv2.subtract(gray, m_filter)
f_sharp = cv2.add(gray, 4 * g_img)
# med_filter = apply_median_filter(sp_noise)
# min_filter = apply_min_filter(gray)
# g_noise = add_gaussian_noise(gray, 0, 128)
# m_filter = apply_mean_filter(g_noise)

# sub = cv2.subtract(gray, sp_noise)

titles = ['Original', 'Smooth', 'G(x,y)', 'Sharp']
images = [gray, g_img, m_filter, f_sharp]
show_images(titles, images)

cv2.waitKey()
cv2.destroyAllWindows()
