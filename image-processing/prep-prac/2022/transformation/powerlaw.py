import cv2
import numpy as np 
import histogram as his

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

def map(num, inMin, inMax, outMin, outMax):
    return outMin + (float(num - inMin) / float(inMax - inMin) * (outMax - outMin))

img = cv2.imread('../img/gagnik.png', cv2.IMREAD_GRAYSCALE)
row, col = img.shape

def power_law(img, c, gamma):
    pl = np.zeros_like(img, np.float32)
    pl = 255 * c * (img / 255) ** gamma

    return pl.astype(np.uint8)

g1, g2 = 6.0, 1/6
p1 = power_law(img, 1, g1)
p2 = power_law(img, 1, g2)

titles = ['Original', f'Power-Law (gamma = {g1:.2f})', f'Power-Law (gamma = {g2:.2f})']
images = [img, p1, p2] 
his.draw_histogram(images, titles)
show_images(titles, images)

cv2.waitKey(0)
cv2.destroyAllWindows()
