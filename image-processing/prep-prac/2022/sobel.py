import cv2
import numpy as np

def constraint(val, minimum, maximum):
    if val > maximum:
        return maximum
    if val < minimum:
        return minimum
    return val

def scale(val, m1, n1, m2, n2):
    return m2 + ((n2 - m2) / (n1 - m1)) * (val - m1)


# Read the image
image = cv2.imread('img/gagnik.png', cv2.IMREAD_GRAYSCALE)
rows, cols = image.shape

sobel_h_x = np.array([[-1, 0, 1],
                      [-2, 0, 2],
                      [-1, 0, 1]])

sobel_h_y = np.array([[-1, -2, -1], 
                      [0, 0, 0],
                      [1, 2, 1]])

roberts_x = np.array([[0, 1],
                      [-1, 0]])

roberts_y = np.array([[1, 0],
                      [0, -1]])

p_img = np.pad(image, ((1, 1), (1, 1)))
edge_sobel_x = np.zeros_like(p_img, np.uint8)
edge_sobel_y = np.zeros_like(p_img, np.uint8)
rob_edge_x = np.zeros_like(p_img, np.uint8)
rob_edge_y = np.zeros_like(p_img, np.uint8)
sobel_both = np.zeros_like(p_img, np.uint8)


# for hx
for x in range(1, rows - 1):
    for y in range(1, cols - 1):
        sop = abs(np.sum(p_img[x-1:x+2, y-1:y+2] * sobel_h_x))
        # sop = np.sum(p_img[x-1:x+2, y-1:y+2] * sobel_h_x)
        edge_sobel_x[x, y] = constraint(sop, 0, 255)

        sop = abs(np.sum(p_img[x-1:x+2, y-1:y+2] * sobel_h_y))
        # sop = np.sum(p_img[x-1:x+2, y-1:y+2] * sobel_h_y)
        edge_sobel_y[x, y] = constraint(sop, 0, 255)
        
        sobel_both[x, y] = np.sqrt(edge_sobel_x[x, y] ** 2 + edge_sobel_y[x, y]** 2)
        # sobel_both[x, y] = constraint(sop, 0, 255)

        # sop = np.sum(p_img[x-1:x+1, y-1:y+1] * roberts_x)
        # rob_edge_x[x,y] = constraint(sop, 0, 255)

        # sop = np.sum(p_img[x-1:x+1, y-1:y+1] * roberts_y)
        # rob_edge_y[x,y] = constraint(sop, 0, 255)

# sobel_both = np.sqrt(edge_sobel_x ** 2 + edge_sobel_y ** 2)
# print(type(sobel_both))

# for x in range(1, rows - 1):
#     for y in range(1, cols - 1):
#         sobel_both[x,y] = constraint(sobel_both[x,y], 0, 255)

# sobel_both = cv2.normalize(sobel_both, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)

ret, sobel_bin = cv2.threshold(sobel_both, 80, 255, cv2.THRESH_BINARY)

cv2.imshow('Padded Image', p_img)
# cv2.imshow('Hx', edge_sobel_x)
# cv2.imshow('Hy', edge_sobel_y)
cv2.imshow('Sobel Combined', sobel_both)
cv2.imshow('Sobel Thresh', sobel_bin)
# cv2.imshow('Sx', rob_edge_x)
# cv2.imshow('Sy', rob_edge_y)
cv2.waitKey(0)

# Close OpenCV windows
cv2.destroyAllWindows()

