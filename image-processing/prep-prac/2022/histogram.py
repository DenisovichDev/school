import cv2, numpy as np
import matplotlib.pyplot as plt

def get_histogram(img):
    return cv2.calcHist(img, [0], None, [256], [0, 256])

def draw_histogram(images):
    h = []
    n = len(images)
    for img in images:
        h.append = get_histogram(img)
    for i, hist in enumurate(hist):
        plt.subplot(1, n, i)
        plt.plot(hist, color='black')
        plt.title(title[i])
        plt.xlabel('Pixel Intensity')
        plt.ylabel('Frequency')

    plt.tight_layout()
    plt.show()


