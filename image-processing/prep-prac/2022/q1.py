# write a program to perform colour separation into R, G and B from a colour image. Then use these R, G, and B to convert the original colour image to its equivalent greyscale image. Display output images.

import cv2
import numpy as np

def sep_channel(img, ch_off):
    out = np.zeros(img.shape, np.uint8)
    out[:,:,ch_off] = img[:,:,ch_off]
    return out

def grey_from_channel(img, ch):
    grey_op = np.zeros(img.shape, np.uint8)
    for i in range(3):
        grey_op[:,:,i] = img[:,:,ch]
    return grey_op

def threshold(img, th1):
    
    return out



def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

def main():
    filename = 'lenna.png'
    img = cv2.imread('img/' + filename)

    grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, binary = cv2.threshold(grey, 127, 255, cv2.THRESH_BINARY)

    red = sep_channel(img, 2)
    green = sep_channel(img, 1)
    blue = sep_channel(img, 0)

    g_r = grey_from_channel(img, 2)
    g_g = grey_from_channel(img, 1)
    g_b = grey_from_channel(img, 0)

    thr = threshold(grey, 127)

    titles = ['Original', 'Gray', 'Threshold']
    images = [img, grey, thr]
    show_images(titles, images)

    cv2.waitKey()


if __name__ == "__main__":
    main()
