import cv2
import numpy as np 

def show_images(titles, images):
    for t, i in zip(titles, images):
        cv2.imshow(t, i)

img = cv2.imread('img/lenna.png')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
row, col = gray.shape

gls = gray.copy()
a, b = 100, 150

# gls[gls > 100 and gls < 150] = 0
for i in range(row):
    for j in range(col):
        if (gls[i, j] > 100 and gls[i, j] < 150):
            gls[i, j] = 0
        else:
            gls[i, j] = 255

titles = ['Original', 'Gray Level Slicing']
images = [gray, gls]
show_images(titles, images)

cv2.waitKey()
cv2.destroyAllWindows()
